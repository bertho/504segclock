/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <pic18fregs.h>
#include <i2c.h>
#include <clock.h>
#include <serial.h>

/*#define DISABLE_WRITE	1*/

#define SCLO(v)		do { PORTCbits.RC2 = (v); i2cdly(); } while(0)
#define SCLI()		(PORTBbits.RB5)
#define SDAO(v)		do { PORTCbits.RC1 = (v); i2cdly(); } while(0)
#define SDAI()		(PORTCbits.RC0)

#define i2c_start()	do { SDAO(0); SCLO(0); } while(0)
#define i2c_start_rep()	do { SDAO(1); SCLO(1); SDAO(0); SCLO(0); } while(0)
#define i2c_stop()	do { SCLO(0); SDAO(0); SCLO(1); SDAO(1); } while(0)

#define I2C_READ	0x01
#define I2C_WRITE	0x00

#define I2C_RTCCADDR	0xde
#define I2C_EEADDR	0xae

#define I2C_TICKS	6	/* Write time is 5ms */

static void i2cdly(void)
{
	__asm
	nop
	nop
	nop
	nop
	nop
	__endasm;
}

void i2c_init(void)
{
	PORTCbits.RC1 = 1	/* i2c lines idle */;
	PORTCbits.RC2 = 1;
	TRISCbits.TRISC0 = 1;	/* SDA input */
	TRISCbits.TRISC1 = 0;	/* SDA output */
	TRISCbits.TRISC2 = 0;	/* SCL output */
	TRISBbits.TRISB5 = 1;	/* SCL input */
}

static void wait_for_timer(void)
{
	if(!INTCONbits.GIE)	/* Make sure we run when ints are off at init */
		return;
	if(clock_timer_idle(TIMER_I2C))
		return;
	while(!clock_timer_expired(TIMER_I2C))
		;
	clock_timer_clear(TIMER_I2C);
}

static uint8_t write_byte(uint8_t a) __wparam
{
	uint8_t i;
	uint8_t c;
	for(i = 0x80; i; i >>= 1) {
		if(a & i)
			SDAO(1);
		else
			SDAO(0);
		SCLO(1);
		for(c = 1; !SCLI() && c; c++)
			;
		SCLO(0);
	}
	SDAO(1);	/* Release dataline */
	SCLO(1);	/* Clock */
	for(c = 1; !SCLI() && c; c++)
		;
	i = SDAI();	/* Low is an ACK */
	SCLO(0);
	return !i;
}

static uint8_t read_byte(uint8_t ack) __wparam
{
	uint8_t i;
	uint8_t v = 0;
	uint8_t c;
	for(i = 8; i > 0; i--) {
		v <<= 1;
		SCLO(1);
		for(c = 1; !SCLI() && c; c++)
			;
		if(SDAI())
			v |= 1;
		SCLO(0);
	}
	if(ack) {
		SDAO(0);
		SCLO(1);
		SCLO(0);
		SDAO(1);
	} else {
		SDAO(1);
		SCLO(1);
		SCLO(0);
	}
	return v;
}

void i2c_uid_read(uint8_t *uid)
{
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_EEADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(0xf0))
		goto not_ack;
	i2c_start_rep();
	if(!write_byte(I2C_EEADDR | I2C_READ))
		goto not_ack;
	*uid++ = read_byte(1);
	*uid++ = read_byte(1);
	*uid++ = read_byte(1);
	*uid++ = read_byte(1);
	*uid++ = read_byte(1);
	*uid++ = read_byte(1);
	*uid++ = read_byte(1);
	*uid++ = read_byte(0);
	i2c_stop();
	return;
not_ack:
	i2c_stop();
	serial_puts("i2c_uid_read(): nak\n");
}

#ifndef DISABLE_WRITE
void i2c_uid_write(uint8_t *uid)
{
	wait_for_timer();
	i2c_reg_write(0x09, 0x55);
	i2c_reg_write(0x09, 0xaa);
	i2c_start();
	if(!write_byte(I2C_EEADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(0xf0))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	if(!write_byte(*uid++))
		goto not_ack;
	i2c_stop();
	clock_timer_set(TIMER_I2C, I2C_TICKS);
	return;
not_ack:
	i2c_stop();
	clock_timer_set(TIMER_I2C, I2C_TICKS);
	serial_puts("i2c_uid_write(): nak\n");
}
#else
void i2c_uid_write(uint8_t *uid)
{
	(void)uid;
}
#endif

uint8_t i2c_reg_read(uint8_t reg) __wparam
{
	uint8_t val = 0;
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_RTCCADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(reg))
		goto not_ack;
	i2c_start_rep();
	if(!write_byte(I2C_RTCCADDR | I2C_READ))
		goto not_ack;
	val = read_byte(0);
	i2c_stop();
	return val;
not_ack:
	i2c_stop();
	serial_puts("i2c_reg_read(): nak\n");
	return val;
}

#ifndef DISABLE_WRITE
void i2c_reg_write(uint8_t reg, uint8_t val)
{
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_RTCCADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(reg))
		goto not_ack;
	if(!write_byte(val))
		goto not_ack;
	i2c_stop();
	/* No need to start the timer, all writes are done fast */
	return;
not_ack:
	i2c_stop();
	serial_puts("i2c_reg_write(): nak\n");
}
#else
void i2c_reg_write(uint8_t reg, uint8_t val)
{
	(void)reg;
	(void)val;
}
#endif

void i2c_regs_read(uint8_t addr, uint8_t len, uint8_t *ptr)
{
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_RTCCADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(addr))
		goto not_ack;
	i2c_start_rep();
	if(!write_byte(I2C_RTCCADDR | I2C_READ))
		goto not_ack;
	while(--len)
		*ptr++ = read_byte(1);
	*ptr = read_byte(0);
	i2c_stop();
	return;
not_ack:
	i2c_stop();
	serial_puts("i2c_regs_read(): nak\n");
}

#ifndef DISABLE_WRITE
void i2c_regs_write(uint8_t addr, uint8_t len, uint8_t *ptr)
{
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_RTCCADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(addr))
		goto not_ack;
	while(len--) {
		if(!write_byte(*ptr++))
			goto not_ack;
	}
	i2c_stop();
	/* No need to start the timer, all writes are done fast */
	return;
not_ack:
	i2c_stop();
	serial_puts("i2c_regs_write(): nak\n");
}
#else
void i2c_regs_write(uint8_t addr, uint8_t len, uint8_t *ptr)
{
	(void)addr;
	(void)len;
	(void)ptr;
}
#endif

void i2c_eeprom_read(uint8_t addr, uint8_t len, uint8_t *ptr)
{
	if(!len)
		return;
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_EEADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(addr))
		goto not_ack;
	i2c_start_rep();
	if(!write_byte(I2C_EEADDR | I2C_READ))
		goto not_ack;
	while(--len)
		*ptr++ = read_byte(1);
	*ptr++ = read_byte(0);
not_ack:
	i2c_stop();
}

#ifndef DISABLE_WRITE
void i2c_eeprom_write(uint8_t addr, uint8_t len, uint8_t *ptr)
{
	if(!len || len > 8)
		return;
	wait_for_timer();
	i2c_start();
	if(!write_byte(I2C_EEADDR | I2C_WRITE))
		goto not_ack;
	if(!write_byte(addr))
		goto not_ack;
	while(len--) {
		if(!write_byte(*ptr++))
			goto not_ack;
	}
not_ack:
	i2c_stop();
	clock_timer_set(TIMER_I2C, I2C_TICKS);
}
#else
void i2c_eeprom_write(uint8_t addr, uint8_t len, uint8_t *ptr)
{
	(void)addr;
	(void)len;
	(void)ptr;
}
#endif
