/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <network.h>
#include <arp.h>
#include <ethernet.h>
#include <ip.h>
#include <serial.h>
#include <clock.h>
#include <config.h>

//#define DEBUG_ARP	1

static arptable_t arptable[MAXARPENTRIES];

void arp_init(void)
{
	memset(arptable, 0, sizeof(arptable));
	clock_timer_set(TIMER_ARP, ARPTIMER_TICKS);
}

void arp_handle_timer(void)
{
	uint8_t i;
	clock_timer_set(TIMER_ARP, ARPTIMER_TICKS);
	for(i = 0; i < MAXARPENTRIES; i++) {
		if(arptable[i].timeout) {
			if(!--arptable[i].timeout)
				memset(&arptable[i], 0, sizeof(arptable_t));
		}
	}
}

#define	_ah	packet.ethpkt.ah
#define _eth	packet.ethpkt.hdr

void arp_handle_rxpacket(uint16_t pktlen)
{
	/* No point continuing if the packet is too short */
	if(pktlen < sizeof(ethhdr_t) + sizeof(arphdr_t))
		return;
#ifdef DEBUG_ARP
	serial_puts("arp: accept len\n");
#endif
	/* Check if this packet makes sense */
	if(_ah.htype == htons(ARP_HTYPE_ETH) && _ah.ptype == htons(ETHTYPE_IP) && _ah.hlen == ARP_HLEN_ETH && _ah.plen == ARP_PLEN_IP) {
		/* We have a winner, request or reply? */
		if(_ah.operation == htons(ARP_REQUEST)) {
#ifdef DEBUG_ARP
			serial_puts("arp: got request\n");
#endif
			/* Ignore request this if the source MAC is a multicast address */
			if(_ah.smac[0] & 1)
				return;
			/* Check if it was us that was addressed */
			if(!IP_IS_SAME(_ah.dip, cfg.ipaddr))
				return;
			/* We need to respond */
			_eth.da[0] = _ah.dmac[0] = _ah.smac[0];	/* Set the destination to the original source */
			_eth.da[1] = _ah.dmac[1] = _ah.smac[1];
			_eth.da[2] = _ah.dmac[2] = _ah.smac[2];
			_eth.da[3] = _ah.dmac[3] = _ah.smac[3];
			_eth.da[4] = _ah.dmac[4] = _ah.smac[4];
			_eth.da[5] = _ah.dmac[5] = _ah.smac[5];
			IP_CP(_ah.dip, _ah.sip);
			_eth.sa[0] = _ah.smac[0] = cfg.macaddr[0];	/* Fill in our addresses */
			_eth.sa[1] = _ah.smac[1] = cfg.macaddr[1];
			_eth.sa[2] = _ah.smac[2] = cfg.macaddr[2];
			_eth.sa[3] = _ah.smac[3] = cfg.macaddr[3];
			_eth.sa[4] = _ah.smac[4] = cfg.macaddr[4];
			_eth.sa[5] = _ah.smac[5] = cfg.macaddr[5];
			IP_CP(_ah.sip, cfg.ipaddr);
			_ah.operation = htons(ARP_REPLY);
			/* We store the source of the request in our own arp-table, if there is room. */
			/* Apparently, they addressed *us*, fair to say we want to know them too. */
			arp_add(_eth.da, _ah.dip);
#ifdef DEBUG_ARP
			serial_puts("arp: sending reply\n");
			serial_flush_out();
#endif
			ethernet_txpacket(sizeof(_eth) + sizeof(_ah));
		} else if(_ah.operation == htons(ARP_REPLY)) {
#ifdef DEBUG_ARP
			serial_puts("arp: got reply\n");
#endif
#if 0
			/* NOTE: If we do this check, then we will not see broadcast arp replies.
			 * This can mess up GW changes and the like from hosts which send gratuitous
			 * arp replies to indicate change of MAC.
			 */
			/* Check if it was us that was addressed */
			if(!IS_SAME_IP(_ah.dip, cfg.ipaddr) || !IS_SAME_MAC(_ah.dmac, cfg.macaddr))
				return;
#endif
#ifdef DEBUG_ARP
			serial_puts("arp: storing mac/ip\n");
#endif
			arp_add(_ah.smac, _ah.sip);
		}
		/* Else we received a junk packet, just ignore */
	}
#ifdef DEBUG_ARP
	serial_flush_out();
#endif
}

void arp_add(uint8_t *mac, uint8_t *ip)
{
	uint8_t to = 255;
	uint8_t j = 255;
	uint8_t i;

	/* Ignore broad-/multi-cast and invalid IP ranges or not on our network */
	if(MAC_IS_MULTICAST(mac) || !IP_IS_VALID(ip) || IP_IS_MULTICAST(ip) || !IP_IS_SAME_MASKED(ip, cfg.ipaddr, cfg.nmaddr))
		return;

	/* Find an entry already used for ip */
	for(i = 0; i < MAXARPENTRIES; i++) {
		if(arptable[i].timeout && IP_IS_SAME(arptable[i].ip, ip)) {
			MAC_CP(arptable[i].mac, mac);
			arptable[i].timeout = ARPTIMEOUT;
			return;
		}
	}

	/* Find the entry with the lowest timeout we can reuse */
	for(i = 0; i < MAXARPENTRIES; i++) {
		if(arptable[i].timeout < to) {
			to = arptable[i].timeout;
			j = i;
		}
	}

	/* Only add it if we find an entry we can use */
	if(j != 255) {
#ifdef DEBUG_ARP
		serial_puts("arp: stored mac/ip\n");
#endif
		arptable[j].timeout = ARPTIMEOUT;
		IP_CP(arptable[j].ip, ip);
		MAC_CP(arptable[j].mac, mac);
	}
}

/* Resolve an IP address to a MAC address */
uint8_t arp_resolve(uint8_t *ip, uint8_t *mac)
{
	uint8_t i;
	uint8_t lip[4];
	if(IP_IS_MULTICAST(ip)) {
		/* This is a multicast target address */
		mac[0] = 0x01;
		mac[1] = 0x00;
		mac[2] = 0x5e;
		mac[3] = ip[1] & 0x7f;
		mac[4] = ip[2];
		mac[5] = ip[3];
		return 1;
	} else if(IP_IS_BROADCAST(ip)) {
		MAC_SET(mac, 0xff);
		return 1;
	}
	if(IP_IS_SAME_MASKED(ip, cfg.ipaddr, cfg.nmaddr))
		IP_CP(lip, ip);
	else
		IP_CP(lip, cfg.gwaddr);
	for(i = 0; i < MAXARPENTRIES; i++) {
		if(arptable[i].timeout && IP_IS_SAME(arptable[i].ip, lip)) {
			MAC_CP(mac, arptable[i].mac);
			return 1;
		}
	}
	return 0;
}

/* Send an ARP-request to resolve an IP */
void arp_whohas(uint8_t *ip)
{
	if(!IP_IS_VALID(ip) || IP_IS_MULTICAST(ip) || !IP_IS_VALID(cfg.ipaddr))
		return;
	MAC_CP(_eth.sa, cfg.macaddr);
	MAC_SET(_eth.da, 0xff);
	_eth.lenw = htons(ETHTYPE_ARP);
	_ah.htype = htons(ARP_HTYPE_ETH);
	_ah.ptype = htons(ETHTYPE_IP);
	_ah.hlen = ARP_HLEN_ETH;
	_ah.plen = ARP_PLEN_IP;
	_ah.operation = htons(ARP_REQUEST);
	MAC_CP(_ah.smac, cfg.macaddr);
	IP_CP(_ah.sip, cfg.ipaddr);
	MAC_SET(_ah.dmac, 0);
	if(IP_IS_SAME_MASKED(ip, cfg.ipaddr, cfg.nmaddr))
		IP_CP(_ah.dip, ip);
	else
		IP_CP(_ah.dip, cfg.gwaddr);
	ethernet_txpacket(sizeof(_eth) + sizeof(_ah));
}

void arp_dump_arptable(void)
{
	uint8_t i;
	serial_puts("MAC-address       IP address   Timeout\n");
	for(i = 0; i < MAXARPENTRIES; i++) {
		serial_puthex(arptable[i].mac[0]);	serial_putc(':');
		serial_puthex(arptable[i].mac[1]);	serial_putc(':');
		serial_puthex(arptable[i].mac[2]);	serial_putc(':');
		serial_puthex(arptable[i].mac[3]);	serial_putc(':');
		serial_puthex(arptable[i].mac[4]);	serial_putc(':');
		serial_puthex(arptable[i].mac[5]);	serial_putc(' ');
		serial_putdec(arptable[i].ip[0]);	serial_putc('.');
		serial_putdec(arptable[i].ip[1]);	serial_putc('.');
		serial_putdec(arptable[i].ip[2]);	serial_putc('.');
		serial_putdec(arptable[i].ip[3]);	serial_putc(' ');
		serial_putdec_u16(arptable[i].timeout);
		serial_putc('\r');
		serial_putc('\n');
	}
}

