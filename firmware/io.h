/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_IO_H
#define __504SEGCLOCK_IO_H

/* Leds:
 * - Inner yellow
 * - Outer green
 * Bottom two are ethernet state
 *
 * Led meanings:
 * - Yellow Left  : CPU activity
 * - Yellow Right : Error indicator
 * - Green Left   : IP address and link status
 * - Green Right  : Movement indicator
 */
#define io_led_ip(x)		do { PORTDbits.RD0 = ((x) ? 0 : 1); } while(0)
#define io_led_ip_toggle()	do { PORTDbits.RD0 ^= 1; } while(0)
#define io_led_ip_state()	(!PORTDbits.RD0)
#define io_led_move(x)		do { PORTBbits.RB4 = ((x) ? 0 : 1); } while(0)
#define io_led_move_toggle()	do { PORTBbits.RB4 ^= 1; } while(0)
#define io_led_move_state()	(!PORTBbits.RB4)
#define io_led_cpu(x)		do { PORTDbits.RD1 = ((x) ? 0 : 1); } while(0)
#define io_led_cpu_toggle()	do { PORTDbits.RD1 ^= 1; } while(0)
#define io_led_cpu_state()	(!PORTDbits.RD1)
#define io_led_err(x)		do { PORTDbits.RD2 = ((x) ? 0 : 1); } while(0)
#define io_led_err_toggle()	do { PORTDbits.RD2 ^= 1; } while(0)
#define io_led_err_state()	(!PORTDbits.RD2)

#define io_display_reset()	do { PORTAbits.RA4 = 0; PORTAbits.RA4 = 1; } while(0)
#define io_display_disable()	do { PORTAbits.RA4 = 0; } while(0)
#define io_display_enable()	do { PORTAbits.RA4 = 1; } while(0)

#define BUTTONMASK	0x0f
#define BUTTON_TICKS	20	/* Check every 20ms for button change */

#define BUTTON_SET	0x01
#define BUTTON_UP	0x02
#define BUTTON_DOWN	0x04
#define BUTTON_SELECT	0x08

void io_init(void);
void io_handle_timer_button(void);

#endif
