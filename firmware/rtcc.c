/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <rtcc.h>
#include <i2c.h>
#include <clock.h>
#include <serial.h>

#define RTCC_REG_CAL	0x08

static rtccdatetime_t datetime;

void rtcc_init(void)
{
	i2c_regs_read(0, sizeof(datetime), datetime.bytes);
	if(!datetime.st || !datetime.vbaten) {
		/* Setup rtcc if not configured */
		serial_puts("RTCC unconfigured, configuring to defaults\n");
		datetime.st      = 1;	/* Oscillator enable */
		datetime.vbaten  = 1;	/* Battery enable */
		datetime.sqwe    = 1;	/* MFP outputs calibrartion */
		datetime.extosc  = 0;	/* Xtal oscillator */
		datetime.rs      = 4;	/* Calibration output mode on MFP (64Hz) */
		datetime.hourfmt = 0;	/* 24 hour mode */
		datetime.year10  = 1;	/* Setup to the 504 segment clock epoch (20)12-02-15 12:00:00 UTC */
		datetime.year1   = 2;
		datetime.mon10   = 0;
		datetime.mon1    = 2;
		datetime.mday10  = 1;
		datetime.mday1   = 5;
		datetime.wday    = 4;
		datetime.hour10  = 1;
		datetime.hour1   = 2;
		datetime.min10   = 0;
		datetime.min1    = 0;
		datetime.sec10   = 0;
		datetime.sec1    = 0;
		i2c_regs_write(0, sizeof(datetime), datetime.bytes);
		clock_secs = 1329307200UL;	/* The 504 segment clock epoch in UTC */
	}
	/* Calculate the system time */
	clock_timeparts.sec  = datetime.sec10 * 10  + datetime.sec1;
	clock_timeparts.min  = datetime.min10 * 10  + datetime.min1;
	clock_timeparts.hour = datetime.hour10 * 10 + datetime.hour1;
	clock_timeparts.mday = datetime.mday10 * 10 + datetime.mday1;
	clock_timeparts.mon  = datetime.mon10 * 10  + datetime.mon1 - 1;
	clock_timeparts.year = datetime.year10 * 10 + datetime.year1 + 2000;
	clock_mktime();		/* And set system clock epoch from RTC */
}

uint8_t rtcc_cal_read(void)
{
	return i2c_reg_read(RTCC_REG_CAL);
}

void rtcc_cal_write(uint8_t cal)
{
	return i2c_reg_write(RTCC_REG_CAL, cal);
}

void rtcc_read(rtccdatetime_t *dt)
{
	i2c_regs_read(0, sizeof(*dt), &dt->bytes[0]);
}

void rtcc_write_now(void)
{
	clock_localtime(1);
	i2c_regs_read(0, sizeof(datetime), datetime.bytes);
	datetime.year10 = (clock_timeparts.year / 10) % 10;
	datetime.year1  = clock_timeparts.year % 10;
	datetime.mon10  = (clock_timeparts.mon+1) / 10;
	datetime.mon1   = (clock_timeparts.mon+1) % 10;
	datetime.mday10 = clock_timeparts.mday / 10;
	datetime.mday1  = clock_timeparts.mday % 10;
	datetime.hour10 = clock_timeparts.hour / 10;
	datetime.hour1  = clock_timeparts.hour % 10;
	datetime.min10  = clock_timeparts.min / 10;
	datetime.min1   = clock_timeparts.min % 10;
	datetime.sec10  = clock_timeparts.sec / 10;
	datetime.sec1   = clock_timeparts.sec % 10;
	datetime.wday   = clock_timeparts.wday + 1;
	i2c_regs_write(0, sizeof(datetime), datetime.bytes);
}

