/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_DHCP_H
#define __504SEGCLOCK_DHCP_H

#include <ethernet.h>
#include <ip.h>
#include <udp.h>

#define DHCPTIMER_TICKS			1000	/* Dhcp does lease contdown on second basis */
#define DHCPTIMER_STARTTICKS		 100	/* First timer interval should be faster */
#define DHCPTIMER_SELECTINGTICKS	5000	/* Max wait for offer after sending discover */
#define DHCPTIMER_REQUESTINGTICKS	5000	/* Max wait for ack/nak after sending request */
#define DHCPTIMER_NAKTICKS		10000	/* Wait after we receive a DHCPNAK to retry procedure */

/* These intervals *must* be below the arp timeout
 * otherwise we'd see only arp queries and never
 * any renewals.
 */
#define DHCP_RENEWING_INTERVAL		120	/* Retry every 120 seconds (RFC minimum is 60) */
#define DHCP_REBINDING_INTERVAL		60	/* Retry every 60 seconds (RFC minimum is 60) */

#define DHCPOPT_NOP		0	/* 0: Filler byte for alignment */
#define DHCPOPT_NETMASK		1	/* 4: netmask */
#define DHCPOPT_TIMEOFFSET	2	/* 4: offset from UTC in seconds in 2's complement */
#define DHCPOPT_GATEWAY		3	/* n*4: IP address of gateway */
#define DHCPOPT_TIMESERVER	4	/* n*4: IP address(es) of time servers */
#define DHCPOPT_NAMESERVER	5	/* n*4: IP address(es) of name servers */
#define DHCPOPT_DNS		6	/* n*4: IP address(es) of DNS servers */
#define DHCPOPT_LOGSERVER	7	/* n*4: IP address(es) of MIT-LCS Log servers */
#define DHCPOPT_HOSTNAME	12	/* n: string */
#define DHCPOPT_DOMAINNAME	15	/* n: string */
#define DHCPOPT_BROADCAST	28	/* 4: Broadcast IP address */
#define DHCPOPT_REQADDR		50	/* 4: Requested IP address */
#define DHCPOPT_LEASETIME	51	/* 4: seconds */
#define DHCPOPT_OVERLOAD	52	/* 1: Overload sname/file fields */
#define DHCPOPT_DHCPMSG		53	/* 1: Message type */
#define DHCPOPT_SERVERID	54	/* 4: IP address of server */
#define DHCPOPT_PARMRQLIST	55	/* n: list of DHCPOPT_* IDs requested by client */
#define DHCPOPT_MESSAGE		56	/* n: error string */
#define DHCPOPT_MAXOPTLEN	57	/* 2: Maximum size of the DHCP packet (>= 576) */
#define DHCPOPT_RENEWALTIME	58	/* 4: seconds */
#define DHCPOPT_REBINDINGTIME	59	/* 4: seconds */
#define DHCPOPT_END		255	/* 0: End of options marker  */

#define DHCPMSG_DISCOVER	1	/* Cl -> Se */
#define DHCPMSG_OFFER		2	/* Se -> Cl */
#define DHCPMSG_REQUEST		3	/* Cl -> Se */
#define DHCPMSG_DECLINE		4	/* Cl -> Se */
#define DHCPMSG_ACK		5	/* Se -> Cl */
#define DHCPMSG_NAK		6	/* Se -> Cl */
#define DHCPMSG_RELEASE		7	/* Cl -> Se */
#define DHCPMSG_INFORM		8	/* Cl -> Se */

#define DHCP_MAGIC_COOKIE	0x63538263

/* DHCP State machine states */
enum {
	DHCPSTATE_OFF,		/* When we use static IP */
	DHCPSTATE_INIT_REBOOT,	/* Not used, we go directly to INIT */
	DHCPSTATE_REBOOTING,	/* Not used, we go directly to INIT */
	DHCPSTATE_INIT,
	DHCPSTATE_SELECTING,
	DHCPSTATE_REQUESTING,
	DHCPSTATE_REBINDING,
	DHCPSTATE_BOUND,
	DHCPSTATE_RENEWING
};

enum {
	BOOTP_REQUEST = 1,
	BOOTP_REPLY = 2
};

/* See RFC 2131 */
typedef struct __dhcphdr_t {
	uint8_t		op;
	uint8_t		htype;
	uint8_t		hlen;
	uint8_t		hops;
	uint32_t	xid;
	uint16_t	secs;
	uint16_t	flags;
	uint8_t		ciaddr[4];
	uint8_t		yiaddr[4];
	uint8_t		siaddr[4];
	uint8_t		giaddr[4];
	uint8_t		chaddr[16];
	uint8_t		sname[64];
	uint8_t		file[128];
	uint32_t	magic_cookie;
} dhcphdr_t;

typedef struct __dhcppkt_t {
	ethhdr_t	eth;
	iphdr_t		ip;
	udphdr_t	udp;
	dhcphdr_t	dhcp;
	uint8_t		options[];
} dhcppkt_t;

extern uint32_t leasetime;
extern uint32_t renewaltime;
extern uint32_t rebindingtime;

void dhcp_init(void);
void dhcp_handle_timer(void);
uint8_t dhcp_handle_rxpacket(uint16_t pktlen);
void dhcp_linkchange(void);
void dhcp_toggle(void);

#endif
