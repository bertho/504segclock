/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_IP_H
#define __504SEGCLOCK_IP_H

#define IP_VERSION_IPV4	4

#define IPPROTO_ICMP	 1
#define IPPROTO_TCP	 6
#define IPPROTO_UDP	17

#define IP_TTL_DEFAULT	64

/* Find the size of the IP header: ip.hdrlen * sizeof(uint32_t) */
#define iphdr_size(ipp)		((uint8_t)((ipp)->hdrlen) << 2)
#define ip_network_available()	(ethlink == ETH_LINK_UP && IP_IS_VALID(cfg.ipaddr))


/* Eth type 0x0800 */
typedef struct __iphdr_t {
	uint8_t		hdrlen		:4;
	uint8_t		version		:4;
	uint8_t		dscp_ecn;
	uint16_t	len;
	uint16_t	id;
	uint8_t		fo_lo		:5;
	uint8_t		flag_mf		:1;
	uint8_t		flag_df		:1;
	uint8_t		flag_res	:1;
	uint8_t		fo_hi;
	uint8_t		ttl;
	uint8_t		proto;
	uint16_t	checksum;
	uint8_t		saddr[4];
	uint8_t		daddr[4];
} iphdr_t;

void ip_init(void);
void ip_handle_rxpacket(uint16_t pktlen);
uint16_t ip_checksum(iphdr_t *p, uint16_t len);

#endif
