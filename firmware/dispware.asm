; vim: syn=pic
;
; 504 Segment Clock - Display driver
; Copyright (C) 2012  B. Stultiens
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;
	list
	errorlevel	1
	radix		dec

	include		"p16f1519.inc"
	__CONFIG _CONFIG1, _FOSC_INTOSC & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _CP_OFF & _BOREN_OFF & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF
	__CONFIG _CONFIG2, _WRT_OFF & _VCAPEN_OFF & _STVREN_ON & _BORV_LO & _LPBOR_OFF & _LVP_ON
	__CONFIG _IDLOC0, 'B'
	__CONFIG _IDLOC1, 'S'
	__CONFIG _IDLOC2, 'C'
	__CONFIG _IDLOC3, 'K'

;
;------------------------------------------------------------------------------
; Segments:
;   ---A---
;  |       |
;  F       B
;  |       |
;   ---G---
;  |       |
;  E       C
;  |       |
;   ---D---  (P)
;
; PortA
;	0 - COM4
;	1 - COM5
;	2 - DP2
;	3 - C2
;	4 - G2
;	5 - spi: nSS
;	6 - F3
;	7 - G3
; PortB
;	0 - A1
;	1 - E1
;	2 - G1
;	3 - F1
;	4 - COM6
;	5 - nLATCH
;	6 - E2
;	7 - D2
; PortC
;	0 - A3
;	1 - COM1
;	2 - B3
;	3 - spi: SCK
;	4 - spi: SDI
;	5 - spi: SDO
;	6 - D3
;	7 - COM3
; PortD
;	0 - COM2
;	1 - E3
;	2 - DP3
;	3 - C3
;	4 - DP1
;	5 - C1
;	6 - D1
;	7 - B1
; PortE
;	0 - F2
;	1 - A2
;	2 - B2
;	3 - reset
;------------------------------------------------------------------------------
;
DISPSECTSIZE	=	3*8		; Three displays are driven at once with 7+1 segments
DISPSTORESIZE	=	6*DISPSECTSIZE	; Six sections of 3 displays
LINEAR_START	=	0x2000		; Start of linear RAM space

; The following is in the shared space for the interrupt service routine
; and fast access to state variables
isr_cnt		=	0x70	; ISR: # of received SPI bytes
pa_bits		=	0x71	; Port A holder store
pb_bits		=	0x72	; Port B holder store
pc_bits		=	0x73	; Port C holder store
pd_bits		=	0x74	; Port D holder store
pe_bits		=	0x75	; Port E holder store
cnt_1		=	0x76	; Display PWM 1 counter
cnt_2		=	0x77	; Display PWM 2 counter
cnt_3		=	0x78	; Display PWM 3 counter
flags		=	0x79	; Flags for backstore and junk
su_state	=	0x7a	; Startup state
su_cnt		=	0x7b	; Startup temp var
su_iv		=	0x7c	; Startup interval counter

SU_INTERVAL	=	10

BF_STARTUP	=	0	; Set when startup completed
BF_BACKSTORE	=	1	; Backstore indicator bit

; Running variables in linear space
disp_data	=	LINEAR_START
disp_cpy	=	(disp_data + DISPSTORESIZE + 0xff) & ~0xff
disp_dummy	=	disp_cpy + DISPSTORESIZE

; Segment bit asssignment
B_A1		=	0
B_B1		=	7
B_C1		=	5
B_D1		=	6
B_E1		=	1
B_F1		=	3
B_G1		=	2
B_P1		=	4

B_A2		=	1
B_B2		=	2
B_C2		=	3
B_D2		=	7
B_E2		=	6
B_F2		=	0
B_G2		=	4
B_P2		=	2

B_A3		=	0
B_B3		=	2
B_C3		=	3
B_D3		=	6
B_E3		=	1
B_F3		=	6
B_G3		=	7
B_P3		=	2

; Segment port asssignment
P_A1		=	pb_bits
P_B1		=	pd_bits
P_C1		=	pd_bits
P_D1		=	pd_bits
P_E1		=	pb_bits
P_F1		=	pb_bits
P_G1		=	pb_bits
P_P1		=	pd_bits

P_A2		=	pe_bits
P_B2		=	pe_bits
P_C2		=	pa_bits
P_D2		=	pb_bits
P_E2		=	pb_bits
P_F2		=	pe_bits
P_G2		=	pa_bits
P_P2		=	pa_bits

P_A3		=	pc_bits
P_B3		=	pc_bits
P_C3		=	pd_bits
P_D3		=	pc_bits
P_E3		=	pd_bits
P_F3		=	pa_bits
P_G3		=	pa_bits
P_P3		=	pd_bits

; Common line bit assignment
B_COM1		=	1
B_COM2		=	0
B_COM3		=	7
B_COM4		=	0
B_COM5		=	1
B_COM6		=	4
B_LATCH		=	5

; Common line port assignment
P_COM1		=	PORTC
P_COM2		=	PORTD
P_COM3		=	PORTC
P_COM4		=	PORTA
P_COM5		=	PORTA
P_COM6		=	PORTB
P_LATCH		=	PORTB

; Port I/O setting
PA_TRIS		=	0x20
PB_TRIS		=	0x20
PC_TRIS		=	0x18
PD_TRIS		=	0x00
PE_TRIS		=	0xf8

; Off state port values
PA_OFF		=	0xdc
PB_OFF		=	0xcf
PC_OFF		=	0x45
PD_OFF		=	0xfe
PE_OFF		=	0x07

; SPI port definitions
B_SCK		=	3
B_SDI		=	4
B_SDO		=	5
B_SS		=	5

P_SCK		=	PORTC
P_SDI		=	PORTC
P_SDO		=	PORTC
P_SS		=	PORTA

T_SCK		=	TRISC
T_SDI		=	TRISC
T_SDO		=	TRISC
T_SS		=	TRISA

PWMSTEPS	=	64


; Set a bit in the port holders (4)
setbit		macro	cntr, port, bit
		moviw	FSR0++ 
		subwf	cntr, w
		btfss	STATUS, C
		bcf	port, bit	; Active low
		endm

; Recalculate the port holders (10 + 24*4 + 12 = 118)
setport		macro	oldcomport, oldcombit, comport, combit
		; resetport
		movlw	PA_OFF
		movwf	pa_bits
		movlw	PB_OFF
		movwf	pb_bits
		movlw	PC_OFF
		movwf	pc_bits
		movlw	PD_OFF
		movwf	pd_bits
		movlw	PE_OFF
		movwf	pe_bits

		setbit	cnt_1, P_A1, B_A1
		setbit	cnt_1, P_B1, B_B1
		setbit	cnt_1, P_C1, B_C1
		setbit	cnt_1, P_D1, B_D1
		setbit	cnt_1, P_E1, B_E1
		setbit	cnt_1, P_F1, B_F1
		setbit	cnt_1, P_G1, B_G1
		setbit	cnt_1, P_P1, B_P1

		setbit	cnt_2, P_A2, B_A2
		setbit	cnt_2, P_B2, B_B2
		setbit	cnt_2, P_C2, B_C2
		setbit	cnt_2, P_D2, B_D2
		setbit	cnt_2, P_E2, B_E2
		setbit	cnt_2, P_F2, B_F2
		setbit	cnt_2, P_G2, B_G2
		setbit	cnt_2, P_P2, B_P2

		setbit	cnt_3, P_A3, B_A3
		setbit	cnt_3, P_B3, B_B3
		setbit	cnt_3, P_C3, B_C3
		setbit	cnt_3, P_D3, B_D3
		setbit	cnt_3, P_E3, B_E3
		setbit	cnt_3, P_F3, B_F3
		setbit	cnt_3, P_G3, B_G3
		setbit	cnt_3, P_P3, B_P3

		; Deactivate previous output
		bcf	oldcomport, oldcombit
		; Set the output ports
		movf	pa_bits, w
		movwf	PORTA
		movf	pb_bits, w
		movwf	PORTB
		movf	pc_bits, w
		movwf	PORTC
		movf	pd_bits, w
		movwf	PORTD
		movf	pe_bits, w
		movwf	PORTE
		; Activate the output
		bsf	comport, combit
		endm

storetable	macro	base
		retlw	LOW(base+0)
		retlw	LOW(base+1)
		retlw	LOW(base+2)
		retlw	LOW(base+3)
		retlw	LOW(base+4)
		retlw	LOW(base+5)
		retlw	LOW(base+6)
		retlw	LOW(base+7)
		endm

cpysegments	macro
		moviw	FSR1++		; Copy 8 segments
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		moviw	FSR1++
		movwi	FSR0++
		endm

cpydisplay		macro
		cpysegments		; Copy 18 displays
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		endm
;
;------------------------------------------------------------------------------
; Reset Entry Point
;------------------------------------------------------------------------------
;
		org	0x00
_reset
		clrf	INTCON			; Disable all interrupts
		banksel	0
		goto	_main
		nop

;
;------------------------------------------------------------------------------
; Interrupt service routine
; The isr runs on receive of SPI data
;------------------------------------------------------------------------------
;
		org	0x04
_isr
						; (3..5)
		bcf	flags, BF_BACKSTORE	; Setup next copy at latch

		movf	isr_cnt, w		; Calculate storage address
		call	_isr_storetable
		movwf	FSR1L			; (9)

		banksel	SSPBUF
		movf	SSPBUF, w		; Get the data
		movwf	INDF1			; Store
		incf	isr_cnt			; Move to next store position
						; (4)
		banksel	PIR1
		bcf	PIR1, SSPIF		; Clear SPI interrupt flag
						; (2)
		retfie				; (2)

		; ISR timing: 3..5 + 9 + 4 + 2 + 2 = 20..22
		; Max continuous SPI clock = 1.45 MHz
		; Fastest update: 144*8/1.45MHz ~ 792 us (~1262 changes per second)

;
;------------------------------------------------------------------------------
; Main entry point
;------------------------------------------------------------------------------
;
_main
		; Setup default location offsets
		movlw	HIGH(disp_data)
		movwf	FSR0H
		movlw	HIGH(disp_cpy)
		movwf	FSR1H

		banksel	OSCCON
		movlw	(1<<IRCF3)|(1<<IRCF2)|(1<<IRCF1)|(1<<IRCF0)|(1<<SCS1)
		movwf	OSCCON			; Setup 16MHz internal oscillator

		btfss	OSCSTAT, HFIOFR		; Wait for HFINTOSC ready
		goto	$-1
		btfss	OSCSTAT, HFIOFS		; and stable
		goto	$-1

		banksel	PORTA
		movlw	PA_OFF			; All LEDs off
		movwf	PORTA
		movlw	PB_OFF
		movwf	PORTB
		movlw	PC_OFF
		movwf	PORTC
		movlw	PD_OFF
		movwf	PORTD
		movlw	PE_OFF
		movwf	PORTE

		banksel	ANSELA			; Select all digital I/O
		clrf	ANSELA
		clrf	ANSELB
		clrf	ANSELC
		clrf	ANSELD
		clrf	ANSELE

		banksel	TRISA			; Set I/O mode
		movlw	PA_TRIS
		movwf	TRISA
		movlw	PB_TRIS
		movwf	TRISB
		movlw	PC_TRIS
		movwf	TRISC
		movlw	PD_TRIS
		movwf	TRISD
		movlw	PE_TRIS
		movwf	TRISE

		banksel	PIE1
		movlw	(1<<SSPIE)		; Enable SPI interrupt
		movwf	PIE1
		banksel	SSPSTAT
		clrf	SSPSTAT			; Sample on falling edge
		; SPI: CKP = 0, CKE = 0
		movlw	(1<<SSPEN)|(1<<SSPM2)	; SPI enable, clk-idle = low, slave with nSS
		movwf	SSPCON1

		; Note: the counter interleave must be > 1 because the
		;       increments are distributed. Otherwise the interleave
		;       would be gone at some points.
		clrf	cnt_1			; Setup PWM counters (interleaved)
		movlw	265*1/3	;2*(256/PWMSTEPS)
		movwf	cnt_2
		movlw	256*2/3	;4*(256/PWMSTEPS)
		movwf	cnt_3

		clrf	flags			; Fresh start
		clrf	su_state
		movlw	SU_INTERVAL
		movwf	su_iv

		movlw	DISPSTORESIZE		; Boot time clear display
		movwf	su_cnt
		clrf	FSR0L
		movlw	0
_boot_clr
		movwi	FSR0++
		decfsz	su_cnt, f
		goto	_boot_clr

		banksel	0			; We need to reach the PORTx addresses
		clrf	PIR1			; Clear any hanging SPI interrupt
		movlw	(1<<GIE)|(1<<PEIE)
		movwf	INTCON			; Enable interrupts

		; PWM 64 steps results in:
		; (16MHz / 4) / ((118 + 2 + 35) * 6 * 64) ~ 67.2 Hz
		; Segment change overhead: 11 / (118 + 2 + 35) ~ 7.1%
		goto	_loopentry

;------------------------------------------------------------------------------

_mainloop
		btfss	flags, BF_STARTUP
		call	_startup_show
		call	_smaller_delay		; needs to be 2 cycles less because of the bottom goto
		setport	P_COM3, B_COM3, P_COM4, B_COM4

		btfss	P_LATCH, B_LATCH	; Reset the SPI data counter on latch
		call	_cpy_disp_data		; and copy the backing store
		call	_small_delay
		setport	P_COM4, B_COM4, P_COM5, B_COM5

		movlw	(256/PWMSTEPS)
		addwf	cnt_1, f
		call	_small_delay
		setport	P_COM5, B_COM5, P_COM6, B_COM6

_loopentry
		movlw	LOW(disp_data)		; Setup data pointer
		movwf	FSR0L
		call	_small_delay
		setport	P_COM6, B_COM6, P_COM1, B_COM1

		movlw	(256/PWMSTEPS)
		addwf	cnt_2, f
		call	_small_delay
		setport	P_COM1, B_COM1, P_COM2, B_COM2

		movlw	(256/PWMSTEPS)
		addwf	cnt_3, f
		call	_small_delay
		setport	P_COM2, B_COM2, P_COM3, B_COM3

		goto	_mainloop
;
;------------------------------------------------------------------------------
; small  : Delay 35 cycles (including call and return)
; smaller: Delay 33 cycles (including call and return)
;------------------------------------------------------------------------------
;
_small_delay				; 2 (entry)
		nop			; 1
		nop			; 1
_smaller_delay
		nop			; 1
		nop			; 1
		movlw	9		; 1
		decfsz	WREG, w		; 8*3 + 2
		goto	$-1
		return			; 2
;
;------------------------------------------------------------------------------
; Copy the backing storage to the display The PWM cycle is >14ms, but the
; individual PWM updates are about 223us apart. Therefore, using a backing
; store enhances the snappiness of the updates in both in- and de-creasing
; intensities as it takes ~75.5us to copy the data. Also, it prevents slow
; updates from being annoying (when the SPI clock is slow or byte intervals
; are large).
; The copying is loop-unrolled to make it faster.
;------------------------------------------------------------------------------
;
_cpy_disp_data
		btfsc	flags, BF_BACKSTORE	; No need to do it again if done
		return

		movlw	LOW(disp_data)		; Copy backup data
		movwf	FSR0L
		movlw	LOW(disp_cpy)
		movwf	FSR1L
		cpydisplay			; *FSR0 = *FSR1 for all data
		movlw	LOW(disp_data + 4*DISPSECTSIZE)	; Re-setup FSR0, FSR1 will get set in the ISR
		movwf	FSR0L
		bsf	flags, BF_BACKSTORE
		clrf	isr_cnt			; Reset storage counter for next SPI data
		return
;
;------------------------------------------------------------------------------
; Show some kind of activity at startup. All segments are set to light in all
; possible intensities.
; Just to show-off...
;------------------------------------------------------------------------------
;
_startup_show
		banksel	PORTA
		btfss	P_LATCH, B_LATCH	; Quit startup if latch active
		goto	_startup_done

		movf	cnt_1, f		; Only do this once every full PWM cycle
		btfsc	STATUS, Z
		return

		decfsz	su_iv, f
		return

		movlw	SU_INTERVAL
		movwf	su_iv

		incf	su_state, f		; Done when we have a full state cycle
;		btfsc	STATUS, Z		; This gives us 256/67.2Hz ~ 3.8 seconds show
;		goto	_startup_done

		movlw	7
		decfsz	WREG, f
		goto	$-1

; FIXME: this does not work properly
		bcf	P_COM3, B_COM3		; Shut down to compensate intensity

		movlw	DISPSTORESIZE
		movwf	su_cnt
if 1
_startup_loop
		movf	su_cnt, w
		addlw	-1
		;call	_isr_storetable
		call	_su_segtable
		movwf	FSR0L
		movf	su_state, w
		addwf	su_cnt, w
		rlf	WREG, w
		btfsc	STATUS, C
		;comf	WREG, w
		movwf	INDF0
		decfsz	su_cnt, f
		goto	_startup_loop
else
		clrf	FSR0L
		movf	su_state, w
		addwf	su_state, w
		addwf	su_cnt, w
		addwf	su_cnt, w
_startup_loop
		movwi	FSR0++
		decfsz	su_cnt, f
		goto	_startup_loop
endif
		movlw	LOW(disp_data + 3*DISPSECTSIZE)	; Restore FSR0
		movwf	FSR0L
		return

_startup_done
		movlw	DISPSTORESIZE		; Startup done, clear the display
		movwf	su_cnt
		clrf	FSR0L
		movlw	0
_startup_clr
		movwi	FSR0++
		decfsz	su_cnt, f
		goto	_startup_clr

		movlw	LOW(disp_data + 3*DISPSECTSIZE)	; Restore FSR0
		movwf	FSR0L
		bsf	flags, BF_STARTUP	; Set done flag
		return
;
;------------------------------------------------------------------------------
; Storage lookup table for making the segments align nicely in the SPI
; datastream.
;------------------------------------------------------------------------------
_isr_storetable
		brw
_isr_storetable_start
		storetable	disp_cpy+0	; Inner three displays
		storetable	disp_cpy+8
		storetable	disp_cpy+16

		storetable	disp_cpy+24	; Middle six displays
		storetable	disp_cpy+48
		storetable	disp_cpy+32
		storetable	disp_cpy+56
		storetable	disp_cpy+40
		storetable	disp_cpy+64

		storetable	disp_cpy+72	; Outer nine displays
		storetable	disp_cpy+96
		storetable	disp_cpy+120
		storetable	disp_cpy+80
		storetable	disp_cpy+104
		storetable	disp_cpy+128
		storetable	disp_cpy+88
		storetable	disp_cpy+112
		storetable	disp_cpy+136

		storetable	disp_dummy	; Fill to 32 entries of 8 bytes to make 0x100 size
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy

seg_innertable	macro	seg
		retlw		LOW(disp_cpy+seg+16)
		retlw		LOW(disp_cpy+seg+8)
		retlw		LOW(disp_cpy+seg+0)
		endm

seg_middletable	macro	seg
		retlw		LOW(disp_cpy+seg+64)
		retlw		LOW(disp_cpy+seg+40)
		retlw		LOW(disp_cpy+seg+56)
		retlw		LOW(disp_cpy+seg+32)
		retlw		LOW(disp_cpy+seg+48)
		retlw		LOW(disp_cpy+seg+24)
		endm

seg_outertable	macro	seg
		retlw		LOW(disp_cpy+seg+136)
		retlw		LOW(disp_cpy+seg+112)
		retlw		LOW(disp_cpy+seg+88)
		retlw		LOW(disp_cpy+seg+128)
		retlw		LOW(disp_cpy+seg+104)
		retlw		LOW(disp_cpy+seg+80)
		retlw		LOW(disp_cpy+seg+120)
		retlw		LOW(disp_cpy+seg+96)
		retlw		LOW(disp_cpy+seg+72)
		endm

_su_segtable
		brw
_su_segtable_start
		seg_outertable	0	; A
		seg_outertable	5	; F
		seg_outertable	1	; B
		seg_outertable	6	; G
		seg_outertable	4	; E
		seg_outertable	2	; C
		seg_outertable	3	; D
		seg_outertable	7	; P

		seg_middletable	0	; A
		seg_middletable	5	; F
		seg_middletable	1	; B
		seg_middletable	6	; G
		seg_middletable	4	; E
		seg_middletable	2	; C
		seg_middletable	3	; D
		seg_middletable	7	; P

		seg_innertable	0	; A
		seg_innertable	5	; F
		seg_innertable	1	; B
		seg_innertable	6	; G
		seg_innertable	4	; E
		seg_innertable	2	; C
		seg_innertable	3	; D
		seg_innertable	7	; P
		end
