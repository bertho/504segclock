/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <pic18fregs.h>
#include <clock.h>
#include <config.h>
#include <io.h>
/*
 * Timer 4 scaling for 1000Hz at 41.667MHz
 * pre post         preset freq      diff[%]  correction
 *  4   11  236.7424 (236) 1003.1459 -0.3136   -318.878 
 *  4   12  217.0139 (217) 1000.0640 -0.0064 -15625.000  a
 *  4   13  200.3205 (200) 1001.6026 -0.1600   -625.000  b
 *  4   14  186.0119 (186) 1000.0640 -0.0064 -15625.000  a
 *  4   15  173.6111 (173) 1003.5324 -0.3520   -284.091 
 *  4   16  162.7604 (162) 1004.6939 -0.4672   -214.041 
 * 16    3  217.0139 (217) 1000.0640 -0.0064 -15625.000  a
 * 16    4  162.7604 (162) 1004.6939 -0.4672   -214.041 
 * 16    5  130.2083 (130) 1001.6026 -0.1600   -625.000  b
 * 16    6  108.5069 (108) 1004.6939 -0.4672   -214.041 
 * 16    7   93.0060 ( 93) 1000.0640 -0.0064 -15625.000  a ***
 * 16    8   81.3802 ( 81) 1004.6939 -0.4672   -214.041 
 * 16    9   72.3380 ( 72) 1004.6939 -0.4672   -214.041 
 * 16   10   65.1042 ( 65) 1001.6026 -0.1600   -625.000  b
 * 16   11   59.1856 ( 59) 1003.1459 -0.3136   -318.878 
 * 16   12   54.2535 ( 54) 1004.6939 -0.4672   -214.041 
 * 16   13   50.0801 ( 50) 1001.6026 -0.1600   -625.000  b
 * 16   14   46.5030 ( 46) 1010.9343 -1.0816    -92.456 
 * 16   15   43.4028 ( 43) 1009.3669 -0.9280   -107.759 
 * 16   16   40.6901 ( 40) 1017.2526 -1.6960    -58.962 
 *
 * a) At -64/1000000 we need to subtract one every 15625
 * b) at -1600/1000000 we need to subtract one every 625
 */

#define clock_countdown(x)	do { if(clock_timers[x].state == TIMER_RUNNING) { \
					if(!--clock_timers[x].cnt) \
						clock_timers[x].state = TIMER_EXPIRED; \
					} \
				} while(0)

volatile clock_timer_t clock_timers[MAXTIMERS];
static uint16_t clock_uptime_msecs;
volatile uint32_t clock_uptime;			/* Seconds since boot ~136.19 years */
volatile uint32_t clock_secs;			/* Seconds unix epoch */
volatile uint16_t clock_msecs;			/* Millisecond resolution [0..999] */
static uint16_t clock_correction;		/* Clock skew correction counter */
volatile entropy_t clock_entropy;
timeparts_t clock_timeparts;

void clock_init(void)
{
	/*clock_entropy = 0;*/	/* Do no init, entropy sucks in embedded systems */
	clock_uptime_msecs = 1000;
	clock_secs = 0;
	clock_msecs = 0;
	clock_correction = 15625;
	clock_uptime = 0;
	memset(clock_timers, 0, sizeof(clock_timers));
	PR4 = 93-1;
	TMR4 = 0;
	PIR3bits.TMR4IF = 0;
	T4CON = 0x36;	/* Pre 1:16, TMR on, Post 1:7 */
	PIE3bits.TMR4IE = 1;
}

void clock_isr(void)
{
	PIR3bits.TMR4IF = 0;
	if(!--clock_correction) {
		/* The clock runs slightly too fast; correct it here. */
		clock_correction = 15625;
		return;
	}
	if(++clock_msecs == 1000) {
		clock_msecs = 0;
		clock_secs++;
	}

	/* Uptime must be independent because of ntp corrections */
	if(!--clock_uptime_msecs) {
		clock_uptime_msecs = 1000;
		clock_uptime++;
	}
#if 0
	uint8_t i;
	for(i = 0; i < MAXTIMERS; i++) {
		if(clock_timers[i].state == TIMER_RUNNING) {
			if(!--clock_timers[i].cnt)
				clock_timers[i].state = TIMER_EXPIRED;
		}
	}
#else
	/* Loop-unroll is faster */
	clock_countdown(TIMER_BUTTON);
	clock_countdown(TIMER_ADC);
	clock_countdown(TIMER_ADC_BC);
	clock_countdown(TIMER_SPI);
	clock_countdown(TIMER_DISPLAY);
	clock_countdown(TIMER_GREEN);
	clock_countdown(TIMER_ARP);
	clock_countdown(TIMER_WHEEL);
	clock_countdown(TIMER_ETHTX);
	clock_countdown(TIMER_DHCP);
	clock_countdown(TIMER_I2C);
	clock_countdown(TIMER_NTP);
	clock_countdown(TIMER_MOVE);
	clock_countdown(TIMER_REMOTE);
	clock_countdown(TIMER_CONFIG);
#endif

	/* See errata: transmitter deadlock on duplex mismatch */
	if(clock_timers[TIMER_ETHTX].state == TIMER_EXPIRED) {
		if(ECON1bits.TXRTS) {
			/* The transmitter is still busy; perform logic reset and try again */
			ECON1bits.TXRST = 1;
			ECON1bits.TXRST = 0;
			/* delay 1.6us */
			__asm
			movlw	6
clock_ethtx_delayloop:
			addlw	255		; w--
			bnz	clock_ethtx_delayloop	; 4/41.667 * 3 * 6 ~1.72us
			__endasm;
			ECON1bits.TXRTS = 1;
			/* Reset the timer to check again in 3ms */
			clock_timers[TIMER_ETHTX].state = TIMER_RUNNING;
			clock_timers[TIMER_ETHTX].cnt = 3;
		} else
			clock_timers[TIMER_ETHTX].state = TIMER_IDLE;
	}
}

#define JULIAN_1_1_1970	2440588		/* The 2440588 is julian date for 1-1-1970 */

void clock_localtime(uint8_t useutc)
{
	int32_t i;
	uint32_t epoch;

	cli();
	epoch = clock_secs;
	clock_timeparts.msec = clock_msecs;
	sti();
	clock_timeparts.sec = epoch % 60;
	epoch /= 60;
	if(!useutc)
		epoch += cfg.tzoffset;
	clock_timeparts.min = epoch % 60;
	epoch /= 60;
	clock_timeparts.hour=epoch % 24;
	epoch /= 24;

	/* epoch is now in days since 1/1 1970 */
	clock_timeparts.wday = (epoch + 4) % 7;

	/* Use julian date calculation */
	epoch += JULIAN_1_1_1970 - 1721119;
	clock_timeparts.year = (4 * epoch - 1) / 146097;
	epoch = 4 * epoch - 1 - 146097 * clock_timeparts.year;
	i = epoch / 4;
	epoch = (4 * i + 3) / 1461;
	i = ((4 * i + 3 - 1461 * epoch) + 4) / 4;
	clock_timeparts.mon = (5 * i - 3) / 153;
	clock_timeparts.mday = ((5 * i - 3 - 153 * clock_timeparts.mon) + 5) / 5;
	clock_timeparts.year = 100 * clock_timeparts.year + epoch;
	if(clock_timeparts.mon < 10)
		clock_timeparts.mon += 3-1;
	else {
		clock_timeparts.mon -= 9+1;
		clock_timeparts.year++;
	}
}

void clock_mktime(void)
{
	uint32_t y = clock_timeparts.year;
	uint32_t m = clock_timeparts.mon + 1;
	uint32_t epoch;

	/* Use julian date calculation */
	if(m > 2)
		m -= 3;
	else {
		m += 9;
		y--;
	}
	epoch = (146097 * (y / 100)) / 4 + (1461 * (y % 100)) / 4 + (153 * m + 2) / 5 + clock_timeparts.mday + 1721119 - JULIAN_1_1_1970;
	epoch *= 86400UL;
	epoch += clock_timeparts.hour * 3600UL;		/* Current day's hours, */
	epoch += clock_timeparts.min * 60UL;		/* minutes */
	epoch += clock_timeparts.sec;			/* and seconds */
	cli();
	clock_secs = epoch;
	sti();
}
