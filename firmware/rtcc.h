/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_RTCC_H
#define __504SEGCLOCK_RTCC_H

typedef union __rtccdatetime_t {
	uint8_t		bytes[8];
	struct {
		uint8_t	sec1:4;
		uint8_t	sec10:3;	/* [0..59] */
		uint8_t	st:1;		/* Start/stop oscillator */

		uint8_t	min1:4;
		uint8_t	min10:3;	/* [0..59] */
		uint8_t	:1;

		union {
			struct {
				uint8_t	hour1:4;
				uint8_t	hour10:2;	/* [0..23] */
				uint8_t	hourfmt:1;	/* 12/24 hour format, 0==24 */
				uint8_t	:1;
			};
			struct {
				uint8_t	ap_hour1:4;
				uint8_t	ap_hour10:1;	/* [0..23] or [1..12+AM/PM] */
				uint8_t	ap_ampm:1;	/* AM/PM indicator in 12h format */
				uint8_t	ap_hourfmt:1;	/* 12/24 hour format, 0==24 */
				uint8_t	:1;
			};
		};

		uint8_t	wday:3;		/* [1..7] */
		uint8_t	vbaten:1;	/* Battery enable */
		uint8_t	vbat:1;		/* Set when running on battery, clear to clear timestamps */
		uint8_t	oscon:1;	/* Oscillator on indicator */
		uint8_t	:2;

		uint8_t	mday1:4;
		uint8_t	mday10:2;	/* [1..31] */
		uint8_t	:2;

		uint8_t	mon1:4;
		uint8_t	mon10:1;	/* [1..12] */
		uint8_t	lp:1;		/* Leap year indicator */
		uint8_t	:1;

		uint8_t	year1:4;
		uint8_t	year10:4;	/* [0..99] */

		uint8_t	rs:3;		/* MFP divider 000=1Hz, 001=4096Hz, 010=8192Hz, 011=32768Hz, 1xx=64Hz calibration */
		uint8_t	extosc:1;	/* External oscillator enable */
		uint8_t	alm:2;		/* Active alarms 00=none, 01=alarm0, 10=alarm1, 11=both */
		uint8_t	sqwe:1;		/* Square wave output on MFP */
		uint8_t	out:1;		/* Static MFP output */
	};
} rtccdatetime_t;


void rtcc_init(void);
uint8_t rtcc_cal_read(void);
void rtcc_cal_write(uint8_t cal);
void rtcc_read(rtccdatetime_t *dt);
void rtcc_write_now(void);

#endif
