/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_NETWORK_H
#define __504SEGCLOCK_NETWORK_H

#include <stdint.h>

#define IP_CP(dst,src)	do { \
					(dst)[0] = (src)[0]; \
					(dst)[1] = (src)[1]; \
					(dst)[2] = (src)[2]; \
					(dst)[3] = (src)[3]; \
				} while(0)

#define IP_SET(dst,val)	do { \
					(dst)[0] = (val); \
					(dst)[1] = (val); \
					(dst)[2] = (val); \
					(dst)[3] = (val); \
				} while(0)

#define IP_IS_SAME(a,b)			((*(uint32_t *)(a)) == (*(uint32_t *)(b)))
#define IP_IS_SAME_MASKED(a,b,m)	(((*(uint32_t *)(a)) & (*(uint32_t *)(m))) == ((*(uint32_t *)(b)) & (*(uint32_t *)(m))))
#define IP_IS_MULTICAST(a)		((a)[0] >= 224 && (a)[0] <= 239)
#define IP_IS_BROADCAST(a)		((a)[0] == 255 && (a)[1] == 255 && (a)[2] == 255 && (a)[3] == 255)
#define IP_IS_VALID(a)			((a)[0] >= 1 && (a)[0] <= 239 && (a)[0] != 127)
#define IP_IS_VALID_NETMASK(a)		((~(*(uint32_t *)(a))) & ((~(*(uint32_t *)(a))) + 1) != 0)

#define MAC_CP(dst,src)	do { \
					(dst)[0] = (src)[0]; \
					(dst)[1] = (src)[1]; \
					(dst)[2] = (src)[2]; \
					(dst)[3] = (src)[3]; \
					(dst)[4] = (src)[4]; \
					(dst)[5] = (src)[5]; \
				} while(0)

#define MAC_SET(dst,val)	do { \
					(dst)[0] = (val); \
					(dst)[1] = (val); \
					(dst)[2] = (val); \
					(dst)[3] = (val); \
					(dst)[4] = (val); \
					(dst)[5] = (val); \
				} while(0)

#define MAC_IS_SAME(a,b)	((a)[0] == (b)[0] && (a)[1] == (b)[1] && (a)[2] == (b)[2] && (a)[3] == (b)[3] && (a)[4] == (b)[4] && (a)[5] == (b)[5])
#define MAC_IS_MULTICAST(a)	(((a)[0] & 1) != 0)

#endif
