/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <network.h>
#include <irq.h>
#include <config.h>
#include <dhcp.h>
#include <clock.h>
#include <serial.h>

//#define DEBUG_DHCP	1
//#define PEDANTICDHCP	1
//#define DUMPPACKET	1

static uint8_t state;			/* Current state of negotiation */
static uint32_t xid;			/* Unique ID for transactions */

static uint8_t netmask[4];		/* Offered netmask */
static uint8_t gateway[4];		/* Offered gateway */
static uint8_t ipaddr[4];		/* Offered IP address */
static uint8_t serverid[4];		/* DHCP server */
static uint8_t ntpaddr[4];		/* NTP server */
static uint8_t dnsaddr1[4];		/* Primary DNS server */
static uint8_t dnsaddr2[4];		/* Secondary DNS server */
static char hostname[MAXHOSTNAME];	/* Offered hostname */
uint32_t leasetime;			/* Expire */
uint32_t renewaltime;			/* T1 */
uint32_t rebindingtime;			/* T2 */
static uint8_t relaymac[6];		/* This is the server/relay server's MAC */
static uint8_t relayip[4];		/* This is the server/relay server's IP */

union __dhcpflags {
	struct {
		uint8_t	dhcpmsg:1;
		uint8_t	serverid:1;
		uint8_t	ipaddr:1;
		uint8_t	netmask:1;
		uint8_t	gateway:1;
		uint8_t	leasetime:1;
		uint8_t	renewaltime:1;
		uint8_t	rebindingtime:1;
	};
	uint8_t	any;
} flags;

union __hostnameflags {
	struct {
		uint8_t	hostname:1;
		uint8_t	domainname:1;
		uint8_t	ntpaddr:1;
		uint8_t	dnsaddr1:1;
		uint8_t	dnsaddr2:1;
		uint8_t	res:3;
	};
	uint8_t	any;
} hflags;

#define valid_offer_flags()	((flags.any & 0x3f) == 0x3f)
#define valid_ack_flags()	(flags.any == 0xff)

void dhcp_init(void)
{
	if(cfg.dhcp_on) {
		state = DHCPSTATE_INIT;
	} else {
		state = DHCPSTATE_OFF;
	}
	leasetime = 0;
	renewaltime = 0;
	rebindingtime = 0;
}

#define _dpkt	(*(dhcppkt_t *)&packet)

/* The parameters we want to know from the DHCP-server */
static const uint8_t parmrqlist[8] = {
	DHCPOPT_NETMASK,
	DHCPOPT_GATEWAY,
	DHCPOPT_HOSTNAME,
	DHCPOPT_DOMAINNAME,
	DHCPOPT_RENEWALTIME,
	DHCPOPT_REBINDINGTIME,
	DHCPOPT_DNS,
	DHCPOPT_TIMESERVER,
};
static uint8_t optsize;

static void add_opt(uint8_t ot, uint8_t ol, uint8_t *lst)
{
	_dpkt.options[optsize++] = ot;
	if(!ol)
		return;
	_dpkt.options[optsize++] = ol;
	while(ol--)
		_dpkt.options[optsize++] = *lst++;
}

static void dhcpmessage(uint8_t dt)
{
	memset(&_dpkt, 0, sizeof(_dpkt));	/* Does not clear option space */
	MAC_CP(_dpkt.eth.sa, cfg.macaddr);
	switch(state) {
	case DHCPSTATE_REBINDING:
	case DHCPSTATE_SELECTING:
		MAC_SET(_dpkt.eth.da, 0xff);	/* Broadcast dest */
		IP_SET(_dpkt.ip.daddr, 0xff);
		//_dpkt.ip.saddr[];		/* This is 0.0.0.0 */
		break;
	case DHCPSTATE_REQUESTING:
		MAC_CP(_dpkt.eth.da, relaymac);
		IP_CP(_dpkt.ip.daddr, relayip);
		IP_CP(_dpkt.ip.saddr, ipaddr);
		break;
	case DHCPSTATE_RENEWING:
		IP_CP(_dpkt.ip.saddr, cfg.ipaddr);
		IP_CP(_dpkt.ip.daddr, serverid);
		if(!arp_resolve(serverid, _dpkt.eth.da)) {
			arp_whohas(serverid);
			return;
		}
		break;
	default:
		/* Should not happen... */
		return;
	}
#ifdef DEBUG_DHCP
	serial_puts("dhcp: sending message in state 0x");
	serial_puthex(state);
	serial_putc('\n');
#endif
	_dpkt.eth.lenw = htons(ETHTYPE_IP);
	_dpkt.ip.version = 4;
	_dpkt.ip.hdrlen = 5;
	_dpkt.ip.ttl = IP_TTL_DEFAULT;
	_dpkt.ip.proto = IPPROTO_UDP;
	//_dpkt.ip.checksum = 0;
	_dpkt.udp.sport = htons(UDP_PORT_BOOTPC);
	_dpkt.udp.dport = htons(UDP_PORT_BOOTPS);
	//_dpkt.udp.checksum = 0;
	_dpkt.dhcp.op = BOOTP_REQUEST;
	_dpkt.dhcp.htype = ARP_HTYPE_ETH;
	_dpkt.dhcp.hlen = ARP_HLEN_ETH;
	//_dpkt.dhcp.hops = 0;
	_dpkt.dhcp.xid = xid;
	//_dpkt.dhcp.secs = 0;
	//_dpkt.dhcp.flags = 0;
	if(state == DHCPSTATE_RENEWING || state == DHCPSTATE_REBINDING)
		IP_CP(_dpkt.dhcp.ciaddr, cfg.ipaddr);
	//_dpkt.dhcp.yiaddr = 0;
	//_dpkt.dhcp.siaddr = 0;
	//_dpkt.dhcp.giaddr = 0;
	MAC_CP(_dpkt.dhcp.chaddr, cfg.macaddr);
	//_dpkt.dhcp.sname[64];
	//_dpkt.dhcp.file[128];
	_dpkt.dhcp.magic_cookie = DHCP_MAGIC_COOKIE;	/* See RFC 2132 */
	optsize = 0;
	add_opt(DHCPOPT_DHCPMSG, 1, &dt);
	add_opt(DHCPOPT_HOSTNAME, strlen(cfg.hostname), cfg.hostname);
	add_opt(DHCPOPT_PARMRQLIST, sizeof(parmrqlist), parmrqlist);
	if(state == DHCPSTATE_REQUESTING) {
		add_opt(DHCPOPT_REQADDR, 4, ipaddr);
		add_opt(DHCPOPT_SERVERID, 4, serverid);
	}
	add_opt(DHCPOPT_END, 0, NULL);
	_dpkt.ip.len = htons(sizeof(_dpkt.ip) + sizeof(_dpkt.udp) + sizeof(_dpkt.dhcp) + optsize);
	_dpkt.ip.checksum = ip_checksum(&_dpkt.ip, sizeof(_dpkt.ip));
	_dpkt.udp.length = htons(sizeof(_dpkt.udp) + sizeof(_dpkt.dhcp) + optsize);
	_dpkt.udp.checksum = udp_checksum(&_dpkt.ip, sizeof(_dpkt.udp) + sizeof(_dpkt.dhcp) + optsize);
	ethernet_txpacket(sizeof(_dpkt) + optsize);
	/* Note: we actually do not care if the packet got send because the
	 * timer will reset the state and try again at an appropriate time
	 */
}

void dhcp_network_halt(void)
{
	IP_SET(cfg.ipaddr, 0);	/* Clear our IP address */
	IP_SET(cfg.nmaddr, 0);	/* Clear our netmask */
	IP_SET(cfg.gwaddr, 0);	/* Clear our gateway address */
}

void dhcp_handle_timer(void)
{
	switch(state) {
	case DHCPSTATE_OFF:
		/* We don't set the timer here anymore */
		/* It will be set when dhcp is enabled */
		clock_timer_clear(TIMER_DHCP);
		break;

	case DHCPSTATE_INIT:
		if(ethlink != ETH_LINK_UP) {
			/* The timer will be set on link-up */
			return;
		}
		xid = 0xdeadbeaf ^ clock_secs ^ clock_msecs;	/* Unique ID for this session */
		state = DHCPSTATE_SELECTING;
		dhcpmessage(DHCPMSG_DISCOVER);
		clock_timer_set(TIMER_DHCP, DHCPTIMER_SELECTINGTICKS);
		break;

	case DHCPSTATE_SELECTING:
	case DHCPSTATE_REQUESTING:
		/* We got a timeout, retry on the next clock tick */
		state = DHCPSTATE_INIT;
		clock_timer_set(TIMER_DHCP, 1);
		break;

	case DHCPSTATE_BOUND:
	case DHCPSTATE_RENEWING:
	case DHCPSTATE_REBINDING:
		clock_timer_set(TIMER_DHCP, DHCPTIMER_TICKS);
		renewaltime--;
		rebindingtime--;
		leasetime--;
		if(!leasetime) {
			/* The lease ended and we could neither renew nor rebind */
			state = DHCPSTATE_INIT;
			dhcp_network_halt();
		} else if(!rebindingtime) {
			rebindingtime = DHCP_REBINDING_INTERVAL;
			state = DHCPSTATE_REBINDING;
			dhcpmessage(DHCPMSG_REQUEST);
		} else if(!renewaltime) {
			renewaltime = DHCP_RENEWING_INTERVAL;
			state = DHCPSTATE_RENEWING;
			dhcpmessage(DHCPMSG_REQUEST);
		} else if(renewaltime == 1) {
			/* We are about to send a packet to the server. So we make sure
			 * that we can reach it by sending an arp request now. Otherwise,
			 * we'd probably delay the renewal by DHCP_RENEWING_INTERVAL seconds.
			 * This is because the default lease is most likely longer than
			 * the arp-timeout. Anyway, it does not hurt to refresh the entry.
			 */
			arp_whohas(serverid);
		}
		break;

	default:
		if(cfg.dhcp_on) {
			state = DHCPSTATE_INIT;
			dhcp_network_halt();
			clock_timer_set(TIMER_DHCP, DHCPTIMER_STARTTICKS);
		} else {
			state = DHCPSTATE_OFF;	/* Don't reset the timer, we don't need it in off mode */
			clock_timer_clear(TIMER_DHCP);
		}
		break;
	}
}

#define _optptr(p)	((uint8_t *)(&(p)[1]))

uint8_t dhcp_handle_rxpacket(uint16_t pktlen)
{
	uint16_t i;
	dhcphdr_t *dhcp;
	uint16_t optlen;
	uint8_t dolf;
	uint8_t msg = 0;
	uint8_t l;

	(void)pktlen;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: got packet\n");
#endif

	/* Ignore anything if we are bound to an IP */
	if(state == DHCPSTATE_BOUND || state == DHCPSTATE_OFF)
		return 0;

	/* The minimum dhcp message size is the header plus 4 bytes in the options (the dhcp message indicator and end-of-options) */
	if(ntohs(_dpkt.udp.length) < sizeof(dhcphdr_t) + 4)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept len\n");
#endif

	dhcp = (dhcphdr_t *)(((uint8_t *)&packet) + sizeof(ethhdr_t) + iphdr_size(&_dpkt.ip) + sizeof(udphdr_t));
	if(_dpkt.udp.sport != htons(UDP_PORT_BOOTPS) || dhcp->op != BOOTP_REPLY)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept bootp reply\n");
#endif

#ifdef DEBUG_DHCP
#ifdef DUMPPACKET
	serial_puts("DHCP packet:");
	for(i = 0; i < pktlen; i++) {
		if(!(i % 32))
			serial_putc('\n');
		if(16 == (i % 32))
			serial_putc(' ');
		serial_puthex(packet.octets[i]);
		serial_putc(' ');
	}
	serial_putc('\n');
#endif
#endif

	if(dhcp->htype != ARP_HTYPE_ETH || dhcp->hlen != ARP_HLEN_ETH)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept htype/hlen eth\n");
#endif

#ifdef PEDANTICDHCP
	if(dhcp->hops || dhcp->secs)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept hops and secs\n");
#endif
#endif

	if(dhcp->xid != xid)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept xid\n");
#endif

#ifdef PEDANTICDHCP
	if(dhcp->flags || dhcp->giaddr)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept flags, giaddr\n");
#endif
#endif

	if(!MAC_IS_SAME(dhcp->chaddr, cfg.macaddr))
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept chaddr\n");
#endif

	if(dhcp->magic_cookie != DHCP_MAGIC_COOKIE)
		return 0;
#ifdef DEBUG_DHCP
	serial_puts("dhcp: accept magic cookie\n");
#endif

	optlen = ntohs(_dpkt.udp.length) - sizeof(dhcphdr_t);
	i = 0;
	flags.any = 0;
	hflags.any = 0;
	memset(hostname, 0xff, sizeof(hostname));
	hostname[0] = 0;
	while(i < optlen) {
		if(optlen - i >= 1)
			dolf = _optptr(dhcp)[i+1];
		else
			dolf = 0;
#ifdef DEBUG_DHCP
		serial_puts("dhcp: parse opt 0x");
		serial_puthex(_optptr(dhcp)[i]);
		serial_putc('\n');
#endif
		switch(_optptr(dhcp)[i]) {
		case DHCPOPT_END:
			i = optlen;
			break;
		case DHCPOPT_DHCPMSG:
			if(optlen - i < 3 || dolf != 1)
				return 0;
			msg = _optptr(dhcp)[i+2];
			i += 3;
			flags.dhcpmsg = 1;
			break;
		case DHCPOPT_NETMASK:
			if(optlen - i < 4 || dolf != 4)
				return 0;
			IP_CP(netmask, &_optptr(dhcp)[i+2]);
			i += 6;
			flags.netmask = 1;
			break;
		case DHCPOPT_GATEWAY:
			if(optlen - i < 4 || dolf < 4)
				return 0;
			/* We only copy the first and ignore any others */
			IP_CP(gateway, &_optptr(dhcp)[i+2]);
			i += dolf + 2;
			flags.gateway = 1;
			break;
		case DHCPOPT_LEASETIME:
			if(optlen - i < 4 || dolf != 4)
				return 0;
			leasetime = ntohl(*(uint32_t *)(&_optptr(dhcp)[i+2]));
			i += 6;
			/* In case rebind and/or renew are not given we must ensure a minimum.
			 * 4sec is minimum because rebind = lease/4 and renew = lease/2 if we
			 * only get a leasetime. And neither rebind nor renew may become 0
			 */
			if(leasetime >= 4)
				flags.leasetime = 1;
			break;
		case DHCPOPT_RENEWALTIME:
			if(optlen - i < 4 || dolf != 4)
				return 0;
			renewaltime = ntohl(*(uint32_t *)(&_optptr(dhcp)[i+2]));
			i += 6;
			flags.renewaltime = 1;
			break;
		case DHCPOPT_REBINDINGTIME:
			if(optlen - i < 4 || dolf != 4)
				return 0;
			rebindingtime = ntohl(*(uint32_t *)(&_optptr(dhcp)[i+2]));
			i += 6;
			flags.rebindingtime = 1;
			break;
		case DHCPOPT_SERVERID:
			if(optlen - i < 4 || dolf != 4)
				return 0;
			IP_CP(serverid, &_optptr(dhcp)[i+2]);
			i += 6;
			flags.serverid = 1;
			break;
		case DHCPOPT_HOSTNAME:
			if(optlen - i < 1 || dolf < 1 || dolf > sizeof(hostname)-1) {
				/* Ignore/skip hostname if empty or too large */
				i += dolf + 2;
				break;
			}
			if(hostname[0]) {
				/* We apparently already have a domainname in the buffer */
				/* Prepend with hostname with a '.' */
				if((l = strlen(hostname)) < sizeof(hostname) - dolf - 2) {
					memmove(&hostname[dolf+1], hostname, l+1);
					memcpy(hostname, &_optptr(dhcp)[i+2], dolf);
					hostname[dolf] = '.';
				} else {
					/* There is not enough room in the buffer for both host and domainname */
					/* Only use hostname then */
					memcpy(hostname, &_optptr(dhcp)[i+2], dolf);
					hostname[dolf] = 0;
				}
			} else {
				memcpy(hostname, &_optptr(dhcp)[i+2], dolf);
				hostname[dolf] = 0;
			}
			hflags.hostname = 1;
			i += dolf + 2;
			break;
		case DHCPOPT_DOMAINNAME:
			if(optlen - i < 1 || dolf < 1 || dolf > sizeof(hostname)-1) {
				/* Ignore/skip domainname if empty or too large */
				i += dolf + 2;
				break;
			}
			if(hostname[0]) {
				/* We apparently already have a hostname in the buffer */
				/* Append a '.' and domainname if there is room */
				if((l = strlen(hostname)) < sizeof(hostname) - dolf - 2) {
					hostname[l-1] = '.';
					memcpy(&hostname[l], &_optptr(dhcp)[i+2], dolf);
					hostname[l+dolf] = 0;
				}
			} else {
				memcpy(hostname, &_optptr(dhcp)[i+2], dolf);
				hostname[dolf] = 0;
			}
			hflags.domainname = 1;
			i += dolf + 2;
			break;
		case DHCPOPT_DNS:
			if(optlen - i < 4 || (dolf & (4-1)))
				return 0;
			IP_CP(dnsaddr1, &_optptr(dhcp)[i+2]);
			if(dolf > 4)
				IP_CP(dnsaddr2, &_optptr(dhcp)[i+2+4]);
			if(IP_IS_VALID(dnsaddr1)) {
				hflags.dnsaddr1 = 1;
			} else {
				if(IP_IS_VALID(dnsaddr2)) {
					IP_CP(dnsaddr1, dnsaddr2);
					hflags.dnsaddr1 = 1;
				}
				IP_SET(dnsaddr2, 0);
			}
			if(IP_IS_VALID(dnsaddr2))
				hflags.dnsaddr2 = 1;
			i += dolf + 2;
			break;
		case DHCPOPT_TIMESERVER:
			if(optlen - i < 4 || (dolf & (4-1)))
				return 0;
			/* Copy the first time server in the list (highest priority) */
			IP_CP(ntpaddr, &_optptr(dhcp)[i+2]);
			if(IP_IS_VALID(ntpaddr))
				hflags.ntpaddr = 1;
			i += dolf + 2;
			break;
		case DHCPOPT_NOP:
			i++;
			break;
		default:
			if(optlen - i < 2)	/* Must have at least 2 bytes here */
				return 0;
			i += dolf + 2;
			break;
		}
	}

#ifdef DEBUG_DHCP
	serial_puts("dhcp: done options\n");
#endif
	if(IP_IS_VALID(_dpkt.dhcp.yiaddr) && !IP_IS_MULTICAST(_dpkt.dhcp.yiaddr)) {
		IP_CP(ipaddr, _dpkt.dhcp.yiaddr);
		flags.ipaddr = 1;
	}

	/* If we did not get renewal- or rebinding-time, set them manually */
	if(flags.leasetime) {
		if(!flags.rebindingtime) {
			/* Rebind at 50% of leasetime */
			rebindingtime = leasetime >> 1;
			flags.rebindingtime = 1;
		}
		if(!flags.renewaltime) {
			/* Renew at 25% of leasetime */
			renewaltime = leasetime >> 2;
			flags.renewaltime = 1;
		}
	}

	if(hflags.domainname && !hflags.hostname) {
		/* We only got offered the domainname part. We must prepend our hostname */
		/* FIXME: should'nt we take our active hostname? */
		if((l = strlen(hostname)) < sizeof(hostname) - sizeof(serialnr) - 1 - 1) {
			/* It fits, prepend */
			memmove(hostname+sizeof(serialnr)+1, hostname, l+1);
			memcpy(hostname, serialnr, sizeof(serialnr));
			hostname[sizeof(serialnr)] = '.';
		} else {
			/* We canot fit both hostname and domainname, drop domainname */
			memcpy(hostname, serialnr, sizeof(serialnr));
			hostname[sizeof(serialnr)] = 0;
		}
	}

	switch(msg) {
	case DHCPMSG_OFFER:
#ifdef DEBUG_DHCP
		serial_puts("dhcp: got an OFFER; flags=0x");
		serial_puthex(flags.any);
		serial_puts("; state=0x");
		serial_puthex(state);
		serial_putc('\n');
#endif
		if(state == DHCPSTATE_SELECTING && valid_offer_flags()) {
			/* Send request based on offer */
			MAC_CP(relaymac, _dpkt.eth.sa);		/* Copy the source MAC and IP so we can reply unicast */
			IP_CP(relayip, _dpkt.ip.saddr);
			state = DHCPSTATE_REQUESTING;
			dhcpmessage(DHCPMSG_REQUEST);
			clock_timer_set(TIMER_DHCP, DHCPTIMER_REQUESTINGTICKS);
			return 1;
		}
		break;

	case DHCPMSG_ACK:
#ifdef DEBUG_DHCP
		serial_puts("dhcp: got an ACK; flags=0x");
		serial_puthex(flags.any);
		serial_puts("; state=0x");
		serial_puthex(state);
		serial_putc('\n');
#endif
		if((state == DHCPSTATE_REQUESTING || state == DHCPSTATE_RENEWING || state == DHCPSTATE_REBINDING) && valid_ack_flags()) {
			/* We have an IP */
			state = DHCPSTATE_BOUND;
			IP_CP(cfg.ipaddr, ipaddr);
			IP_CP(cfg.nmaddr, netmask);
			IP_CP(cfg.gwaddr, gateway);
			if(hflags.ntpaddr && cfg.ntp_use_dhcp)
				IP_CP(cfg.ntpaddr, ntpaddr);
			if(hflags.dnsaddr1)
				IP_CP(cfg.dnsaddr1, dnsaddr1);
			else
				IP_SET(cfg.dnsaddr1, 0);
			if(hflags.dnsaddr2)
				IP_CP(cfg.dnsaddr2, dnsaddr2);
			else
				IP_SET(cfg.dnsaddr2, 0);
			if(cfg.dhcp_hostname && hostname[0])
				memcpy(cfg.hostname, hostname, sizeof(hostname));
			clock_timer_set(TIMER_DHCP, DHCPTIMER_TICKS);
			return 1;
		}
		break;

	case DHCPMSG_NAK:
#ifdef DEBUG_DHCP
		serial_puts("dhcp: got a NACK; flags=0x");
		serial_puthex(flags.any);
		serial_puts("; state=0x");
		serial_puthex(state);
		serial_putc('\n');
#endif
		if(state == DHCPSTATE_REQUESTING || state == DHCPSTATE_RENEWING || state == DHCPSTATE_REBINDING) {
			/* We are rejected, retry from the start */
			state = DHCPSTATE_INIT;
			dhcp_network_halt();
			clock_timer_set(TIMER_DHCP, DHCPTIMER_NAKTICKS);
			return 1;
		}
		break;

#ifdef DEBUG_DHCP
	default:
		serial_puts("dhcp: unhandled: got a dhcpmsg=0x");
		serial_puthex(msg);
		serial_puts("; flags=0x");
		serial_puthex(flags.any);
		serial_puts("; state=0x");
		serial_puthex(state);
		serial_putc('\n');
#endif
	}
	return 0;
}

void dhcp_linkchange(void)
{
	if(!cfg.dhcp_on)
		return;
	if(ethlink == ETH_LINK_UP) {
		/* The link is up, start the timer to start the state-machine */
		clock_timer_set(TIMER_DHCP, DHCPTIMER_STARTTICKS);
	} else {
		state = DHCPSTATE_INIT;
		dhcp_network_halt();
		clock_timer_clear(TIMER_DHCP);
	}
}

void dhcp_toggle(void)
{
	if(cfg.dhcp_on) {
		cfg.dhcp_on = 0;
		state = DHCPSTATE_OFF;
		dhcp_network_halt();
		clock_timer_clear(TIMER_DHCP);
		/* FIXME: If we have a static IP we might want to set it again... */
	} else {
		cfg.dhcp_on = 1;
		state = DHCPSTATE_INIT;
		dhcp_network_halt();
		clock_timer_set(TIMER_DHCP, DHCPTIMER_STARTTICKS);	/* Set for "fast" start */
	}
}
