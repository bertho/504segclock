/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <pic18fregs.h>
#include <irq.h>
#include <bytes.h>
#include <network.h>
#include <config.h>
#include <ethernet.h>
#include <io.h>
#include <clock.h>
#include <serial.h>
#include <arp.h>
#include <ip.h>

//#define FULLDUPLEX	1
//#define INITETHRAM	1

static eth_rx_status_t ethrxstatus;
volatile uint8_t ethlink;		/* Indicate current link-state */
volatile uint8_t ethlinkchange;		/* Indicate change from last state */
volatile uint32_t eth_secs;		/* Timestamp of packet receive */
volatile uint16_t eth_msecs;

#ifdef ABS_PACKET
__data packet_t __at __PACKET_ADDRESS packet;
#else
packet_t packet;
#endif

static void ethernet_setphy(uint8_t r, uint16_t v)
{
	/* Wait for PHY to become available */
	while(MISTATbits.BUSY)
		;
	/* See "PIC18F97J60 Family Rev. A0 Silicon Errata" at 5 */
	PRODL = (uint8_t)(v & 0xff);
	PRODH = (uint8_t)(v >> 8);
	MIREGADR = r;
	//cli();
	__asm
	movff	_PRODL, _MIWRL
	nop
	movff	_PRODH, _MIWRH
	__endasm;
	//sti();
	//MIWRL = (uint8_t)(v & 0xff);
	//MIWRH = (uint8_t)(v >> 8);
}

uint16_t ethernet_getphy(uint8_t r)
{
	static union {
		struct {
			uint8_t b1;
			uint8_t b2;
		};
		uint16_t w1;
	} word;

	/* Wait for PHY to become available */
	while(MISTATbits.BUSY)
		;
	MIREGADR = r;
	MICMDbits.MIIRD = 1;
	/* delay(10.24us) */
	__asm
	movlw	36
phyrd_delayloop:
	addlw	255		; w--
	bnz	phyrd_delayloop	; 4/41.667 * 3 * 36 ~10.36us
	__endasm;
	/* Wait for PHY to become available */
	while(MISTATbits.BUSY)
		;
	MICMDbits.MIIRD = 0;
	word.b1 = MIRDL;
	word.b2 = MIRDH;
	return word.w1;
}

void ethernet_init(void)
{
	ethlink = ETH_LINK_DOWN;
	ethlinkchange = 1;

	ECON2bits.AUTOINC = 1;
	TRISAbits.TRISA0 = 0;	/* Set the port to output for LED[AB] on RA[01] */
	TRISAbits.TRISA1 = 0;

	/* Setup receive buffer */
	ECON1bits.RXEN = 0;	/* Make sure to disable receiver when updating the buffer pointers */
#ifdef INITETHRAM
{
	uint16_t i;
	ETXSTL = 0;
	ETXSTH = 0;
	ETXNDL = LOBYTE(ETH_BUFSIZE - 1);
	ETXNDH = HIBYTE(ETH_BUFSIZE - 1);
	EWRPTL = 0;
	EWRPTH = 0;
	for(i = 0; i < ETH_BUFSIZE/2; i++) {
		PRODH = HIBYTE(i);
		PRODL = LOBYTE(i);
		PRODH = 0xff;
		PRODL = 0xff;
		__asm
		movff	_PRODH, _EDATA
		movff	_PRODL, _EDATA
		__endasm;
	}
}
#endif
	ETXSTL = LOBYTE(TXBUF_START);
	ETXSTH = HIBYTE(TXBUF_START);
	ETXNDL = LOBYTE(TXBUF_END);
	ETXNDH = HIBYTE(TXBUF_END);
	EWRPTL = LOBYTE(TXBUF_START);
	EWRPTH = HIBYTE(TXBUF_START);

	ERXSTL = LOBYTE(RXBUF_START);
	ERXSTH = HIBYTE(RXBUF_START);
	ERXNDL = LOBYTE(RXBUF_END);
	ERXNDH = HIBYTE(RXBUF_END);
	ERXRDPTL = LOBYTE(RXBUF_END);	/* See errata; pointer must always be odd; will wrap at first read */
	ERXRDPTH = HIBYTE(RXBUF_END);

	/* Setup receive filter */

	/* Enable module and wait for PHY */
	ECON2bits.ETHEN = 1;
	/* Wait for the PHY to startup */
	while(!ESTATbits.PHYRDY)
		;

#ifdef FULLDUPLEX
	MACON1 = 0x0d;	/* pause frames, no promisc, enable receive */
	MACON3 = 0xf3;	/* 64B pad, CRC enable, no prop hdr, no huge frame, check frame len, full duplex */
	MACON4 = 0x40;	/* Set 802.3 compliance */
	MABBIPG = 0x15;	/* Back-to-back interpacket gap: 0x12 in half, 0x15 in full */
	MAIPGL = 0x12;	/* Back-to-back non-interpacket gap low */
	MAIPGH = 0x00;	/* Back-to-back non-interpacket gap high (only half duplex) */
#else
	MACON1 = 0x01;	/* no pause frames, no promisc, enable receive */
	MACON3 = 0xf2;	/* 64B pad, CRC enable, no prop hdr, no huge frame, check frame len, half duplex */
	MACON4 = 0x40;	/* Set 802.3 compliance */
	MABBIPG = 0x12;	/* Back-to-back interpacket gap: 0x12 in half, 0x15 in full */
	MAIPGL = 0x12;	/* Back-to-back non-interpacket gap low */
	MAIPGH = 0x0c;	/* Back-to-back non-interpacket gap high (only half duplex) */
#endif
	MAADR1 = cfg.macaddr[0];	/* Set the MAC */
	MAADR2 = cfg.macaddr[1];
	MAADR3 = cfg.macaddr[2];
	MAADR4 = cfg.macaddr[3];
	MAADR5 = cfg.macaddr[4];
	MAADR6 = cfg.macaddr[5];
#ifdef FULLDUPLEX
	ethernet_setphy(PHCON1, 0x0100);	/* set full duplex (half: 0x0000) */
#else
	ethernet_setphy(PHCON1, 0x0000);	/* set half duplex (full: 0x0100) */
#endif
	ethernet_setphy(PHCON2, 0x0110);	/* normal operation */
	//ethernet_setphy(PHLCON, 0x3c12);	/* A: link+rx, B: tx, stretch 40ms */
	//ethernet_setphy(PHLCON, 0x3742);	/* A: tx+rx activity, B: link, stretch 40ms */
	ethernet_setphy(PHLCON, 0x3472);	/* B: tx+rx activity, A: link, stretch 40ms */
	//ethernet_setphy(PHLCON, 0x3d36);	/* A: link+tx+rx activity, B: collision, stretch 70ms */
	ethernet_getphy(PHIR);			/* Read the flags to clear any pending interrupt */
	ethernet_setphy(PHIE, 0x0012);		/* Link change interrupt enable */

	EIR = 0;	/* Clear all interrupt flags */
	EIE = 0x5b;	/* Enable rx/tx packet, linkchange and rx/tx error interrupts */
	//EIE = 0x13;	/* Enable rx/tx error and linkchange interrupts */

	ECON1bits.CSUMEN = 0;	/* Disable DMA crc calculation; See "PIC18F97J60 Family Rev. A0 Silicon Errata" at 8 */
	ERXFCON = 0xa3;		/* Enable uni-/multi-/broad-cast packets to arrive with valid CRC, no hash or pattern match filters */
	//ERXFCON = 0xa1;	/* Enable uni-/broad-cast packets to arrive with valid CRC, no hash or pattern match filters */
	ECON1bits.RXEN = 1;	/* Enable receiver */

	/* Enable interrupts */
	PIR2bits.ETHIF = 0;
	PIE2bits.ETHIE = 1;
}

static void handle_linkchange(void)
{
	ethlink = (ethernet_getphy(PHSTAT2) & 0x0400) ? ETH_LINK_UP : ETH_LINK_DOWN;
	ethlinkchange = 1;
	ethernet_getphy(PHIR);	/* Clear the interrupt by reading the register */
}

static void handle_rxerror(void)
{
	ESTATbits.BUFER = 0;
	EIRbits.RXERIF = 0;
	//EIEbits.RXERIE = 0;
	io_led_err_toggle();
	//serial_puts("--RXError--");
}

static void handle_txerror(void)
{
	ESTATbits.TXABRT = 0;
	ESTATbits.BUFER = 0;
	EIRbits.TXERIF = 0;
	//EIEbits.TXERIE = 0;
	io_led_err_toggle();
	//serial_puts("--TXError--");
}

static void handle_rx(void)
{
	/* Flag only gets cleared when EPKTCNT is 0, so we disable the interrupt */
	EIEbits.PKTIE = 0;
	/* Tag the packet's arrival */
	eth_secs = clock_secs;
	eth_msecs = clock_msecs;
}

static void handle_tx(void)
{
	/* Simply clear the flag. The interrupt wakes us from sleep */
	//EIRbits.TXIF_EIR = 0;
	EIRbits.TXIF = 0;
}

void ethernet_isr(void)
{
	if(EIEbits.RXERIE && EIRbits.RXERIF)
		handle_rxerror();
	if(EIEbits.TXERIE && EIRbits.TXERIF)
		handle_txerror();
	if(EIEbits.PKTIE && EIRbits.PKTIF)
		handle_rx();
	//if(EIEbits.TXIE_EIE && EIRbits.TXIF_EIR)
	if(EIEbits.TXIE && EIRbits.TXIF)
		handle_tx();
	if(EIEbits.LINKIE && EIRbits.LINKIF)
		handle_linkchange();
	PIR2bits.ETHIF = 0;
}

void ethernet_handle_rxpacket(void)
{
	uint16_t i;

#if 0
	serial_puts("rx: packet to handle\n");
#endif
	/* NOTE:
	 * Eventhough the ERXRDPT was programmed with an odd value,
	 * we do not read a dummy. It appears that the controller
	 * aligns the pointer itself
	 */
#if 0
	serial_puts("rx: vector RD: 0x");
	i = ERXRDPTH;
	i <<= 8;
	i |= ERXRDPTL;
	serial_puthex_u16(i);
	serial_puts(" WR: 0x");
	i = ERXWRPTH;
	i <<= 8;
	i |= ERXWRPTL;
	serial_puthex_u16(i);
	serial_puts(" ST: 0x");
	i = ERXSTH;
	i <<= 8;
	i |= ERXSTL;
	serial_puthex_u16(i);
	serial_puts(" ND: 0x");
	i = ERXNDH;
	i <<= 8;
	i |= ERXNDL;
	serial_puthex_u16(i);
	serial_putc('\n');
	serial_flush_out();
#endif
	/* Read the status vector */
	for(i = 0; i < sizeof(ethrxstatus); i++) {
		__asm
		movff	_EDATA, _PRODL
		__endasm;
		ethrxstatus.bytes[i] = PRODL;
	}
#if 0
	serial_puts("rx: Status dump: \n");
	for(i = 0; i < sizeof(ethrxstatus); i++) {
		serial_puthex(ethrxstatus.bytes[i]);
		serial_putc(' ');
	}
	serial_putc('\n');
#endif
	/* Read the packet data */
	for(i = 0; i < ethrxstatus.rx; i++) {
		__asm
		movff	_EDATA, _PRODL
		__endasm;
		packet.octets[i] = PRODL;
	}

	/* If the packet length is odd, we need to advance to the next even boundary */
	if(ethrxstatus.rx & 1) {
		__asm
		movff	_EDATA, _PRODL
		__endasm;
	}
	/* Setup read for the next packet */
	/* See errata: read pointer must always be odd */
	/* Errara says:
	 * if(ethrxstatus.next -1 < ERXST || ethrxstatus.next - 1 > ERXND)
	 *	if(ethrxstatus.next -1 < ERXST || ethrxstatus.next - 1 > ERXND)
	 *		ERXRDPT = ERXND;
	 *	else
	 *		ERXRDPT = ethrxstatus.next - 1;
	 */
	/* Note: RXBUF_START == 0 */
#if 0
serial_puts("rx: ERXRDPT: 0x");
serial_puthex(ERXRDPTH);
serial_puthex(ERXRDPTL);
serial_puts(" going to 0x");
#endif
	if(ethrxstatus.next - 1 < RXBUF_START || ethrxstatus.next - 1 > RXBUF_END) {
		ERXRDPTL = LOBYTE(RXBUF_END);
		ERXRDPTH = HIBYTE(RXBUF_END);
#if 0
		serial_puthex_u16(RXBUF_END);
#endif
	} else {
		ERXRDPTL = LOBYTE(ethrxstatus.next - 1);
		ERXRDPTH = HIBYTE(ethrxstatus.next - 1);
#if 0
		serial_puthex_u16(ethrxstatus.next-1);
#endif
	}
#if 0
	serial_putc('\n');
#endif

	/* Decrement packet counter, but do not underflow */
	if(EPKTCNT)
		ECON2bits.PKTDEC = 1;

	clock_entropy.words[0] += ethrxstatus.next ^ clock_msecs;
	clock_entropy.words[1] += ethrxstatus.rx ^ clock_msecs;
	clock_entropy.dword += clock_secs;

	if(ethrxstatus.rx_ok) {
		/* Handle the data */
#if 0
	serial_puts("rx: got packet: 0x");
	serial_puthex_u16(ntohs(packet.ethpkt.hdr.lenw));
	serial_puts(" 0x");
	serial_puthex_u16(ethrxstatus.rx);
	serial_putc('\n');
#endif
#if 0
	serial_puts("rx: Packet dump:");
	for(i = 0; i < ethrxstatus.rx; i++) {
		if(!(i & 0xf))
			serial_putc('\n');
		serial_puthex(packet.octets[i]);
		serial_putc(' ');
	}
	serial_putc('\n');
	serial_flush_out();
#endif
		/* Add the CRC to the entropy pool */
		clock_entropy.dword ^= *(uint32_t *)(&packet.octets[ethrxstatus.rx - 4]);

		if(packet.ethpkt.hdr.lenw == htons(ETHTYPE_ARP))
			arp_handle_rxpacket(ethrxstatus.rx - 4);
		else if(packet.ethpkt.hdr.lenw == htons(ETHTYPE_IP))
			ip_handle_rxpacket(ethrxstatus.rx - 4);
	}
#if 0
	serial_puts("rx: packet done\n\n");
	serial_flush_out();
#endif
}

uint8_t ethernet_txpacket(uint16_t len)
{
	uint16_t i;

	/* No need to go through all the trouble if the link is down */
	if(ethlink != ETH_LINK_UP)
		return 0;

	/* Wait for transmitter to become idle */
	/* FIXME: We might want to go to sleep in mainloop */
	while(ECON1bits.TXRTS)
		;

	/* Setup transmit buffer */
	ETXSTL = LOBYTE(TXBUF_START);
	ETXSTH = HIBYTE(TXBUF_START);

	/* Setup write pointer */
	EWRPTL = LOBYTE(TXBUF_START);
	EWRPTH = HIBYTE(TXBUF_START);

	PRODL = 0x00;	/* Preamble command byte: use set config, no overrides */
	__asm
	movff	_PRODL, _EDATA
	__endasm;
	/* Transfer the packet data to the ethernet module */
	for(i = 0; i < len; i++) {
		PRODL = packet.octets[i];
		__asm
		movff	_PRODL, _EDATA
		__endasm;
	}

	/* Set end-of-packet pointer */
	ETXNDL = LOBYTE(TXBUF_START + len);
	ETXNDH = HIBYTE(TXBUF_START + len);

	/* There is a racecondition here from link check to start tx */
	/* but we do not care */
	if(ethlink != ETH_LINK_UP)
		return 0;
	ECON1bits.TXRTS = 1;	/* Go! */

	/* See errata: possible transmitter deadlock resolved in timer */
	clock_timer_set(TIMER_ETHTX, 4);	/* 4ms because we do no know on which side of the tick we are */
	return 1;
}

