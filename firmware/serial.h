/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_SERIAL_H
#define __504SEGCLOCK_SERIAL_H

/* Baudrate @41.667MHz SYNC=0 BRG16=1 BRGH=1 */

#define B2400	((uint16_t)4339)
#define B9600	((uint16_t)1084)
#define B19200	((uint16_t) 542)
#define B38400	((uint16_t) 270)
#define B57600	((uint16_t) 180)
#define B115200	((uint16_t)  89)

/* Baudrate @41.667MHz SYNC=0 BRG16=1 BRGH=0 */
/*
#define B2400	((uint16_t)1084)
#define B9600	((uint16_t) 270)
#define B19200	((uint16_t) 135)
#define B38400	((uint16_t)  72)
#define B57600	((uint16_t)  44)
#define B115200	((uint16_t)  22)
*/

extern volatile uint8_t recvbufcnt;
#define serial_have_input()	(recvbufcnt != 0)

void serial_init(void);
void serial_putc(uint8_t ch) __wparam;
void serial_puts(const char *str);
void serial_put(const char *str, uint8_t len);
void serial_puthex(uint8_t h) __wparam;
void serial_puthex_u16(uint16_t h);
void serial_puthex_u32(uint32_t h);
void serial_putdec(uint8_t d);
void serial_putdec_u16(uint16_t d);
void serial_putdec_s16(int16_t d);
#define serial_putdec_s8(v)	serial_putdec_s16((int16_t)((int8_t)(v)))
void serial_putdec_u32(uint32_t d);

uint8_t serial_getc(void);

void serial_flush_out(void);

void serial_send_isr(void);
void serial_recv_isr(void);

#endif
