; vim: syn=pic
;
; 504 Segment Clock - Display driver
; Copyright (C) 2013  B. Stultiens
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;
	list
	errorlevel	1
	radix		dec

	include		"p18f45k22.inc"
	CONFIG	FOSC = INTIO67
	CONFIG	PLLCFG = ON
	CONFIG	PRICLKEN = ON
	CONFIG	FCMEN = ON
	CONFIG	IESO = ON
	CONFIG	PWRTEN = OFF
	CONFIG	BOREN = OFF
	CONFIG	BORV = 220
	CONFIG	WDTEN = OFF
	CONFIG	WDTPS = 32768
	CONFIG	CCP2MX = PORTC1
	CONFIG	PBADEN = OFF
	CONFIG	CCP3MX = PORTB5
	CONFIG	HFOFST = ON
	CONFIG	T3CMX = PORTC0
	CONFIG	P2BMX = PORTD2
	CONFIG	MCLRE = EXTMCLR
	CONFIG	STVREN = ON
	CONFIG	LVP = ON
	CONFIG	XINST = OFF
	CONFIG	CP0 = OFF
	CONFIG	CP1 = OFF
	CONFIG	CP2 = OFF
	CONFIG	CPB = OFF
	CONFIG	CPD = OFF
	CONFIG	WRT1 = OFF
	CONFIG	WRT2 = OFF
	CONFIG	WRT3 = OFF
	CONFIG	WRTC = OFF
	CONFIG	WRTD = OFF
	CONFIG	EBTR0 = OFF
	CONFIG	EBTR1 = OFF
	CONFIG	EBTR2 = OFF
	CONFIG	EBTR3 = OFF
	CONFIG	EBTRB = OFF

	__IDLOCS	_IDLOC0, 'B'
	__IDLOCS	_IDLOC1, 'S'
	__IDLOCS	_IDLOC2, 'C'
	__IDLOCS	_IDLOC3, 'K'
	__IDLOCS	_IDLOC4, '2'
	__IDLOCS	_IDLOC5, '0'
	__IDLOCS	_IDLOC6, '0'
	__IDLOCS	_IDLOC7, '0'

;
;------------------------------------------------------------------------------
; Segments:
;   ---A---
;  |       |
;  F       B
;  |       |
;   ---G---
;  |       |
;  E       C
;  |       |
;   ---D---  (P)
;
; PortA
;	0 - COM4
;	1 - COM5
;	2 - DP2
;	3 - C2
;	4 - G2
;	5 - spi: nSS
;	6 - F3
;	7 - G3
; PortB
;	0 - A1
;	1 - E1
;	2 - G1
;	3 - F1
;	4 - COM6
;	5 - nLATCH
;	6 - E2
;	7 - D2
; PortC
;	0 - A3
;	1 - COM1
;	2 - B3
;	3 - spi: SCK
;	4 - spi: SDI
;	5 - spi: SDO
;	6 - D3
;	7 - COM3
; PortD
;	0 - COM2
;	1 - E3
;	2 - DP3
;	3 - C3
;	4 - DP1
;	5 - C1
;	6 - D1
;	7 - B1
; PortE
;	0 - F2
;	1 - A2
;	2 - B2
;	3 - reset
;------------------------------------------------------------------------------
;
a		=	0

DISPSECTSIZE	=	3*8		; Three displays are driven at once with 7+1 segments
DISPSTORESIZE	=	6*DISPSECTSIZE	; Six sections of 3 displays

; The following is in the shared space for the interrupt service routine
; and fast access to state variables
isr_cnt		=	0x00	; ISR: # of received SPI bytes
pa_bits		=	0x01	; Port A holder store
pb_bits		=	0x02	; Port B holder store
pc_bits		=	0x03	; Port C holder store
pd_bits		=	0x04	; Port D holder store
pe_bits		=	0x05	; Port E holder store
cnt_1		=	0x06	; Display PWM 1 counter
cnt_2		=	0x07	; Display PWM 2 counter
cnt_3		=	0x08	; Display PWM 3 counter
flags		=	0x09	; Flags for backstore and junk
su_state	=	0x0a	; Startup state
su_cnt		=	0x0b	; Startup temp var
su_iv		=	0x0c	; Startup interval counter
temp		=	0x0d	; Temp var in table lookup

SU_INTERVAL	=	10

BF_STARTUP	=	0	; Set when startup completed
BF_BACKSTORE	=	1	; Backstore indicator bit

; Running variables in linear space
disp_data	=	0x0100	; Bank 1 for display data
disp_dummy	=	disp_data + DISPSTORESIZE	; Dummy addresses in translation table
disp_cpy	=	0x0200	; Bank 2 for back store

; Segment bit asssignment
B_A1		=	0
B_B1		=	7
B_C1		=	5
B_D1		=	6
B_E1		=	1
B_F1		=	3
B_G1		=	2
B_P1		=	4

B_A2		=	1
B_B2		=	2
B_C2		=	3
B_D2		=	7
B_E2		=	6
B_F2		=	0
B_G2		=	4
B_P2		=	2

B_A3		=	0
B_B3		=	2
B_C3		=	3
B_D3		=	6
B_E3		=	1
B_F3		=	6
B_G3		=	7
B_P3		=	2

; Segment port asssignment
P_A1		=	pb_bits
P_B1		=	pd_bits
P_C1		=	pd_bits
P_D1		=	pd_bits
P_E1		=	pb_bits
P_F1		=	pb_bits
P_G1		=	pb_bits
P_P1		=	pd_bits

P_A2		=	pe_bits
P_B2		=	pe_bits
P_C2		=	pa_bits
P_D2		=	pb_bits
P_E2		=	pb_bits
P_F2		=	pe_bits
P_G2		=	pa_bits
P_P2		=	pa_bits

P_A3		=	pc_bits
P_B3		=	pc_bits
P_C3		=	pd_bits
P_D3		=	pc_bits
P_E3		=	pd_bits
P_F3		=	pa_bits
P_G3		=	pa_bits
P_P3		=	pd_bits

; Common line bit assignment
B_COM1		=	1
B_COM2		=	0
B_COM3		=	7
B_COM4		=	0
B_COM5		=	1
B_COM6		=	4
B_LATCH		=	5

; Common line port assignment
P_COM1		=	PORTC
P_COM2		=	PORTD
P_COM3		=	PORTC
P_COM4		=	PORTA
P_COM5		=	PORTA
P_COM6		=	PORTB
P_LATCH		=	PORTB

; Port I/O setting
PA_TRIS		=	0x20
PB_TRIS		=	0x20
PC_TRIS		=	0x18
PD_TRIS		=	0x00
PE_TRIS		=	0xf8

; Off state port values
PA_OFF		=	0xdc
PB_OFF		=	0xcf
PC_OFF		=	0x45
PD_OFF		=	0xfe
PE_OFF		=	0x07

; SPI port definitions
B_SCK		=	3
B_SDI		=	4
B_SDO		=	5
B_SS		=	5

P_SCK		=	PORTC
P_SDI		=	PORTC
P_SDO		=	PORTC
P_SS		=	PORTA

T_SCK		=	TRISC
T_SDI		=	TRISC
T_SDO		=	TRISC
T_SS		=	TRISA

PWMSTEPS	=	256


; Set a bit in the port holders (4)
setbit		macro	cntr, port, bit
		movf	POSTINC0, w, a
		subwf	cntr, w, a	; (f - W) --> W <= f --> C==1 --> off
		btfss	STATUS, C
		bcf	port, bit, a	; Active low
		endm

setbits		macro
		setbit	cnt_1, P_A1, B_A1
		setbit	cnt_1, P_B1, B_B1
		setbit	cnt_1, P_C1, B_C1
		setbit	cnt_1, P_D1, B_D1
		setbit	cnt_1, P_E1, B_E1
		setbit	cnt_1, P_F1, B_F1
		setbit	cnt_1, P_G1, B_G1
		setbit	cnt_1, P_P1, B_P1

		setbit	cnt_2, P_A2, B_A2
		setbit	cnt_2, P_B2, B_B2
		setbit	cnt_2, P_C2, B_C2
		setbit	cnt_2, P_D2, B_D2
		setbit	cnt_2, P_E2, B_E2
		setbit	cnt_2, P_F2, B_F2
		setbit	cnt_2, P_G2, B_G2
		setbit	cnt_2, P_P2, B_P2

		setbit	cnt_3, P_A3, B_A3
		setbit	cnt_3, P_B3, B_B3
		setbit	cnt_3, P_C3, B_C3
		setbit	cnt_3, P_D3, B_D3
		setbit	cnt_3, P_E3, B_E3
		setbit	cnt_3, P_F3, B_F3
		setbit	cnt_3, P_G3, B_G3
		setbit	cnt_3, P_P3, B_P3
		endm

; Recalculate the port holders (10 + 24*4 + 4 + 14 = 124)
setport		macro	oldcomport, oldcombit, comport, combit
		; resetport
		movlw	PA_OFF
		movwf	pa_bits, a
		movlw	PB_OFF
		movwf	pb_bits, a
		movlw	PC_OFF
		movwf	pc_bits, a
		movlw	PD_OFF
		movwf	pd_bits, a
		movlw	PE_OFF
		movwf	pe_bits, a

		rcall	_setbits

		bcf	INTCON, GIE, a
		; Deactivate previous output
		bcf	oldcomport, oldcombit, a
		; Set the output ports
		movff	pa_bits, PORTA
		movff	pb_bits, PORTB
		movff	pc_bits, PORTC
		movff	pd_bits, PORTD
		movff	pe_bits, PORTE
		; Activate the output
		bsf	comport, combit, a
		bsf	INTCON, GIE, a
		endm

storetable	macro	base
		db	LOW(base+0), LOW(base+1)
		db	LOW(base+2), LOW(base+3)
		db	LOW(base+4), LOW(base+5)
		db	LOW(base+6), LOW(base+7)
		endm

cpysegments	macro
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		movff	POSTINC1, POSTINC0
		endm

cpydisplay		macro
		cpysegments		; Copy 18 displays
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		cpysegments
		endm
;
;------------------------------------------------------------------------------
; Reset Entry Point
;------------------------------------------------------------------------------
;
		org	0x00
_reset
		clrf	INTCON, a		; Disable all interrupts
		goto	_main
		nop

;
;------------------------------------------------------------------------------
; Interrupt service routine
; The isr runs on receive of SPI data
;------------------------------------------------------------------------------
;
		org	0x08
_isr
						; (3..5) Entry
		bcf	flags, BF_BACKSTORE, a	; Setup next copy at latch

		movff	isr_cnt, TBLPTRL	; (2)
		tblrd*				; (2)
		movf	TABLAT, w, a		; (1)
		movff	SSP1BUF, PLUSW1		; (2)

		incf	isr_cnt, f, a		; (1) Move to next store position
		bcf	PIR1, SSPIF, a		; (1) Clear SPI interrupt flag
		retfie	1			; (2)

		; ISR timing: 3..5 + 7 + 1 + 1 + 2 = 14..16
		; Max continuous SPI clock = ~8.0MHz
		; Fastest update: 144*8/8.0MHz ~ 144 us (~6940 changes per second)

;
;------------------------------------------------------------------------------
; Main entry point
;------------------------------------------------------------------------------
;
_main
		; Setup default location offsets
		; - FSR0 points to the current data being displayed
		; - FSR1 points to the backing store
		; - TBLPTR points to the SPI datastream address translation table
		movlw	HIGH(disp_data)
		movwf	FSR0H, a
		movlw	HIGH(disp_cpy)
		movwf	FSR1H, a

		movlw	UPPER(_isr_storetable_start)
		movwf	TBLPTRU, a
		movlw	HIGH(_isr_storetable_start)
		movwf	TBLPTRH, a

		movlw	(1<<IRCF2)|(1<<IRCF1)|(1<<IRCF0)|(1<<SCS1)
		movwf	OSCCON, a		; Setup 16MHz internal oscillator

		btfss	OSCCON, HFIOFS, a	; Wait for HFINTOSC stable
		goto	$-1

		bsf	OSCTUNE, PLLEN, a	; Enable PLL

		btfss	OSCCON, PLLRDY, a	; Wait for PLL stable
		goto	$-1

		movlw	PA_OFF			; All LEDs off
		movwf	PORTA, a
		movlw	PB_OFF
		movwf	PORTB, a
		movlw	PC_OFF
		movwf	PORTC, a
		movlw	PD_OFF
		movwf	PORTD, a
		movlw	PE_OFF
		movwf	PORTE, a

		banksel	ANSELA			; Select all digital I/O
		clrf	ANSELA, b
		clrf	ANSELB, b
		clrf	ANSELC, b
		clrf	ANSELD, b
		clrf	ANSELE, b

		movlw	PA_TRIS			; Set I/O mode
		movwf	TRISA, a
		movlw	PB_TRIS
		movwf	TRISB, a
		movlw	PC_TRIS
		movwf	TRISC, a
		movlw	PD_TRIS
		movwf	TRISD, a
		movlw	PE_TRIS
		movwf	TRISE, a

		movlw	(1<<SSP1IE)		; Enable SPI interrupt
		movwf	PIE1, a
		clrf	SSP1STAT, a		; Sample on falling edge
		; SPI: CKP = 0, CKE = 0
		movlw	(1<<SSPEN)|(1<<SSPM2)	; SPI enable, clk-idle = low, slave with nSS
		movwf	SSP1CON1, a
		movlw	(1<<BOEN)		; Overwrite SPI buffer on overrun
		movwf	SSP1CON3, a

		; Note: the counter interleave must be > 1 because the
		;       increments are distributed. Otherwise the interleave
		;       would be gone at some points.
		clrf	cnt_1, a		; Setup PWM counters (interleaved)
		movlw	265*1/3	;2*(256/PWMSTEPS)
		movwf	cnt_2, a
		movlw	256*2/3	;4*(256/PWMSTEPS)
		movwf	cnt_3, a

		clrf	flags, a		; Fresh start
		clrf	su_state, a
		movlw	SU_INTERVAL
		movwf	su_iv, a

		movlw	DISPSTORESIZE		; Boot time clear display
		movwf	su_cnt, a
		clrf	FSR0L, a
		movlw	0
_boot_clr
		movwf	POSTINC0, a
		decfsz	su_cnt, f, a
		goto	_boot_clr

		clrf	PIR1, a			; Clear any hanging SPI interrupt
		movlw	(1<<GIE)|(1<<PEIE)
		movwf	INTCON			; Enable interrupts

		; PWM 256 steps results in:
		; (64MHz / 4) / ((124 + 2 + 34) * 6 * 256) ~ 65.1 Hz
		; Segment change overhead: (14-1) / (124 + 2 + 34) ~ 8.1%
		goto	_loopentry

;------------------------------------------------------------------------------

_mainloop
		btfss	flags, BF_STARTUP, a
		rcall	_startup_show
		rcall	_smaller_delay		; needs to be 2 cycles less because of the bottom goto
		setport	P_COM3, B_COM3, P_COM4, B_COM4

		btfss	P_LATCH, B_LATCH, a	; Reset the SPI data counter on latch
		rcall	_cpy_disp_data		; and copy the backing store
		rcall	_small_delay
		setport	P_COM4, B_COM4, P_COM5, B_COM5

		movlw	(256/PWMSTEPS)
		addwf	cnt_1, f, a
		rcall	_small_delay
		setport	P_COM5, B_COM5, P_COM6, B_COM6

_loopentry
		movlw	LOW(disp_data)		; Setup data pointer
		movwf	FSR0L, a
		rcall	_small_delay
		setport	P_COM6, B_COM6, P_COM1, B_COM1

		movlw	(256/PWMSTEPS)
		addwf	cnt_2, f, a
		rcall	_small_delay
		setport	P_COM1, B_COM1, P_COM2, B_COM2

		movlw	(256/PWMSTEPS)
		addwf	cnt_3, f, a
		rcall	_small_delay
		setport	P_COM2, B_COM2, P_COM3, B_COM3

		goto	_mainloop
;
;------------------------------------------------------------------------------
; small  : Delay 34 cycles (including call and return)
; smaller: Delay 32 cycles (including call and return)
;------------------------------------------------------------------------------
;
_small_delay				; 2 (entry)
		nop			; 1
		nop			; 1
_smaller_delay
		nop			; 1
		nop
		nop
		movlw	8		; 1
		decfsz	WREG, w, a	; 8*3
		goto	$-1
		return			; 2
;
;------------------------------------------------------------------------------
; Show some kind of activity at startup. All segments are set to light in all
; possible intensities.
; Just to show-off...
;------------------------------------------------------------------------------
;
_startup_show
		btfss	P_LATCH, B_LATCH, a	; Quit startup if latch active
		goto	_startup_done

		movf	cnt_1, f, a		; Only do this once every full PWM cycle
		btfsc	STATUS, Z, a
		return

		decfsz	su_iv, f, a
		return

		movlw	SU_INTERVAL
		movwf	su_iv, a

		incf	su_state, f, a		; Done when we have a full state cycle
;		btfsc	STATUS, Z		; This gives us 256/67.2Hz ~ 3.8 seconds show
;		goto	_startup_done

		movlw	7
		decfsz	WREG, f, a
		goto	$-1

; FIXME: this does not work properly
		bcf	P_COM3, B_COM3, a	; Shut down to compensate intensity

		movlw	DISPSTORESIZE
		movwf	su_cnt, a
if 1
_startup_loop
		movf	su_cnt, w, a
		addlw	-1
		call	_su_segtable
		movwf	FSR0L, a
		movf	su_state, w, a
		addwf	su_cnt, w, a
		rlcf	WREG, w, a
		btfsc	STATUS, C, a
		;comf	WREG, w
		movwf	INDF0, a
		decfsz	su_cnt, f, a
		goto	_startup_loop
else
		clrf	FSR0L, a
		movf	su_state, w, a
		addwf	su_state, w, a
		addwf	su_cnt, w, a
		addwf	su_cnt, w, a
_startup_loop
		movwf	POSTINC0, a
		decfsz	su_cnt, f, a
		goto	_startup_loop
endif
		movlw	LOW(disp_data + 3*DISPSECTSIZE)	; Restore FSR0
		movwf	FSR0L, a
		return

_startup_done
		movlw	DISPSTORESIZE		; Startup done, clear the display
		movwf	su_cnt, a
		clrf	FSR0L, a
		movlw	0
_startup_clr
		movwf	POSTINC0, a
		decfsz	su_cnt, f, a
		goto	_startup_clr

		movlw	LOW(disp_data + 3*DISPSECTSIZE)	; Restore FSR0
		movwf	FSR0L, a
		bsf	flags, BF_STARTUP, a	; Set done flag
		return
;
;------------------------------------------------------------------------------
; Recalculate the port values for the next PWM interval
;------------------------------------------------------------------------------
;
_setbits
		setbits
		return
;
;------------------------------------------------------------------------------
; Copy the backing storage to the display The PWM cycle is >14ms, but the
; individual PWM updates are about 223us apart. Therefore, using a backing
; store enhances the snappiness of the updates in both in- and de-creasing
; intensities as it takes ~18.75us to copy the data. Also, it prevents slow
; updates from being annoying (when the SPI clock is slow or byte intervals
; are large).
; The copying is loop-unrolled to make it faster.
;------------------------------------------------------------------------------
;
_cpy_disp_data
		btfsc	flags, BF_BACKSTORE, a	; No need to do it again if done
		return				; LATCH may be active longer than the PWM cycle.
						; (2)
		movlw	LOW(disp_data)		; Copy backup data
		movwf	FSR0L, a
		movlw	LOW(disp_cpy)
		movwf	FSR1L, a
						; (4)
		cpydisplay			; (288) *FSR0 = *FSR1 for all data
		movlw	LOW(disp_data + 4*DISPSECTSIZE)	; Re-setup FSR0, FSR1 will get set in the ISR
		movwf	FSR0L, a
		bsf	flags, BF_BACKSTORE, a
		clrf	isr_cnt, a		; Reset storage counter for next SPI data
						; (4)
		return				; (2)
;
;------------------------------------------------------------------------------
; Get the startup segment mapping for lighting in the correct order
;------------------------------------------------------------------------------
;
_su_segtable
		movff	TBLPTRH, temp
		bcf	INTCON, GIE
		movwf	TBLPTRL, a
		movlw	HIGH(_su_segtable_start)
		movwf	TBLPTRH, a
		tblrd*
		movf	TABLAT, w, a
		movff	temp, TBLPTRH
		bsf	INTCON, GIE
		return
;
;------------------------------------------------------------------------------
; Storage lookup table for making the segments align nicely in the SPI
; datastream.
;------------------------------------------------------------------------------
		org ($+0xff) & (~0xff)
_isr_storetable_start
		storetable	disp_cpy+0	; Inner three displays
		storetable	disp_cpy+8
		storetable	disp_cpy+16

		storetable	disp_cpy+24	; Middle six displays
		storetable	disp_cpy+48
		storetable	disp_cpy+32
		storetable	disp_cpy+56
		storetable	disp_cpy+40
		storetable	disp_cpy+64

		storetable	disp_cpy+72	; Outer nine displays
		storetable	disp_cpy+96
		storetable	disp_cpy+120
		storetable	disp_cpy+80
		storetable	disp_cpy+104
		storetable	disp_cpy+128
		storetable	disp_cpy+88
		storetable	disp_cpy+112
		storetable	disp_cpy+136

		storetable	disp_dummy	; Fill to 32 entries of 8 bytes to make 0x100 size
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy
		storetable	disp_dummy

;
; The 'db' statements need to be combined te get continuous memory usage.
; With the entries being an odd number, we combine two in one macro-call.
;
seg_innertable	macro	sega, segb
		db	LOW(disp_cpy+sega+16), LOW(disp_cpy+sega+ 8)
		db	LOW(disp_cpy+sega+ 0), LOW(disp_cpy+segb+16)
		db	LOW(disp_cpy+segb+ 8), LOW(disp_cpy+segb+ 0)
		endm

seg_middletable	macro	sega, segb
		db	LOW(disp_cpy+sega+64), LOW(disp_cpy+sega+40)
		db	LOW(disp_cpy+sega+56), LOW(disp_cpy+sega+32)
		db	LOW(disp_cpy+sega+48), LOW(disp_cpy+sega+24)
		db	LOW(disp_cpy+segb+64), LOW(disp_cpy+segb+40)
		db	LOW(disp_cpy+segb+56), LOW(disp_cpy+segb+32)
		db	LOW(disp_cpy+segb+48), LOW(disp_cpy+segb+24)
		endm

seg_outertable	macro	sega, segb
		db	LOW(disp_cpy+sega+136), LOW(disp_cpy+sega+112)
		db	LOW(disp_cpy+sega+ 88), LOW(disp_cpy+sega+128)
		db	LOW(disp_cpy+sega+104), LOW(disp_cpy+sega+ 80)
		db	LOW(disp_cpy+sega+120), LOW(disp_cpy+sega+ 96)
		db	LOW(disp_cpy+sega+ 72), LOW(disp_cpy+segb+136)
		db	LOW(disp_cpy+segb+112), LOW(disp_cpy+segb+ 88)
		db	LOW(disp_cpy+segb+128), LOW(disp_cpy+segb+104)
		db	LOW(disp_cpy+segb+ 80), LOW(disp_cpy+segb+120)
		db	LOW(disp_cpy+segb+ 96), LOW(disp_cpy+segb+ 72)
		endm

		org ($+0xff) & (~0xff)
_su_segtable_start
		seg_outertable	0, 5	; A, F
		seg_outertable	1, 6	; B, G
		seg_outertable	4, 2	; E, C
		seg_outertable	3, 7	; D, P

		seg_middletable	0, 5	; A, F
		seg_middletable	1, 6	; B, G
		seg_middletable	4, 2	; E, C
		seg_middletable	3, 7	; D, P

		seg_innertable	0, 5	; A, F
		seg_innertable	1, 6	; B, G
		seg_innertable	4, 2	; E, C
		seg_innertable	3, 7	; D, P
		end
