/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <network.h>
#include <ethernet.h>
#include <ip.h>
#include <udp.h>
#include <display.h>
#include <config.h>
#include <netctrl.h>
#include <spi.h>
#include <move.h>
#include <clock.h>
#include <serial.h>

//#define DEBUG_NETCTRL	1

#define REMOTE_TICKS	5000	/* Reset to local control after 5 seconds */

#define _iph	packet.ethpkt.iph

static void netctrl_send_cfg(uint16_t sport);

uint8_t netctrl_handle_rxpacket(uint16_t pktlen)
{
	uint16_t plen;
	netctrl_t *nc;
	(void)pktlen;

	/* Point to the start of the data */
	nc = (netctrl_t *)(((uint8_t *)&_iph) + iphdr_size(&_iph) + sizeof(udphdr_t));
	plen = ntohs(_iph.len) - iphdr_size(&_iph) - sizeof(udphdr_t);

	if(plen < 2 || plen > sizeof(netctrl_t))
		return 0;

	clock_timer_set(TIMER_REMOTE, REMOTE_TICKS);

	switch(ntohs(nc->cmd)) {
	case NETCMD_SETMODE:
		if(plen != sizeof(nc->cmd) + sizeof(nc->mode))
			return 0;
		display_set_mode(nc->mode);
		break;
	case NETCMD_SETDATA:
		display_set_mode(DISPMODE_REMOTE);
		if(plen != sizeof(nc->cmd) + sizeof(nc->sections))
			return 0;
		memcpy(&segments, nc->sections, sizeof(segments));
		spi_update();
		break;
	case NETCMD_SETTIMEOUT:
		if(plen != sizeof(nc->cmd) + sizeof(nc->timeout))
			return 0;
		cfg.movetimeout = ntohl(nc->timeout);
		break;
	case NETCMD_GETCFG:
		if(plen != sizeof(nc->cmd))
			return 0;
		netctrl_send_cfg(((udphdr_t *)(((uint8_t *)&_iph) + iphdr_size(&_iph)))->sport);
		break;
	default:
#ifdef DEBUG_NETCTRL
		serial_puts("netctrl: junk cmd 0x");
		serial_puthex_u16(ntohs(nc->cmd));
		serial_puts(" len 0x");
		serial_puthex_u16(plen);
		serial_putc('\n');
#endif
		return 0;
	}
	return 1;
}

void netctrl_handle_timer(void)
{
	clock_timer_clear(TIMER_REMOTE);
	if(display_get_mode() == DISPMODE_REMOTE)
		display_set_mode(DISPMODE_CLOCK);
}

#define _npkt	(*(netctrlpkt_t *)&packet)
#define PKTSIZE	(sizeof(_npkt.nc.cmd) + sizeof(_npkt.nc.value))

static void netctrl_broadcast_values(void)
{
	MAC_CP(_npkt.eth.sa, cfg.macaddr);
	MAC_SET(_npkt.eth.da, 0xff);	/* Broadcast dest */
	IP_CP(_npkt.ip.saddr, cfg.ipaddr);
	IP_SET(_npkt.ip.daddr, 0xff);
	_npkt.eth.lenw = htons(ETHTYPE_IP);
	_npkt.ip.version = 4;
	_npkt.ip.hdrlen = 5;
	_npkt.ip.ttl = IP_TTL_DEFAULT;
	_npkt.ip.proto = IPPROTO_UDP;
	//_npkt.ip.checksum = 0;
	_npkt.udp.sport = htons(cfg.port);
	_npkt.udp.dport = htons(cfg.port);
	//_npkt.udp.checksum = 0;

	cli(); _npkt.nc.value.timestamp = htonl(clock_secs); sti();

	_npkt.ip.len = htons(sizeof(_npkt.ip) + sizeof(_npkt.udp) + PKTSIZE);
	_npkt.ip.checksum = ip_checksum(&_npkt.ip, sizeof(_npkt.ip));
	_npkt.udp.length = htons(sizeof(_npkt.udp) + PKTSIZE);
	_npkt.udp.checksum = udp_checksum(&_npkt.ip, sizeof(_npkt.udp) + PKTSIZE);
	ethernet_txpacket(((char *)&_npkt.nc - (char *)&_npkt) + PKTSIZE);
	/* Note: we actually do not care if the packet got send because the
	 * we will send another one on move or timer
	 */
}

void netctrl_broadcast_light(uint8_t light)
{
	if(display_get_mode() != DISPMODE_REMOTE)
		return;
	memset(&_npkt, 0, sizeof(netctrlpkt_t)-sizeof(_npkt.nc) + PKTSIZE);
	_npkt.nc.cmd = htons(NETCMD_LIGHT);
	_npkt.nc.value.light = light;
	netctrl_broadcast_values();
}

void netctrl_broadcast_move(uint8_t mv)
{
	if(display_get_mode() != DISPMODE_REMOTE)
		return;
	memset(&_npkt, 0, sizeof(netctrlpkt_t)-sizeof(_npkt.nc) + PKTSIZE);
	_npkt.nc.cmd = htons(NETCMD_MOVE);
	_npkt.nc.value.move = mv;
	netctrl_broadcast_values();
}

#undef PKTSIZE
#define PKTSIZE	(sizeof(_npkt.nc.cmd) + sizeof(_npkt.nc.config))
static void netctrl_send_cfg(uint16_t sport)
{
	uint8_t ip[4];
	/* Reply to the get config command */
	/* The MAC was recorded at the IP level and we assume that the source MAC and */
	/* IP are correct to reach the target... or we will cause backscatter. */
	IP_CP(ip, _npkt.ip.saddr);
	memset(&_npkt, 0, sizeof(netctrlpkt_t)-sizeof(_npkt.nc) + PKTSIZE);
	if(!arp_resolve(ip, _npkt.eth.da)) {
		return;
	}
	MAC_CP(_npkt.eth.sa, cfg.macaddr);
	_npkt.eth.lenw = htons(ETHTYPE_IP);
	IP_CP(_npkt.ip.daddr, ip);
	IP_CP(_npkt.ip.saddr, cfg.ipaddr);
	_npkt.ip.version = 4;
	_npkt.ip.hdrlen = 5;
	_npkt.ip.ttl = IP_TTL_DEFAULT;
	_npkt.ip.proto = IPPROTO_UDP;
	_npkt.udp.sport = htons(cfg.port);
	_npkt.udp.dport = sport;
	memcpy(&_npkt.nc.config.cfg, &cfg, sizeof(_npkt.nc.config.cfg));
	memcpy(_npkt.nc.config.serial, serialnr, sizeof(serialnr));

	_npkt.ip.len = htons(sizeof(_npkt.ip) + sizeof(_npkt.udp) + PKTSIZE);
	_npkt.ip.checksum = ip_checksum(&_npkt.ip, sizeof(_npkt.ip));
	_npkt.udp.length = htons(sizeof(_npkt.udp) + PKTSIZE);
	_npkt.udp.checksum = udp_checksum(&_npkt.ip, sizeof(_npkt.udp) + PKTSIZE);
	ethernet_txpacket(((char *)&_npkt.nc - (char *)&_npkt) + PKTSIZE);
}
