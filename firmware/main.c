/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <network.h>
#include <config.h>
#include <io.h>
#include <serial.h>
#include <clock.h>
#include <ethernet.h>
#include <cmdline.h>
#include <arp.h>
#include <dhcp.h>
#include <ip.h>
#include <adc.h>
#include <spi.h>
#include <display.h>
#include <move.h>
#include <i2c.h>
#include <rtcc.h>
#include <ntp.h>
#include <netctrl.h>
#include <event.h>

#if 1
#pragma config DEBUG=OFF
#pragma config XINST=OFF
#pragma config STVR=OFF
#pragma config WDT=OFF
#pragma config CP0=OFF
#pragma config FCMEN=ON
#pragma config IESO=ON
#pragma config FOSC2=ON
#pragma config FOSC=HSPLL
#pragma config WDTPS=1024
#pragma config ETHLED=ON
#endif

#define WHEEL_TICKS		1000
#define GREEN_TICKS		250
#define GREEN_TICKS_LOW		450
#define GREEN_TICKS_HIGH	50

static const char wheelchars[4] = {'|', '/', '-', '\\'};


/*
 * Interrupt service
 * We get called by the c0 code as a normal function
 */
void isr(void)
{
	if(PIE3bits.TMR4IE && PIR3bits.TMR4IF)
		clock_isr();
	if(PIE1bits.SSP1IE && PIR1bits.SSP1IF)
		spi_isr();
	if(PIE1bits.RC1IE && PIR1bits.RC1IF)
		serial_recv_isr();
	if(PIE1bits.TX1IE && PIR1bits.TX1IF)
		serial_send_isr();
	if(PIE2bits.ETHIE && PIR2bits.ETHIF)
		ethernet_isr();
	if(PIE1bits.ADIE && PIR1bits.ADIF)
		adc_isr();
	if(PIE2bits.CMIE && PIR2bits.CMIF)
		move_isr();

	clock_entropy.bytes[0] ^= PRODL;
	clock_entropy.bytes[1] ^= PRODH;
	clock_entropy.bytes[2] ^= FSR0L;
	clock_entropy.bytes[3] ^= FSR0H;
}

static void delay_100us(void)
{
	__asm
	movlw	208
start_delayloop:
	nop
	nop
	nop
	nop
	addlw	255		; w--
	bnz	start_delayloop	; 4/41.667 * 5 * 208 --> ~99.84us
	__endasm;
}

int main(void)
{
	static uint8_t wcnt;

	/* Janitorial */
	cli();
	RCON = 0x30;		/* Reset condition */

	/* First setup the CLR signal for all boards to hold them in reset */
	io_init();		/* Basic I/O enable (i.e. LEDs) */
	io_display_disable();	/* Kill the display's startup pattern */

	/* Setup xtal osc */
	OSCTUNE = 0x40;		/* Set 41.667MHz */
	OSCCON = 0x02;		/* on the primary oscillator */

	/* Delay ~20ms to settle clock */
	for(wcnt = 0; wcnt < 200; wcnt++)
		delay_100us();

	WDTCONbits.SWDTEN = 1;	/* Enable the watchdog */
	__asm
	clrwdt
	__endasm;

	clock_init();		/* Setup the clock so we may use timing */
	serial_init();		/* Now we can use serial output */

	/* Delay ~20ms to settle the serial TX/RX lines to idle */
	for(wcnt = 0; wcnt < 200; wcnt++)
		delay_100us();

	wcnt = 0;		/* TIMER_WHEEL character display */

	/* Enable interrupts */
	INTCONbits.PEIE = 1;
	sti();

	/* Flush junk */
	serial_puts("\n\nBooting...");

	io_led_ip(0);		/* Give the user some indication asap after boot */
	io_led_move(1);
	io_led_cpu(1);
	io_led_err(1);

	i2c_init();
	serial_putc('1');
	if(!config_load())
		serial_puts("\nNo valid config found, generated new default one.\n");
	serial_putc('2');
	rtcc_init();		/* This will set the system clock from the RTC */
	serial_putc('3');
	arp_init();
	serial_putc('4');
	ip_init();
	serial_putc('5');
	dhcp_init();
	serial_putc('6');
	ntp_init();
	serial_putc('7');
	ethernet_init();
	serial_putc('8');
	adc_init();
	serial_putc('9');
	display_init();
	serial_putc('A');
	spi_init();
	serial_putc('B');
	cmdline_init();
	serial_putc('C');
	move_init();
	serial_putc('D');
	event_init();
	serial_putc('E');

	io_display_enable();

	serial_puts("\n\n");

	clock_timer_set(TIMER_BUTTON, BUTTON_TICKS);
	clock_timer_set(TIMER_GREEN, GREEN_TICKS);
	clock_timer_set(TIMER_WHEEL, WHEEL_TICKS);
	if(cfg.dhcp_on)
		clock_timer_set(TIMER_DHCP, DHCPTIMER_STARTTICKS);

	io_led_ip_toggle();
	io_led_ip(0);
	io_led_move(0);
	io_led_cpu(1);
	io_led_err(0);

	for(;;) {
		/* Kick the dog */
		__asm
		clrwdt
		__endasm;

		if(clock_timer_expired(TIMER_SPI))
			spi_handle_timer();

		if(ethlinkchange) {
			ethlinkchange = 0;
			dhcp_linkchange();
#if 0
			if(ethlink == ETH_LINK_UP) {
				/*serial_puts("Ethernet: Link up\n");*/
				io_yellowl(1);
			} else {
				/*serial_puts("Ethernet: Link down\n");*/
				io_yellowl(0);
			}
#endif
		}

		if(clock_timer_expired(TIMER_BUTTON))
			io_handle_timer_button();

		if(clock_timer_expired(TIMER_GREEN)) {
			/* Blink pattern:
			 *   Off   On
			 * - long  short  ethernet link down
			 * - half  half   ethernet link up, no IP set
			 * - short long   ethernet link up, have an IP
			 */
			if(ethlink == ETH_LINK_UP) {
				if(cfg.ipaddr[0])
					clock_timer_set(TIMER_GREEN, io_led_ip_state() ? GREEN_TICKS_HIGH : GREEN_TICKS_LOW);
				else
					clock_timer_set(TIMER_GREEN, GREEN_TICKS);
			} else {
				clock_timer_set(TIMER_GREEN, io_led_ip_state() ? GREEN_TICKS_LOW : GREEN_TICKS_HIGH);
			}
			io_led_ip_toggle();
		}

		if(clock_timer_expired(TIMER_ARP))
			arp_handle_timer();

		if(clock_timer_expired(TIMER_WHEEL)) {
			clock_timer_set(TIMER_WHEEL, WHEEL_TICKS);
			serial_putc(wheelchars[wcnt++]);
			serial_putc(0x08);
			wcnt %= sizeof(wheelchars);
		}

		if(clock_timer_expired(TIMER_DHCP))
			dhcp_handle_timer();

		if(clock_timer_expired(TIMER_DISPLAY))
			display_handle_timer();

		if(clock_timer_expired(TIMER_NTP))
			ntp_handle_timer();

		if(clock_timer_expired(TIMER_MOVE))
			move_handle_timer();

		if(clock_timer_expired(TIMER_REMOTE))
			netctrl_handle_timer();

		if(clock_timer_expired(TIMER_ADC_BC))
			adc_handle_bctimer();

		if(clock_timer_expired(TIMER_CONFIG))
			event_handle_timer();

		/* The move detection is in an interrupt; we send the data here */
		if(bcmove) {
			bcmove = 0;
			netctrl_broadcast_move(1);
		}

		if(ethernet_rxpacket_ready())
			ethernet_handle_rxpacket();
		else
			EIEbits.PKTIE = 1;	/* Re-enable the interrupt to wake us from sleep */

		cmdline_loop();

		/* Worst case race: a packet arrives after the test and we go to sleep. We
		 * don't care, the PKTIF flag gets set and an interrupt is generated. However,
		 * we clear the interrupt-enable flag in the service routine and thus will go
		 * to sleep below...
		 * We still would be awoken in a millisecond or so to handle the clock timer.
		 * So, we can risk that more than one packet arrives in the meantime, but that
		 * is no real problem IMO. It sucks for ping-time stability, but who is
		 * checking that anyway...
		 */

		if(!ethernet_rxpacket_ready()) {
			/* Goto sleep if no packets are waiting. There is no point in spinning */
			io_led_cpu(0);
			OSCCONbits.IDLEN = 1;
			__asm
			clrwdt
			__endasm;
			/* The A/D must be started after IDLEN for it to run in sleep mode */
			if(clock_timer_expired(TIMER_ADC))
				adc_handle_timer();
			__asm
			sleep
			__endasm;
			io_led_cpu(1);
			io_led_cpu(0);	/* Only a very small pulse */
			io_led_err(0);	/* Clear errors (probably eth rx error) */
		} else
			io_led_cpu(1);	/* We are really busy... */
	}
}
