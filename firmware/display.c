/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <display.h>
#include <clock.h>
#include <spi.h>
#include <adc.h>
#include <io.h>
#include <config.h>
#include <event.h>

uint8_t dispmode;
static uint8_t dispstate;

#ifdef LINEAR_DISPLAY
#ifdef ABS_SEGMENTS
__data lineardisplay_t __at __SEGMENTS_ADDRESS segments;
#else
lineardisplay_t segments;
#endif
#else
#ifdef ABS_SEGMENTS
__data section_t __at __SEGMENTS_ADDRESS segments[NSECTIONS];
#else
section_t segments[NSECTIONS];
#endif
#endif

static const uint8_t alphabet[] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x86, 0x22, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x80, 0x00,	/*  !"#$%&'()*+,-./ */
	0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0x00, 0x00, 0x00, 0x44, 0x00, 0x00,	/* 0123456789:;<=>? */
	0x00, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, 0x6f, 0x76, 0x06, 0x0e, 0x70, 0x38, 0x37, 0x54, 0x5c,	/* @ABCDEFGHIJKLMNO */
	0x73, 0xdc, 0x50, 0x6d, 0x78, 0x1c, 0x3e, 0x3e, 0x76, 0x6a, 0x5b, 0x00, 0x00, 0x00, 0x00, 0x08,	/* PQRSTUVWXYZ[\]^_ */
	0x20, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, 0x3d, 0x76, 0x06, 0x0e, 0x70, 0x38, 0x37, 0x54, 0x5c,	/* `abcdefghijklmno */
	0x73, 0xdc, 0x50, 0x6d, 0x78, 0x1c, 0x3e, 0x3e, 0x76, 0x6a, 0x5b, 0x00, 0x00, 0x00, 0x00, 0x00,	/* pqrstuvwxyz{|}~  */
};

static const uint8_t digits[] = {
	0x3f,		/* 0 00111111 */
	0x06,		/* 1 00000110 */
	0x5b,		/* 2 01011011 */
	0x4f,		/* 3 01001111 */
	0x66,		/* 4 01100110 */
	0x6d,		/* 5 01101101 */
	0x7d,		/* 6 01111101 */
	0x07,		/* 7 00000111 */
	0x7f,		/* 8 01111111 */
	0x6f,		/* 9 01101111 */
	0x77,		/* A 01110111 */
	0x7c,		/* B 01111100 */
	0x39,		/* C 00111001 */
	0x5e,		/* D 01011110 */
	0x79,		/* E 01111001 */
	0x71		/* F 01110001 */
};

void display_init(void)
{
	display_clear_mem();
	clock_timer_set(TIMER_DISPLAY, DISPLAY_TICKS);
	dispmode = DISPMODE_CLOCK;
	//dispmode = DISPMODE_RINGING;
	dispstate = 0;
}

void display_clear_mem(void)
{
#if 0
	__asm
	lfsr	0, _segments
	movlw	0
clrloop1:			; clear 0..255
	clrf	_POSTINC0
	addlw	1
	bnz	clrloop1
clrloop2:			; clear 256..511
	clrf	_POSTINC0
	addlw	1
	bnz	clrloop2
	movlw	-64
clrloop3:			; clear 512..575
	clrf	_POSTINC0
	addlw	1
	bnz	clrloop2
	__endasm;
#else
	memset(&segments, 0, sizeof(segments));
#endif
}

static void set_segment_digit(display_t *d, uint8_t n, uint8_t val)
{
	n = digits[n & 0x0f];
	if(n & 0x01)	d->a = val;
	if(n & 0x02)	d->b = val;
	if(n & 0x04)	d->c = val;
	if(n & 0x08)	d->d = val;
	if(n & 0x10)	d->e = val;
	if(n & 0x20)	d->f = val;
	if(n & 0x40)	d->g = val;
	/*if(n & 0x80)	d->p = val;*/
}

static void set_segment_alpha(display_t *d, uint8_t n, uint8_t val)
{
	uint8_t x = alphabet[n & 0x7f];
	if(x & 0x01)		d->a = val;
	if(x & 0x02)		d->b = val;
	if(x & 0x04)		d->c = val;
	if(x & 0x08)		d->d = val;
	if(x & 0x10)		d->e = val;
	if(x & 0x20)		d->f = val;
	if(x & 0x40)		d->g = val;
	if((n | x) & 0x80)	d->p = val;
}

/* Set ring effect moving outward */
#define lcval(x)	(x)
#define LCSTEP(x)	(3*(x))
static void show_rings(uint8_t state) __wparam
{
	uint8_t i;
	uint8_t v = state << 2;
	for(i = 0; i < NINNER; i++) {
		segments.ringinner[i].d = lcval(v+LCSTEP(0));
		segments.ringinner[i].c = segments.ringinner[i].e = lcval(v+LCSTEP(1));
		segments.ringinner[i].g = lcval(v+LCSTEP(2));
		segments.ringinner[i].b = segments.ringinner[i].f = lcval(v+LCSTEP(3));
		segments.ringinner[i].a = lcval(v+LCSTEP(4));
		segments.ringinner[i].p = 0;
		v += LCSTEP(1);
	}
	for(i = 0; i < NMIDDLE; i++) {
		segments.ringmiddle[i].d = lcval(v+LCSTEP(0));
		segments.ringmiddle[i].c = segments.ringmiddle[i].e = lcval(v+LCSTEP(1));
		segments.ringmiddle[i].g = lcval(v+LCSTEP(2));
		segments.ringmiddle[i].b = segments.ringmiddle[i].f = lcval(v+LCSTEP(3));
		segments.ringmiddle[i].a = lcval(v+LCSTEP(4));
		segments.ringmiddle[i].p = 0;
		v += LCSTEP(1);
	}
	for(i = 0; i < NOUTER; i++) {
		segments.ringouter[i].d = lcval(v+LCSTEP(0));
		segments.ringouter[i].c = segments.ringouter[i].e = lcval(v+LCSTEP(1));
		segments.ringouter[i].g = lcval(v+LCSTEP(2));
		segments.ringouter[i].b = segments.ringouter[i].f = lcval(v+LCSTEP(3));
		segments.ringouter[i].a = lcval(v+LCSTEP(4));
		segments.ringouter[i].p = 0;
		v += LCSTEP(1);
	}
#if 0
	state <<= 2;
	segments.ringouter[0].a = state + (0 << 2);
	segments.ringouter[0].b = segments.ringouter[0].f = state + (1 << 2);
	segments.ringouter[0].g = state + (2 << 2);
	segments.ringouter[0].c = segments.ringouter[0].e = state + (3 << 2);
	segments.ringouter[0].d = state + (4 << 2);
	segments.ringouter[0].p = 0;
	memcpy(&segments.ringouter[1], &segments.ringouter[0], sizeof(segments.ringouter[0]));
	memcpy(&segments.ringouter[2], &segments.ringouter[0], 2*sizeof(segments.ringouter[0]));
	memcpy(&segments.ringouter[4], &segments.ringouter[0], 4*sizeof(segments.ringouter[0]));
	memcpy(&segments.ringouter[8], &segments.ringouter[0], 8*sizeof(segments.ringouter[0]));
	memcpy(&segments.ringouter[16], &segments.ringouter[0], 16*sizeof(segments.ringouter[0]));
	memcpy(&segments.ringouter[32], &segments.ringouter[0], 4*sizeof(segments.ringouter[0]));

	segments.ringmiddle[0].a = state + (5 << 2);
	segments.ringmiddle[0].b = segments.ringmiddle[0].f = state + (6 << 2);
	segments.ringmiddle[0].g = state + (7 << 2);
	segments.ringmiddle[0].c = segments.ringmiddle[0].e = state + (8 << 2);
	segments.ringmiddle[0].d = state + (9 << 2);
	segments.ringmiddle[0].p = 0;
	memcpy(&segments.ringmiddle[1], &segments.ringmiddle[0], sizeof(segments.ringmiddle[0]));
	memcpy(&segments.ringmiddle[2], &segments.ringmiddle[0], 2*sizeof(segments.ringmiddle[0]));
	memcpy(&segments.ringmiddle[4], &segments.ringmiddle[0], 4*sizeof(segments.ringmiddle[0]));
	memcpy(&segments.ringmiddle[8], &segments.ringmiddle[0], 8*sizeof(segments.ringmiddle[0]));
	memcpy(&segments.ringmiddle[16], &segments.ringmiddle[0], 8*sizeof(segments.ringmiddle[0]));

	segments.ringinner[0].a = state + (10 << 2);
	segments.ringinner[0].b = segments.ringinner[0].f = state + (11 << 2);
	segments.ringinner[0].g = state + (12 << 2);
	segments.ringinner[0].c = segments.ringinner[0].e = state + (13 << 2);
	segments.ringinner[0].d = state + (14 << 2);
	segments.ringinner[0].p = 0;
	memcpy(&segments.ringinner[1], &segments.ringinner[0], sizeof(segments.ringinner[0]));
	memcpy(&segments.ringinner[2], &segments.ringinner[0], 2*sizeof(segments.ringinner[0]));
	memcpy(&segments.ringinner[4], &segments.ringinner[0], 4*sizeof(segments.ringinner[0]));
	memcpy(&segments.ringinner[8], &segments.ringinner[0], 4*sizeof(segments.ringinner[0]));
#endif
}

#ifdef LINEAR_DISPLAY

static void show_clock(uint8_t val)
{
	display_clear_mem();
	clock_localtime(0);

	if(clock_timeparts.hour >= 10) {
		set_segment_digit(&segments.ringinner[inner_idx(NINNER*(clock_timeparts.hour%12) / 12 - 1)], clock_timeparts.hour / 10, val);
		set_segment_digit(&segments.ringinner[inner_idx(NINNER*(clock_timeparts.hour%12) / 12 - 0)], clock_timeparts.hour % 10, val);
	} else
		set_segment_digit(&segments.ringinner[inner_idx(NINNER*clock_timeparts.hour / 12 - 1)], clock_timeparts.hour % 10, val);

	set_segment_digit(&segments.ringmiddle[middle_idx(NMIDDLE*clock_timeparts.min / 60 - 1)], clock_timeparts.min / 10, val);
	set_segment_digit(&segments.ringmiddle[middle_idx(NMIDDLE*clock_timeparts.min / 60 - 0)], clock_timeparts.min % 10, val);

	set_segment_digit(&segments.ringouter[outer_idx(NOUTER*clock_timeparts.sec / 60 - 1)], clock_timeparts.sec / 10, val);
	set_segment_digit(&segments.ringouter[outer_idx(NOUTER*clock_timeparts.sec / 60 - 0)], clock_timeparts.sec % 10, val);

	segments.ringinner[inner_idx(clock_msecs*3/250)].p = val;
}

static const int mdays[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

static void show_date(uint8_t val)
{
	display_clear_mem();
	clock_localtime(0);

	set_segment_digit(&segments.ringinner[inner_idx(-2)], clock_timeparts.year / 1000, val);
	set_segment_digit(&segments.ringinner[inner_idx(-1)], (clock_timeparts.year % 1000) / 100, val);
	set_segment_digit(&segments.ringinner[inner_idx( 0)], (clock_timeparts.year % 100) / 10, val);
	set_segment_digit(&segments.ringinner[inner_idx( 1)], clock_timeparts.year % 10, val);

	set_segment_digit(&segments.ringmiddle[middle_idx(NMIDDLE*clock_timeparts.mon / 12 - 1)], (clock_timeparts.mon+1) / 10, val);
	set_segment_digit(&segments.ringmiddle[middle_idx(NMIDDLE*clock_timeparts.mon / 12 - 0)], (clock_timeparts.mon+1) % 10, val);

	set_segment_digit(&segments.ringouter[outer_idx(NOUTER*(clock_timeparts.mday-1) / mdays[clock_timeparts.mon] - 1)], clock_timeparts.mday / 10, val);
	set_segment_digit(&segments.ringouter[outer_idx(NOUTER*(clock_timeparts.mday-1) / mdays[clock_timeparts.mon] - 0)], clock_timeparts.mday % 10, val);

	segments.ringinner[inner_idx(clock_msecs*3/250)].p = val;
}

void display_text_inner(int8_t pos, const char *txt, uint8_t val, uint8_t clr)
{
	if(clr)
		memset(segments.ringinner, 0, sizeof(segments.ringinner));
	while(*txt)
		set_segment_alpha(&segments.ringinner[inner_idx(pos++)], *txt++, val);
}

void display_text_middle(int8_t pos, const char *txt, uint8_t val, uint8_t clr)
{
	if(clr)
		memset(segments.ringmiddle, 0, sizeof(segments.ringmiddle));
	while(*txt)
		set_segment_alpha(&segments.ringmiddle[middle_idx(pos++)], *txt++, val);
}

void display_text_outer(int8_t pos, const char *txt, uint8_t val, uint8_t clr)
{
	if(clr)
		memset(segments.ringouter, 0, sizeof(segments.ringouter));
	while(*txt)
		set_segment_alpha(&segments.ringouter[outer_idx(pos++)], *txt++, val);
}

#else
static const uint8_t section_remap[4] = {
	2,
	3,
	0,
	1,
};

/* Set ring effect moving outward */
static void show_rings(uint8_t state) __wparam
{
	uint8_t i;
	state <<= 2;
	for(i = 0; i < NDISPOUTER; i++) {
		segments[0].outer[i].a = state + (0 << 2);
		segments[0].outer[i].b = segments[0].outer[i].f = state + (1 << 2);
		segments[0].outer[i].g = state + (2 << 2);
		segments[0].outer[i].c = segments[0].outer[i].e = state + (3 << 2);
		segments[0].outer[i].d = state + (4 << 2);
		segments[0].outer[i].p = 0;
	}
	for(i = 0; i < NDISPMIDDLE; i++) {
		segments[0].middle[i].a = state + (5 << 2);
		segments[0].middle[i].b = segments[0].middle[i].f = state + (6 << 2);
		segments[0].middle[i].g = state + (7 << 2);
		segments[0].middle[i].c = segments[0].middle[i].e = state + (8 << 2);
		segments[0].middle[i].d = state + (9 << 2);
		segments[0].middle[i].p = 0;
	}
	for(i = 0; i < NDISPINNER; i++) {
		segments[0].inner[i].a = state + (10 << 2);
		segments[0].inner[i].b = segments[0].inner[i].f = state + (11 << 2);
		segments[0].inner[i].g = state + (12 << 2);
		segments[0].inner[i].c = segments[0].inner[i].e = state + (13 << 2);
		segments[0].inner[i].d = state + (14 << 2);
		segments[0].inner[i].p = 0;
	}
	memcpy(&segments[1], &segments[0], sizeof(segments[0]));
	memcpy(&segments[2], &segments[0], sizeof(segments[0]));
	memcpy(&segments[3], &segments[0], sizeof(segments[0]));
}

static display_t *display_inner(uint8_t idx)
{
	idx = (idx+cfg.orientation+NSECTIONS*NDISPINNER-1) % (NSECTIONS*NDISPINNER);
	return &segments[section_remap[idx / NDISPINNER]].inner[NDISPINNER - 1 - (idx % NDISPINNER)];
}

static display_t *display_middle(uint8_t idx)
{
	idx = (idx+cfg.orientation*2+NSECTIONS*NDISPMIDDLE-1) % (NSECTIONS*NDISPMIDDLE);
	return &segments[section_remap[idx / NDISPMIDDLE]].middle[NDISPMIDDLE - 1 - (idx % NDISPMIDDLE)];
}

static display_t *display_outer(uint8_t idx)
{
	idx = (idx+cfg.orientation*3+NSECTIONS*NDISPOUTER-1) % (NSECTIONS*NDISPOUTER);
	return &segments[section_remap[idx / NDISPOUTER]].outer[NDISPOUTER - 1 - (idx % NDISPOUTER)];
}

static void show_clock(uint8_t val)
{
	display_clear_mem();
	clock_localtime(0);
	if(clock_timeparts.hour < 10)
		set_segment_digit(display_inner(clock_timeparts.hour), clock_timeparts.hour, val);
	else {
		set_segment_digit(display_inner(clock_timeparts.hour), clock_timeparts.hour/10, val);
		set_segment_digit(display_inner(clock_timeparts.hour+1), clock_timeparts.hour%10, val);
	}
	set_segment_digit(display_middle((clock_timeparts.min << 1)/5), clock_timeparts.min/10, val);
	set_segment_digit(display_middle((clock_timeparts.min << 1)/5+1), clock_timeparts.min%10, val);
	set_segment_digit(display_outer(clock_timeparts.sec*3/5), clock_timeparts.sec/10, val);
	set_segment_digit(display_outer(clock_timeparts.sec*3/5+1), clock_timeparts.sec%10, val);
	display_inner(clock_msecs*3/250)->p = val;
}

static void show_date(uint8_t val)
{
	display_clear_mem();
	clock_localtime(0);
	set_segment_digit(display_outer(clock_timeparts.mday*36/31), (clock_timeparts.mday)/10, val);
	set_segment_digit(display_outer(clock_timeparts.mday*36/31+1), (clock_timeparts.mday)%10, val);
	set_segment_digit(display_middle(clock_timeparts.mon << 1), (clock_timeparts.mon+1)/10, val);
	set_segment_digit(display_middle((clock_timeparts.mon << 1)+1), (clock_timeparts.mon+1)%10, val);
	set_segment_digit(display_inner( 2), (clock_timeparts.year)%10, val);
	set_segment_digit(display_inner( 1), (clock_timeparts.year/10)%10, val);
	set_segment_digit(display_inner( 0), (clock_timeparts.year/100)%10, val);
	set_segment_digit(display_inner(NSECTIONS*NDISPINNER-1), (clock_timeparts.year)/1000, val);
}
#endif

void display_set_mode(uint8_t m) __wparam
{
	switch(m) {
	case DISPMODE_IDLE:
	case DISPMODE_IDLE_NOUPDATE:
	case DISPMODE_CLOCK:
	case DISPMODE_DATE:
	case DISPMODE_RINGING:
	case DISPMODE_REMOTE:
	case DISPMODE_TEXT:
		dispmode = m;
		break;
	}
}

void display_handle_timer(void)
{
	clock_timer_set(TIMER_DISPLAY, DISPLAY_TICKS);
	dispstate++;
	switch(dispmode) {
	case DISPMODE_IDLE:
		display_clear_mem();
		dispmode = DISPMODE_IDLE_NOUPDATE;	/* Clear needs to be done once only */
		break;
	case DISPMODE_REMOTE:
	case DISPMODE_IDLE_NOUPDATE:
		return;
	case DISPMODE_CLOCK:
		show_clock(light < 4 ? 4 : light);	/* Be sure to set at least lowest level */
		break;
	case DISPMODE_DATE:
		show_date(light < 4 ? 4 : light);	/* Be sure to set at least lowest level */
		break;
	case DISPMODE_RINGING:
		show_rings(dispstate);
		break;
	case DISPMODE_TEXT:
		event_display_hook();
		break;
	}
	spi_update();
}

