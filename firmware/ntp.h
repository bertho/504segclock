/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_NTP_H
#define __504SEGCLOCK_NTP_H

#define NTP_FRAC_FROM_MSEC(f)	((uint32_t)(f) * 4294967UL)	/* Fraction ~ msecs * 2^32/1000 */
#define MSEC_FROM_NTP_FRAC(f)	((uint16_t)((uint32_t)(f) / 4294967UL))
#define NTP_SECS_FROM_SECS(s)	((uint32_t)(s) + 2208988800UL)	/* Compensate 70 years as NTP counts from 1900 */
#define SECS_FROM_NTP_SECS(s)	((uint32_t)(s) - 2208988800UL)

typedef struct __ntptimestamp_t {
	uint32_t	secs;
	uint32_t	frac;
} ntptimestamp_t;

#define NTPVERSION_4		4

#define NTPMODE_CLIENT		3
#define NTPMODE_SERVER		4
#define NTPMODE_BCSERVER	5
#define NTPMODE_BCCLIENT	6

#define NTPSTRATUM_INVALID	0
#define NTPSTRATUM_UNSYNC	16

typedef struct __ntphdr_t {
	uint8_t		mode:3;		/* 3 = client */
	uint8_t		version:3;	/* 4 = NTPv4 */
	uint8_t		li:2;		/* Leap indicator */
	uint8_t		stratum;	/* 16 = unsynced */
	int8_t		poll;		/* 4 = 16s interval */
	int8_t		precision;
	uint32_t	root_delay;
	uint32_t	root_dispers;
	uint32_t	ref_id;		/* Our IP */
	ntptimestamp_t	ts_ref;
	ntptimestamp_t	ts_org;
	ntptimestamp_t	ts_rcv;
	ntptimestamp_t	ts_txd;
} ntphdr_t;

typedef struct __ntppkt_t {
	ethhdr_t	eth;
	iphdr_t		ip;
	udphdr_t	udp;
	ntphdr_t	ntp;
} ntppkt_t;

void ntp_init(void);
void ntp_handle_timer(void);
uint8_t ntp_handle_rxpacket(uint16_t pktlen);
void ntp_toggle(void);
void ntp_toggle_mcpeer(void);
void ntp_dump_status(void);

#endif
