/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_EVENT_H
#define __504SEGCLOCK_EVENT_H

enum {
	EV_PRESS_SET,
	EV_PRESS_UP,
	EV_PRESS_DOWN,
	EV_PRESS_SELECT,
	EV_RELEASE_SET,
	EV_RELEASE_UP,
	EV_RELEASE_DOWN,
	EV_RELEASE_SELECT,
};

void event_init(void);
void event_handle_timer(void);
void event_handle(uint8_t ev) __wparam;
void event_display_hook(void);

#endif
