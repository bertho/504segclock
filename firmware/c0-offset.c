/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <pic18fregs.h>
#include <config.h>

extern stack_end;

extern void main(void);
extern void isr(void);

/*
 * Entry function, placed after the bootloader @ vector 0
 * ISR function, placed after the bootloader @ vector 1
 */
#ifdef HAVE_BOOTLOADER
void _entry(void) __naked
{
	__asm
	org	MAIN_ADDR_START
	goto	__startup
	nop
	nop
	__endasm;
}

void _isrh_entry(void) __naked
{
	__asm
	org	MAIN_ADDR_ISRH
	goto	__isr_func
	nop
	nop
	nop
	nop
	nop
	nop
	__endasm;
}

void _isrl_entry(void) __naked
{
	__asm
	org	MAIN_ADDR_ISRL
	goto	__isr_func
	nop
	nop
	__endasm;
}
#else
void _entry(void) __naked __interrupt 0
{
	__asm
	goto	__startup
	__endasm;
}

void _isrh_entry(void) __naked __interrupt 1
{
	__asm
	goto	__isr_func
	__endasm;
}

void _isrl_entry(void) __naked __interrupt 2
{
	__asm
	goto	__isr_func
	__endasm;
}
#endif

/* The interrupt service routine */
void _isr_func(void) __naked
{
	__asm
	movff	_WREG, _POSTDEC1
	movff	_STATUS, _POSTDEC1
	movff	_BSR, _POSTDEC1
	movff	_PRODL, _POSTDEC1
	movff	_PRODH, _POSTDEC1
	movff	_FSR0L, _POSTDEC1
	movff	_FSR0H, _POSTDEC1
	movff	_PCLATH, _POSTDEC1
	movff	_PCLATU, _POSTDEC1
	movff	_FSR2L, _POSTDEC1
	movff	_FSR1L, _FSR2L
	__endasm;
	isr();
	__asm
	movff	_PREINC1, _FSR2L
	movff	_PREINC1, _PCLATU
	movff	_PREINC1, _PCLATH
	movff	_PREINC1, _FSR0H
	movff	_PREINC1, _FSR0L
	movff	_PREINC1, _PRODH
	movff	_PREINC1, _PRODL
	movff	_PREINC1, _BSR
	movff	_PREINC1, _STATUS
	movff	_PREINC1, _WREG
	retfie
	__endasm;
}

/* Reset entry point */
void _startup(void) __naked
{
	/* Setup the stack */
	__asm
	lfsr	1, _stack_end
	lfsr	2, _stack_end
	__endasm;

	main();		/* Execute our main program */

	__asm
lockup:
	; Returning from main will lock up.
	bra     lockup
	__endasm;
}
