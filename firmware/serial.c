/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <pic18fregs.h>
#include <serial.h>

#define BAUDRATE	B115200
#define SENDBUFSIZE	(1 << 5)	/* Must be power of 2 */
#define RECVBUFSIZE	(1 << 3)	/* Must be power of 2 */

#define CONVERT_NL_CRNL	1

static volatile uint8_t sendbuf[SENDBUFSIZE];
static volatile uint8_t recvbuf[RECVBUFSIZE];
static volatile uint8_t sendbufstart;
static volatile uint8_t sendbufcnt;
static volatile uint8_t recvbufstart;
volatile uint8_t recvbufcnt;

void serial_init(void)
{
	/* SYNC=0 BRG16=1 BRGH=1 */
	BAUDCON1 = 0x08;	/* 16bit baudrate generator */
	SPBRGH1  = (BAUDRATE >> 8);
	SPBRG1   = (BAUDRATE & 0xff);
	TRISCbits.TRISC7 = 1;
	TRISCbits.TRISC6 = 0;
	RCSTA1 = 0x90;
	TXSTA1 = 0x24;
	sendbufstart = sendbufcnt = recvbufstart = recvbufcnt = 0;
	PIR1bits.RC1IF = 0;	/* No spurious recv. int */
	PIE1bits.RC1IE = 1;	/* Enable receiver interrupts */
	PIE1bits.TX1IE = 0;	/* Disable transmitter interrupts */
	PIR1bits.TX1IF = 1;	/* Set tranmitter interrupt to fire on enable */
}

void serial_recv_isr(void)
{
	if(RCSTA1 & 0x06) {
		/* An error, clear it and drop the char */
		RCSTA1bits.CREN = 0;
		__asm
		nop
		movf	_RCREG1, w	; To clear any framing error
		nop
		__endasm;
		PIR1bits.RC1IF = 0;
		RCSTA1bits.CREN = 1;
		return;
	}

	if(recvbufcnt >= RECVBUFSIZE) {
		/* Buffer overflow, drop the char */
		__asm
		movf	_RCREG1, w	; Still read the char, even if it gets dropped
		__endasm;
		PIR1bits.RC1IF = 0;
		return;
	}
	recvbuf[(recvbufstart + recvbufcnt) & (RECVBUFSIZE-1)] = RCREG1;
	recvbufcnt++;
	PIR1bits.RC1IF = 0;
}

void serial_send_isr(void)
{
	if(!sendbufcnt) {
		/* We're done, disable ints */
		PIE1bits.TX1IE = 0;
		/* Leave the flag intact so we'll get an interrupt on next enable */
		return;
	}
	TXREG1 = sendbuf[sendbufstart];
	sendbufstart++;
	sendbufstart &= (SENDBUFSIZE-1);
	sendbufcnt--;
	PIR1bits.TX1IF = 0;	/* Reset tranmitter interrupt for next round */
}

uint8_t serial_getc(void)
{
	uint8_t tmp;
	while(!recvbufcnt)
		;
	PIE1bits.RC1IE = 0;
	tmp = recvbuf[recvbufstart];
	recvbufstart++;
	recvbufstart &= (RECVBUFSIZE-1);
	recvbufcnt--;
	PIE1bits.RC1IE = 1;
	return tmp;
}

void serial_putc(uint8_t ch) __wparam
{
#ifdef CONVERT_NL_CRNL
	if(ch == '\n')
		serial_putc('\r');
#endif
	while(sendbufcnt >= SENDBUFSIZE)
		;	/* Wait for space */
	PIE1bits.TX1IE = 0;
	sendbuf[(sendbufstart + sendbufcnt) & (SENDBUFSIZE-1)] = ch;
	sendbufcnt++;
	PIE1bits.TX1IE = 1;
}

void serial_puts(const char *str)
{
	while(*str)
		serial_putc(*str++);
}

void serial_put(const char *str, uint8_t len)
{
	while(len--)
		serial_putc(*str++);
}

void serial_flush_out(void)
{
	while(sendbufcnt)
		;
}

#define put_nibble(n)	serial_putc((n) <= 9 ? (n)+'0' : (n)+ '0'+7)
void serial_puthex(uint8_t h) __wparam
{
	uint8_t c = h & 0x0f;
	h >>= 4;
	put_nibble(h);
	put_nibble(c);
}

void serial_puthex_u16(uint16_t h)
{
	serial_puthex(h >> 8);
	serial_puthex(h);
}

void serial_puthex_u32(uint32_t h)
{
	serial_puthex_u16(h >> 16);
	serial_puthex_u16(h);
}


void serial_putdec(uint8_t d)
{
	if(d >= 100) {
		serial_putc(d / 100 + '0');
		d %= 100;
		if(d < 10)
			serial_putc('0');
	}
	if(d >= 10) {
		serial_putc(d / 10 + '0');
		d %= 10;
	}
	serial_putc(d + '0');
}

void serial_putdec_u16(uint16_t v)
{
	char buf[6];
	uint8_t c = 0;
	do {
		buf[c++] = (v % 10) + '0';
		v /= 10;
	} while (v);
	do {
		serial_putc(buf[--c]);
	} while(c);
}

void serial_putdec_s16(int16_t v)
{
	if(v < 0) {
		serial_putc('-');
		serial_putdec_u16(-v);
	} else
		serial_putdec_u16(v);
}

void serial_putdec_u32(uint32_t v)
{
	char buf[11];
	uint8_t c = 0;
	do {
		buf[c++] = (v % 10) + '0';
		v /= 10;
	} while (v);
	do {
		serial_putc(buf[--c]);
	} while(c);
}

