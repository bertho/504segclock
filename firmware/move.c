/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <pic18fregs.h>
#include <move.h>
#include <clock.h>
#include <display.h>
#include <io.h>
#include <config.h>
#include <ethernet.h>
#include <ip.h>
#include <udp.h>
#include <netctrl.h>

#define MOVE_TICKS		1000	/* Second interval */
#define DATEMODE_COUNT		2	/* 2 second continuous movement --> data display */
#define DATEMODE_HOLD		4	/* Show date 4 seconds */

volatile uint8_t bcmove;
static volatile uint16_t lastmove;
static int8_t datemodecnt;

void move_init(void)
{
	datemodecnt = -(DATEMODE_HOLD + 1);
	bcmove = 0;

	CMCON = 0x23;			/* Two comparators with outputs */

	PORTF = 0x00;
	TRISF = 0x79;			/* Analogue inputs, except C[12]OUT and RF7 */

	PIR2bits.CMIF = 0;		/* Enable interrupts */
	PIE2bits.CMIE = 1;

	clock_timer_set(TIMER_MOVE, MOVE_TICKS);
}

void move_isr(void)
{
	/* End the mismatch */
	__asm
	movf	_CMCON, w
	__endasm;
	PIR2bits.CMIF = 0;	/* Clear interrupt flag */

	lastmove = cfg.movetimeout;

	/* Enable the clock from idle on movement */
	if(display_mode_is_idle())
		display_set_mode(DISPMODE_CLOCK);

	/* If this is a first detection, inform main */
	if(!io_led_move_state())
		bcmove = 1;	/* Message send in main() */

	io_led_move(1);		/* Set the move-LED */
}

void move_handle_timer(void)
{
	clock_timer_set(TIMER_MOVE, MOVE_TICKS);

	/* If either is active, we have an active detection */
	if(CMCONbits.C1OUT || CMCONbits.C2OUT) {
		io_led_move(1);
		cli(); lastmove = cfg.movetimeout; sti();
	} else {
		if(io_led_move_state())
			netctrl_broadcast_move(0);	/* Send message on deactivation */
		io_led_move(0);
	}

	/* Reset the count if no movement and not currently showing date/ringing */
	if(!display_mode_is_nonclock() && !io_led_move_state())
		datemodecnt = DATEMODE_COUNT;
	else if((display_mode_is_clock() || display_mode_is_nonclock()) && datemodecnt >= -(DATEMODE_HOLD))
		datemodecnt--;

	if(display_mode_is_clock() && !datemodecnt)	/* Progress to date */
		display_set_mode(DISPMODE_DATE);
	else if(display_mode_is_nonclock() && datemodecnt <= -(DATEMODE_HOLD)) {
		if(io_led_move_state())
			display_set_mode(DISPMODE_RINGING);
		else
			display_set_mode(DISPMODE_CLOCK);
	}

	/* Check if we timed out on idle counter */
	if(!display_mode_is_remote()) {
		cli(); if(lastmove) lastmove--; sti();
		if(cfg.movetimeout && !lastmove) {
			display_set_mode(DISPMODE_IDLE);
		}
	}
}
