/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_NETCTRL_H
#define __504SEGCLOCK_NETCTRL_H

#ifdef __GNUC__
#define PACKED_FIELDS	__attribute__((packed))
#else
#define PACKED_FIELDS
#endif

enum {
	NETCMD_SETMODE,
	NETCMD_SETDATA,
	NETCMD_SETTIMEOUT,
	NETCMD_ALARM,
	NETCMD_LIGHT,
	NETCMD_MOVE,
	NETCMD_GETCFG,
	NETCMD_SETCFG,
};

typedef struct __netctrl_t {
	uint16_t	cmd;
	union {
		uint8_t		mode;			/* Operational mode */
		uint32_t	timeout;
		section_t	sections[NSECTIONS];	/* Display data */
		struct {
			uint32_t	timestamp;		/* Sender timestamp */
			union {
				uint8_t		light;		/* Ambiant light value */
				uint8_t		move;		/* !=0 --> Move sensor triggered */
			} PACKED_FIELDS;
		} value PACKED_FIELDS;
		struct {
			sysconfig_t	cfg;			/* Device config */
			uint8_t		serial[8];		/* Device ID */
		} config;
	} PACKED_FIELDS;
} netctrl_t;

#ifndef __GNUC__
typedef struct __netctrlpkt_t {
	ethhdr_t	eth;
	iphdr_t		ip;
	udphdr_t	udp;
	netctrl_t	nc;
} netctrlpkt_t;

uint8_t netctrl_handle_rxpacket(uint16_t pktlen);
void netctrl_handle_timer(void);
void netctrl_broadcast_light(uint8_t light);
void netctrl_broadcast_move(uint8_t mv);
#endif

#endif
