/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_DISPLAY_H
#define __504SEGCLOCK_DISPLAY_H

#define ABS_SEGMENTS	1

#define NSECTIONS	4		/* four display sections */
#define NDISPINNER	3		/* Inner */
#define NDISPMIDDLE	6		/* Middle */
#define NDISPOUTER	9		/* Outer */
#define NDISPLAYS	(NDISPINNER+NDISPMIDDLE+NDISPOUTER)
#define NSEGMENTS	(NDISPLAYS*8)	/* 18 displays with 7+1 segments */

#ifdef LINEAR_DISPLAY
#define NOUTER		(NSECTIONS*NDISPOUTER)
#define NMIDDLE		(NSECTIONS*NDISPMIDDLE)
#define NINNER		(NSECTIONS*NDISPINNER)
#define NALL		(NINNER + NMIDDLE + NOUTER)
#define inner_idx(x)	(((x) + NINNER + cfg.orientation) % NINNER)
#define middle_idx(x)	(((x) + NMIDDLE + 2*cfg.orientation) % NMIDDLE)
#define outer_idx(x)	(((x) + NOUTER + 3*cfg.orientation) % NOUTER)
#endif

#define DISPLAY_TICKS	100

#define	DISPF_IDLE	0x80
#define DISPF_NONCLK	0x40
#define DISPF_REMOTE	0x20

enum {
	DISPMODE_IDLE		= 0x00 | DISPF_IDLE,
	DISPMODE_IDLE_NOUPDATE	= 0x01 | DISPF_IDLE,
	DISPMODE_CLOCK		= 0x02,
	DISPMODE_DATE		= 0x03 | DISPF_NONCLK,
	DISPMODE_RINGING	= 0x04 | DISPF_NONCLK,
	DISPMODE_REMOTE		= 0x05 | DISPF_REMOTE,
	DISPMODE_TEXT		= 0x06 | DISPF_REMOTE,
};

#define display_mode_is_idle()		(dispmode & DISPF_IDLE)
#define display_mode_is_nonclock()	(dispmode & DISPF_NONCLK)
#define display_mode_is_clock()		(dispmode == DISPMODE_CLOCK)
#define display_mode_is_remote()	(dispmode & DISPF_REMOTE)
#define display_get_mode()		(dispmode)


typedef union __display_t {
	struct {
		uint8_t	a;
		uint8_t	b;
		uint8_t	c;
		uint8_t	d;
		uint8_t	e;
		uint8_t	f;
		uint8_t	g;
		uint8_t	p;
	};
	uint8_t	bytes[8];
} display_t;

typedef union __lineardisplays_t {		/* Linear ring layout; easier to handle */
	struct {
		display_t	ringouter[NOUTER];
		display_t	ringmiddle[NMIDDLE];
		display_t	ringinner[NINNER];
	};
	display_t	display[NALL];
	uint8_t		bytes[NALL*8];
} lineardisplay_t;

typedef union __section_t {
	struct {
		display_t	inner[NDISPINNER];
		display_t	middle[NDISPMIDDLE];
		display_t	outer[NDISPOUTER];
	};
	display_t	display[NDISPLAYS];
	uint8_t		bytes[NSEGMENTS];
} section_t;

#ifndef __GNUC__

#ifdef LINEAR_DISPLAY
#ifdef ABS_SEGMENTS
#define __SEGMENTS_ADDRESS	0x0700
extern __data lineardisplay_t __at __SEGMENTS_ADDRESS segments;
#else
extern lineardisplay_t segments;
#endif
#else
#ifdef ABS_SEGMENTS
#define __SEGMENTS_ADDRESS	0x0700
extern __data section_t __at __SEGMENTS_ADDRESS segments[NSECTIONS];
#else
extern section_t segments[NSECTIONS];
#endif
#endif

extern uint8_t dispmode;

void display_init(void);
void display_set_mode(uint8_t m) __wparam;
void display_clear_mem(void);
void display_handle_timer(void);
void display_text_inner(int8_t pos, const char *txt, uint8_t val, uint8_t clr);
void display_text_middle(int8_t pos, const char *txt, uint8_t val, uint8_t clr);
void display_text_outer(int8_t pos, const char *txt, uint8_t val, uint8_t clr);
#endif

#endif
