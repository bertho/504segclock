/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <pic18fregs.h>
#include <adc.h>
#include <clock.h>
#include <display.h>
#include <ethernet.h>
#include <ip.h>
#include <udp.h>
#include <config.h>
#include <netctrl.h>

#define ADC_TICKS	250	/* Update the light sensor every quarter second */
#define ADC_BC_TICKS	1000	/* Broadcast light value every 1 seconds */

volatile uint8_t light;

void adc_init(void)
{
	TRISAbits.TRISA2 = 1;	/* Light is input */

	ADCON1 = 0x03;		/* Enable AN0..AN2 on Vss/Vdd reference */
	ADCON0 = 0x08;		/* Channel 2 */
	ADCON2 = 0x13;		/* Left adjust, 4Tad, 1Mc RC/osc */
	ADCON0bits.ADON = 1;	/* Enable ADC */
	PIR1bits.ADIF = 0;
	PIE1bits.ADIE = 1;

	ADCON0bits.ADCAL = 1;	/* Perform a calibration cycle */
	ADCON0bits.GO = 1;

	clock_timer_set(TIMER_ADC, ADC_TICKS);
	clock_timer_set(TIMER_ADC_BC, ADC_BC_TICKS);
}

void adc_handle_timer(void)
{
	clock_timer_set(TIMER_ADC, ADC_TICKS);
	ADCON0bits.ADCAL = 0;
	ADCON0bits.GO = 1;	/* Start conversion */
}

void adc_handle_bctimer(void)
{
	clock_timer_set(TIMER_ADC_BC, ADC_BC_TICKS);
	netctrl_broadcast_light(light);
}

void adc_isr(void)
{
	static uint16_t newlight;
	static uint8_t llight;
	newlight *= 15;
	newlight >>= 4;		/* newlight * 15 / 16 */
	newlight += ADRESH;	/* plus new AD value (which is left adjusted) */
	PIR1bits.ADIF = 0;
	/* Implement hysteresis to avoid jumping intensity */
	llight = newlight >> 4;
	if(llight > light || llight < light-1 || (llight < light && !(newlight & 0x000f)))
		light = newlight >> 4;
}
