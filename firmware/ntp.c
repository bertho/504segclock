/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>
#include <ethernet.h>
#include <network.h>
#include <ip.h>
#include <udp.h>
#include <ntp.h>
#include <clock.h>
#include <config.h>
#include <serial.h>
#include <rtcc.h>

//#define DEBUG_NTP_CONVERGENCE	1

#define NTP_TICKS		1000
#define NTP_MAXOFFSET		900	/* 900 seconds max offset for slow adjust */
#define NTP_MINLOGINTERVAL	4	/* 2^4 seconds min (from RFC) */
#define NTP_MAXLOGINTERVAL	8	/* 2^8 seconds max (RFC says 36 hours ~2^17) */
#define NTP_HOLDOFFVAL		((NTP_MAXLOGINTERVAL - loginterval) * 2)

enum {
	NTPSTATE_OFF,
	NTPSTATE_INIT,
	NTPSTATE_PKTSENT,
	NTPSTATE_PKTRCVD,
};

typedef struct __timetag_t {
	int32_t		secs;
	uint16_t	msec;
} timetag_t;

static uint8_t ntpstate;
static uint8_t loginterval;	/* Logarithmic interval for 2^x seconds */
static uint16_t curinterval;	/* Current interval seconds counter */
static timetag_t timetags[4];	/* T1..T4 from NTP */
static timetag_t theta;		/* Offset to server */
static timetag_t delta;		/* Round-trip delay to server */
static uint8_t ntpaddr[4];	/* Server we are bound to */
static uint8_t ntppeer[4];	/* Server we last hear from */
static int32_t drift;		/* Drift in 1/256 milliseconds */
static int16_t drift_rest;	/* Accumulated rest of correction per second */
static uint8_t holdoff;		/* Hold off the interval adjust when stabelizing */
static uint8_t ntpinsync;	/* Set if we are synchronized */

static void print_timetag(timetag_t *t);

/*
 * Timetag representation:
 *	+10.123 seconds
 *		secs: 10
 *		msec: 123
 *	-10.123 seconds
 *		secs: -11
 *		msec: 877
 *	-0.001 seconds
 *		secs: -1
 *		msec: 999
 */
static void tsdiff(timetag_t *dst, timetag_t *s1, timetag_t *s2)
{
	dst->secs = s1->secs - s2->secs;
	if(s1->msec < s2->msec) {
		dst->msec = s1->msec + 1000 - s2->msec;
		dst->secs--;
	} else
		dst->msec = s1->msec - s2->msec;
}

static void tssum(timetag_t *dst, timetag_t *s1, timetag_t *s2)
{
	dst->secs = s1->secs + s2->secs;
	dst->msec = s1->msec + s2->msec;
	if(dst->msec >= 1000) {
		dst->msec -= 1000;
		dst->secs++;
	}
}

static void tsabs(timetag_t *dst, timetag_t *src)
{
	if(src->secs < 0) {
		dst->msec = 1000 - src->msec;
		dst->secs = -src->secs - 1;
	} else {
		dst->secs = src->secs;
		dst->msec = src->msec;
	}
}

static void tsdiv2(timetag_t *dst)
{
	if(dst->secs < 0) {
		/* Make positive */
		dst->msec = 1000 - dst->msec;
		dst->secs = -dst->secs - 1;
		/* Divide by 2 */
		if(dst->secs & 1)
			dst->msec += 1000;
		dst->secs /= 2;		/* Signed! */
		dst->msec >>= 1;
		if(!dst->msec && !dst->secs)	/* Leave 0.0 to be 0.0 */
			return;
		/* Make negative */
		dst->msec = 1000 - dst->msec;
		dst->secs = -dst->secs - 1;
	} else {
		if(dst->secs & 1)
			dst->msec += 1000;
		dst->secs /= 2;		/* Signed! */
		dst->msec >>= 1;
	}
}

static void set_timer(void)
{
	curinterval = 1 << loginterval;
	clock_timer_set(TIMER_NTP, NTP_TICKS);
}

static void reset_vars(void)
{
	loginterval = NTP_MINLOGINTERVAL;
	IP_CP(ntpaddr, cfg.ntpaddr);
	memset(ntppeer, 0, sizeof(ntppeer));
	memset(timetags, 0, sizeof(timetags));
	memset(&theta, 0, sizeof(theta));
	memset(&delta, 0, sizeof(delta));
	holdoff = NTP_HOLDOFFVAL;
	ntpinsync = 0;
	set_timer();
}

void ntp_init(void)
{
	if(!cfg.ntp_on)
		ntpstate = NTPSTATE_OFF;
	else
		ntpstate = NTPSTATE_INIT;
	reset_vars();
	drift = cfg.drift;
	drift_rest = 0;
}

#define _npkt	(*(ntppkt_t *)&packet)

static void send_ntppkt(void)
{
	/* We know about UC, MC and BC */
	if(!(IP_IS_VALID(ntpaddr) || IP_IS_MULTICAST(ntpaddr) || IP_IS_BROADCAST(ntpaddr)))
		return;
	memset(&_npkt, 0, sizeof(_npkt));
	MAC_CP(_npkt.eth.sa, cfg.macaddr);
	IP_CP(_npkt.ip.saddr, cfg.ipaddr);
	IP_CP(_npkt.ip.daddr, ntpaddr);
	if(!arp_resolve(ntpaddr, _npkt.eth.da)) {
		arp_whohas(ntpaddr);
		return;
	}
	_npkt.eth.lenw = htons(ETHTYPE_IP);
	_npkt.ip.version = 4;
	_npkt.ip.hdrlen = 5;
	_npkt.ip.ttl = IP_IS_VALID(ntpaddr) ? IP_TTL_DEFAULT : 1;
	_npkt.ip.proto = IPPROTO_UDP;
	//_npkt.ip.checksum = 0;
	_npkt.udp.sport = htons(UDP_PORT_NTP);
	_npkt.udp.dport = htons(UDP_PORT_NTP);
	//_npkt.udp.checksum = 0;
	//_npkt.ntp.li = 0;
	_npkt.ntp.version = NTPVERSION_4;	/* NTPv4 */
	_npkt.ntp.mode = NTPMODE_CLIENT;
	_npkt.ntp.stratum = NTPSTRATUM_UNSYNC;	/* Unconfigured */
	_npkt.ntp.poll = loginterval;			/* Unconfigured */
	IP_CP((uint8_t *)&_npkt.ntp.ref_id, cfg.ipaddr);
	/* Get the current time for T1 */
	cli();
	timetags[0].secs = clock_secs;
	timetags[0].msec = clock_msecs;
	sti();
	_npkt.ntp.ts_org.secs = htonl(NTP_SECS_FROM_SECS(timetags[0].secs));
	_npkt.ntp.ts_org.frac = htonl(NTP_FRAC_FROM_MSEC(timetags[0].msec));

	_npkt.ip.len = htons(sizeof(_npkt.ip) + sizeof(_npkt.udp) + sizeof(_npkt.ntp));
	_npkt.ip.checksum = ip_checksum(&_npkt.ip, sizeof(_npkt.ip));
	_npkt.udp.length = htons(sizeof(_npkt.udp) + sizeof(_npkt.ntp));
	_npkt.udp.checksum = udp_checksum(&_npkt.ip, sizeof(_npkt.udp) + sizeof(_npkt.ntp));
	ethernet_txpacket(sizeof(_npkt));
	/* Note: we actually do not care if the packet got send because the
	 * timer will reset the state and try again at an appropriate time
	 */
}

void ntp_handle_timer(void)
{
	if(drift) {
		cli();
		clock_msecs += drift / 256;
		if(clock_msecs >= 1000) {
			if(drift > 0) {
				clock_msecs -= 1000;
				clock_secs++;
			} else {
				clock_msecs += 1000;
				clock_secs--;
			}
		}
		sti();
		drift_rest += drift % 256;
		if(drift_rest >= 256) {
			cli();
			clock_msecs++;
			if(clock_msecs >= 1000) {
				clock_msecs -= 1000;
				clock_secs++;
			}
			sti();
			drift_rest -= 256;
		} else if(drift_rest <= -256) {
			cli();
			clock_msecs--;
			if(clock_msecs >= 1000) {
				clock_msecs += 1000;
				clock_secs--;
			}
			sti();
			drift_rest += 256;
		}
	}
	if(--curinterval) {
		clock_timer_set(TIMER_NTP, NTP_TICKS);
		return;
	}
	set_timer();

	switch(ntpstate) {
	case NTPSTATE_OFF:
		ntpinsync = 0;
		break;
	case NTPSTATE_INIT:
		ntpinsync = 0;
	case NTPSTATE_PKTRCVD:
		/* Only do sometething if we have a valid IP */
		if(!IP_IS_VALID(cfg.ipaddr))
			break;
		send_ntppkt();		/* Send the next packet */
		ntpstate = NTPSTATE_PKTSENT;
		break;

	case NTPSTATE_PKTSENT:
		/* We get here when we get a timeout */
		if(IP_IS_VALID(ntpaddr)) {
			/* Revert to set up IP on timeout (may be MC or BC) */
			IP_CP(ntpaddr, cfg.ntpaddr);
			loginterval = NTP_MINLOGINTERVAL;
		} else if(loginterval > NTP_MINLOGINTERVAL) {
			/* If UC, reduce the interval */
			loginterval--;
		}
		set_timer();
		ntpstate = NTPSTATE_INIT;
		ntpinsync = 0;
		break;

	default:
		ntpstate = NTPSTATE_INIT;
		ntpinsync = 0;
		break;
	}
}

uint8_t ntp_handle_rxpacket(uint16_t pktlen)
{
	ntphdr_t *ntp;
	timetag_t adj;

	/* Get the T4 timestamp */
	cli();
	timetags[3].secs = eth_secs;
	timetags[3].msec = eth_msecs;
	sti();

	if(ntpstate != NTPSTATE_PKTSENT)
		return 0;

	/* Packet can be larger due to option fields, but not smaller */
	if(pktlen < sizeof(ntppkt_t))
		return 0;

	/* We send with both source and destination port set to the NTP port */
	if(_npkt.udp.sport != htons(UDP_PORT_NTP))
		return 0;

	ntp = (ntphdr_t *)(((uint8_t *)&packet) + sizeof(ethhdr_t) + iphdr_size(&_npkt.ip) + sizeof(udphdr_t));

	if(ntp->version != NTPVERSION_4)
		return 0;

	if(ntp->mode != NTPMODE_SERVER)
		return 0;

	/* Don't accept unsynchronized or invalid stratum servers */
	//if(!ntp->stratum ||  ntp->stratum >= NTPSTRATUM_UNSYNC)
	if(ntp->stratum >= NTPSTRATUM_UNSYNC)
		return 0;

	/* If we are sending MC/BC and the answer is from a UC server, use this server */
	if(!cfg.ntp_mcpeer && !IP_IS_VALID(ntpaddr) && IP_IS_VALID(_npkt.ip.saddr))
		IP_CP(ntpaddr, _npkt.ip.saddr);
	IP_CP(ntppeer, _npkt.ip.saddr);

	timetags[1].secs = SECS_FROM_NTP_SECS(ntohl(ntp->ts_rcv.secs));	/* T2 */
	timetags[1].msec = MSEC_FROM_NTP_FRAC(ntohl(ntp->ts_rcv.frac));
	timetags[2].secs = SECS_FROM_NTP_SECS(ntohl(ntp->ts_txd.secs));	/* T3 */
	timetags[2].msec = MSEC_FROM_NTP_FRAC(ntohl(ntp->ts_txd.frac));

#if 0
	serial_puts(" \n");
	print_timetag(&timetags[0]);
	serial_putc(',');
	print_timetag(&timetags[1]);
	serial_putc(',');
	print_timetag(&timetags[2]);
	serial_putc(',');
	print_timetag(&timetags[3]);
	serial_putc(' ');
#endif

	/*
	 * Offset between B and A:
	 * theta = T(B) - T(A) = 1/2 * [(T2-T1) + (T3-T4)]
	 * Round-trip delay:
	 * delta = T(ABA) = (T4-T1) - (T3-T2)
	 */
	tsdiff(&theta, timetags+1, timetags+0);
	tssum(&theta, &theta, timetags+2);
	tsdiff(&theta, &theta, timetags+3);
	tsdiv2(&theta);
	tsdiff(&delta, timetags+3, timetags+0);
	tssum(&delta, &delta, timetags+1);
	tsdiff(&delta, &delta, timetags+2);
	tsabs(&delta, &delta);		/* Ensure positive RTD to correct for rounding errors */

	tsabs(&adj, &theta);
	if(adj.secs >= NTP_MAXOFFSET) {
		/* Outside the boundary, copy the NTP server's time */
		cli();
		clock_secs = timetags[2].secs;
		clock_msecs = timetags[2].msec;
		sti();
#ifdef DEBUG_NTP_CONVERGENCE
		serial_puts(" \nCopied NTP time from server\n");
		print_timetag(&delta);
		serial_putc(',');
		print_timetag(&theta);
		serial_putc(',');
		print_timetag(&adj);
		serial_putc(',');
		serial_putdec_s16(drift);
		serial_putc(',');
		serial_putdec(loginterval);
		serial_putc(',');
		serial_putdec(holdoff);
		serial_putc(' ');
#endif
	} else {
		/*
		 * Adjust the clock.
		 * - 1/4 part of the offset is corrected.
		 */
		adj.secs = theta.secs;
		adj.msec = theta.msec;
		tsdiv2(&adj);
		tsdiv2(&adj);
#ifdef DEBUG_NTP_CONVERGENCE
		serial_puts(" \n");
		print_timetag(&delta);
		serial_putc(',');
		print_timetag(&theta);
		serial_putc(',');
		print_timetag(&adj);
		serial_putc(',');
		serial_putdec_s16(drift);
		serial_putc(',');
		serial_putdec(loginterval);
		serial_putc(',');
		serial_putdec(holdoff);
		serial_putc(' ');
#endif
		if(adj.secs < 0) {
			/* Adjust negative offset */
			tsabs(&adj, &adj);
			cli();
			clock_secs -= adj.secs;
			clock_msecs -= adj.msec;
			if(clock_msecs >= 1000) {
				clock_msecs += 1000;
				clock_secs--;
			}
			sti();
			drift--;
			holdoff = NTP_HOLDOFFVAL;
		} else if(adj.secs > 0 || adj.msec) {
			/* Adjust positive offset */
			cli();
			clock_secs += adj.secs;
			clock_msecs += adj.msec;
			if(clock_msecs >= 1000) {
				clock_msecs -= 1000;
				clock_secs++;
			}
			sti();
			drift++;
			holdoff = NTP_HOLDOFFVAL;
		}

		/* the 'adj' value is absolute here (used tsabs() if negative above) */
		if(0 != (adj.msec >> (loginterval-1))) {
			/* We had to adjust the time more than the interval allows */
			/* If the adjust is large, reduce the interval */
			if(loginterval > NTP_MINLOGINTERVAL)
				loginterval--;
			ntpinsync = 0;
		} else if(holdoff) {
			/* Hold off the interval adjust for the drift to stabelize */
			holdoff--;
		} else if(!adj.msec) {
			/* If the adjust is zero, increase the interval */
			if(loginterval < NTP_MAXLOGINTERVAL)
				loginterval++;
			holdoff = NTP_HOLDOFFVAL;
			if(!ntpinsync)
				cfg.drift = drift;	/* Save the current drift for quicker convergence */
			ntpinsync = 1;
			if(cfg.ntp_to_rtcc)
				rtcc_write_now();	/* Update the internal clock so it may be up-to-date */
		}
	}
	ntpstate = NTPSTATE_PKTRCVD;
	return 1;
}

void ntp_toggle(void)
{
	if(cfg.ntp_on) {
		ntpstate = NTPSTATE_OFF;
		cfg.ntp_on = 0;
	} else {
		ntpstate = NTPSTATE_INIT;
		reset_vars();
		cfg.ntp_on = 1;
	}
}

void ntp_toggle_mcpeer(void)
{
	if(cfg.ntp_mcpeer) {
		ntpstate = NTPSTATE_INIT;
		reset_vars();
		cfg.ntp_mcpeer = 0;
	} else {
		cfg.ntp_mcpeer = 1;
	}
}

static void print_timetag(timetag_t *t)
{
	if(t->secs < 0) {
		uint16_t m = 1000 - t->msec;
		serial_putc('-');
		serial_putdec_u32(-t->secs - 1);
		serial_putc('.');
		if(m < 10)
			serial_putc('0');
		if(m < 100)
			serial_putc('0');
		serial_putdec_u16(m);
	} else {
		serial_putdec_u32(t->secs);
		serial_putc('.');
		if(t->msec < 10)
			serial_putc('0');
		if(t->msec < 100)
			serial_putc('0');
		serial_putdec_u16(t->msec);
	}
}

void ntp_dump_status(void)
{
	if(!cfg.ntp_on) {
		serial_puts("NTP is currently off\n");
		return;
	}
	serial_puts("NTP status:\nConfig NTP peer : ");
	serial_putdec(cfg.ntpaddr[0]);	serial_putc('.');
	serial_putdec(cfg.ntpaddr[1]);	serial_putc('.');
	serial_putdec(cfg.ntpaddr[2]);	serial_putc('.');
	serial_putdec(cfg.ntpaddr[3]);
	serial_puts("\nCurrent NTP peer: ");
	serial_putdec(ntppeer[0]);	serial_putc('.');
	serial_putdec(ntppeer[1]);	serial_putc('.');
	serial_putdec(ntppeer[2]);	serial_putc('.');
	serial_putdec(ntppeer[3]);
	serial_puts("\nUpdate interval : 2^");
	serial_putdec(loginterval);
	serial_puts("\nNext update     : ");
	serial_putdec_u16(curinterval);
	serial_puts("\nPeer offset     : ");
	print_timetag(&theta);
	serial_puts("\nPeer round-trip : ");
	print_timetag(&delta);
	serial_puts("\nDrift           : ");
	serial_putdec_s16(drift);
	if(ntpinsync)
		serial_puts("\nNTP sync status : synchronized");
	else
		serial_puts("\nNTP sync status : not in sync");
	serial_putc('\n');
}
