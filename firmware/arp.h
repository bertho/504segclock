/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_ARP_H
#define __504SEGCLOCK_ARP_H

#define MAXARPENTRIES	4
#define ARPTIMER_TICKS	1000	/* 1000ms between timer ticks */
#define ARPTIMEOUT	1800	/* 30 minutes */

#define ARP_REQUEST	1
#define ARP_REPLY	2

#define ARP_HTYPE_ETH	1
#define ARP_HLEN_ETH	6
#define ARP_PLEN_IP	4

/* Eth type 0x0806 */
typedef struct __arphdr_t {
	uint16_t	htype;		/* Hardware type; Eth == 1 */
	uint16_t	ptype;		/* Protocol type; IP == 0x800 */
	uint8_t		hlen;		/* Hardware address length; Eth == 6 */
	uint8_t		plen;		/* Protocol address length; IP == 4 */
	uint16_t	operation;	/* 1 == request, 2 == reply */
	union {
		struct {
			uint8_t	smac[6];	/* Source MAC */
			uint8_t	sip[4];		/* Source IP */
			uint8_t	dmac[6];	/* Destination MAC */
			uint8_t	dip[4];		/* Destination IP */
		};			/* IP arp structure for eth */
	};
} arphdr_t;

typedef struct __arptable_t {
	uint16_t	timeout;
	uint8_t		ip[4];
	uint8_t		mac[6];
} arptable_t;

void arp_init(void);
void arp_handle_timer(void);
void arp_handle_rxpacket(uint16_t pktlen);
void arp_add(uint8_t *mac, uint8_t *ip);
uint8_t arp_resolve(uint8_t *ip, uint8_t *mac);
void arp_whohas(uint8_t *ip);
void arp_dump_arptable(void);

#endif
