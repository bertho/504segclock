/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <ethernet.h>
#include <udp.h>
#include <dhcp.h>
#include <serial.h>
#include <display.h>
#include <ntp.h>
#include <config.h>
#include <netctrl.h>

//#define DEBUG_UDP	1

#define _iph	packet.ethpkt.iph

uint8_t udp_handle_rxpacket(uint16_t pktlen)
{
	uint16_t udplen;
	uint16_t sum;
	udphdr_t *udp = (udphdr_t *)(uint8_t)&packet;
	(void)pktlen;

	udp = (udphdr_t *)(((uint8_t *)&_iph) + iphdr_size(&_iph));
	udplen = ntohs(_iph.len) - iphdr_size(&_iph);

	if(udp->checksum && 0 != (sum = udp_checksum(&_iph, udplen))) {
		/* Wrong checksum */
#ifdef DEBUG_UDP
		serial_putc('-');
		serial_puthex_u16(sum);
		serial_putc('-');
		serial_puthex_u16(udp->checksum);
		serial_putc('-');
#endif
		return 0;
	}
#ifdef DEBUG_UDP
	serial_puts("udp: accept checksum\n");
#endif
	if(udplen != ntohs(udp->length))
		return 0;

#ifdef DEBUG_UDP
	serial_puts("udp: accept udp length\n");
#endif
	if(udp->dport == htons(UDP_PORT_BOOTPC))
		return dhcp_handle_rxpacket(pktlen);
	if(udp->dport == htons(UDP_PORT_NTP))
		return ntp_handle_rxpacket(pktlen);
	if(udp->dport == htons(cfg.port))
		return netctrl_handle_rxpacket(pktlen);
	return 0;
}

/* The 1's complement sum in UDP checksums */
uint16_t udp_checksum(iphdr_t *ipp, uint16_t len)
{
	uint32_t sum;
	uint16_t *p;
	/* calc chksum of pseudo-header */
	sum =  ((uint16_t *)(ipp->saddr))[0];
	sum += ((uint16_t *)(ipp->saddr))[1];
	sum += ((uint16_t *)(ipp->daddr))[0];
	sum += ((uint16_t *)(ipp->daddr))[1];
	sum += htons(len + IPPROTO_UDP);
	p = (uint16_t *)((uint8_t *)ipp + iphdr_size(ipp));
	for(; len > 1; len -= 2)
		sum += *p++;
	if(len)
		sum += *(uint8_t *)p;
	/* Fold the sum */
	while(sum & 0xffff0000)
		sum = (sum & 0x0000ffff) + (sum >> 16);
	return ~sum;
}

