/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <event.h>
#include <clock.h>
#include <display.h>
#include <config.h>
#include <network.h>
#include <ethernet.h>
#include <ip.h>
#include <udp.h>
#include <ntp.h>

#define CONFIG_TICKS	10000	/* 10s timeout for commands */
#define CONFIG2_TICKS	30000	/* 30s timeout for selections */

static uint16_t ticks;

/*
 * Configuration:
 *	time
 *		second
 *		minute
 *		hour
 *	date
 *		day
 *		month
 *		year
 *	timezone
 *		timezone
 *	feature
 *		blanking
 *		rotation
 *	ntp
 *		ntp on/off
 *		server
 *		use peer on/off
 *	ip
 *		dhcp on/off
 *		ip address
 *			octet 1
 *			octet 2
 *			octet 3
 *			octet 4
 *		netmask
 *			octet 1
 *			octet 2
 *			octet 3
 *			octet 4
 *		gateway
 *			octet 1
 *			octet 2
 *			octet 3
 *			octet 4
 *		port
 *	save
 *		yes/no
 *	exit
 */
enum {
	EVSTATE_NONE			= 0x00,

	EVSTATE_CONFIG_TIME		= 0x10,
	EVSTATE_CONFIG_TIME_SET_SEC,
	EVSTATE_CONFIG_TIME_SET_MIN,
	EVSTATE_CONFIG_TIME_SET_HOUR,

	EVSTATE_CONFIG_DATE		= 0x20,
	EVSTATE_CONFIG_DATE_SET_DAY,
	EVSTATE_CONFIG_DATE_SET_MON,
	EVSTATE_CONFIG_DATE_SET_YEAR,

	EVSTATE_CONFIG_TZ		= 0x30,
	EVSTATE_CONFIG_TZ_VALUE,

	EVSTATE_CONFIG_FEATURE		= 0x40,
	EVSTATE_CONFIG_FEATURE_BLANKING,
	EVSTATE_CONFIG_FEATURE_ROTATE,

	EVSTATE_CONFIG_NTP		= 0x50,
	EVSTATE_CONFIG_NTP_ONOFF,
	EVSTATE_CONFIG_NTP_OCTET1,
	EVSTATE_CONFIG_NTP_OCTET2,
	EVSTATE_CONFIG_NTP_OCTET3,
	EVSTATE_CONFIG_NTP_OCTET4,

	EVSTATE_CONFIG_IP		= 0x60,
	EVSTATE_CONFIG_IP_DHCP,
	EVSTATE_CONFIG_IP_OCTET1,
	EVSTATE_CONFIG_IP_OCTET2,
	EVSTATE_CONFIG_IP_OCTET3,
	EVSTATE_CONFIG_IP_OCTET4,

	EVSTATE_CONFIG_NM		= 0x70,
	EVSTATE_CONFIG_NM_PREFIX,

	EVSTATE_CONFIG_GW		= 0x80,
	EVSTATE_CONFIG_GW_OCTET1,
	EVSTATE_CONFIG_GW_OCTET2,
	EVSTATE_CONFIG_GW_OCTET3,
	EVSTATE_CONFIG_GW_OCTET4,

	EVSTATE_CONFIG_SAVE		= 0x90,
	EVSTATE_CONFIG_SAVE_YESNO,

	EVSTATE_CONFIG_EXIT		= 0xa0,
};

#define LIGHTLOW	64
#define LIGHTHIGH	255

static uint8_t evstate;
static union {
	uint8_t		ip[4];
	uint32_t	u32;
	int16_t		i16;
	uint16_t	u16;
	uint8_t		u8;
} value;

void event_init(void)
{
	evstate = EVSTATE_NONE;
	ticks = CONFIG_TICKS;
}

static uint8_t maskbits(uint8_t o) __wparam
{
	switch(o) {
	case 255:	return 8;
	case 254:	return 7;
	case 252:	return 6;
	case 248:	return 5;
	case 240:	return 4;
	case 224:	return 3;
	case 192:	return 2;
	case 128:	return 1;
	}
	return 0;
}

/* Actions to enter a state */
static void setstate(uint8_t st)
{
	/* State exit actions */
	switch(evstate & 0xf0) {
	case EVSTATE_NONE:
	case EVSTATE_CONFIG_TIME:
	case EVSTATE_CONFIG_DATE:
	case EVSTATE_CONFIG_TZ:
	case EVSTATE_CONFIG_FEATURE:
	case EVSTATE_CONFIG_NTP:
	case EVSTATE_CONFIG_IP:
	case EVSTATE_CONFIG_NM:
	case EVSTATE_CONFIG_GW:
	case EVSTATE_CONFIG_SAVE:
	case EVSTATE_CONFIG_EXIT:
		display_text_outer(-3, "CONFIG", LIGHTHIGH, 1);
		display_text_middle(0, "", 0, 1);
		display_text_inner(0, "", 0, 1);
		break;
	}

	/* State entry actions */
	evstate = st;

	switch(evstate) {
	case EVSTATE_NONE:
		ticks = CONFIG_TICKS;
		clock_timer_clear(TIMER_CONFIG);
		display_set_mode(DISPMODE_CLOCK);
		return;

	case EVSTATE_CONFIG_TIME:
		ticks = CONFIG_TICKS;
		display_text_middle(-2, "TIME", LIGHTHIGH, 1);
		if(cfg.ntp_on)
			display_text_inner(-3, "-NTP-", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_TIME_SET_SEC:
	case EVSTATE_CONFIG_TIME_SET_MIN:
	case EVSTATE_CONFIG_TIME_SET_HOUR:
		ticks = CONFIG2_TICKS;
		display_text_outer(-5, "CONFIG TIME", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_DATE:
		ticks = CONFIG_TICKS;
		display_text_middle(-2, "DATE", LIGHTHIGH, 1);
		if(cfg.ntp_on)
			display_text_inner(-3, "-NTP-", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_DATE_SET_DAY:
	case EVSTATE_CONFIG_DATE_SET_MON:
	case EVSTATE_CONFIG_DATE_SET_YEAR:
		ticks = CONFIG2_TICKS;
		display_text_outer(-5, "CONFIG DATE", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_TZ:
		ticks = CONFIG_TICKS;
		display_text_middle(-4, "TIMEZONE", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_TZ_VALUE:
		ticks = CONFIG2_TICKS;
		display_text_outer(-7, "CONFIG TIMEZONE", LIGHTHIGH, 1);
		value.i16 = cfg.tzoffset;
		break;
	case EVSTATE_CONFIG_FEATURE:
		ticks = CONFIG_TICKS;
		display_text_middle(-4, "FEATURE", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_FEATURE_BLANKING:
		ticks = CONFIG2_TICKS;
		display_text_outer(-5, "CONFIG IDLE", LIGHTHIGH, 1);
		value.u16 = cfg.movetimeout;
		break;
	case EVSTATE_CONFIG_FEATURE_ROTATE:
		ticks = CONFIG2_TICKS;
		display_text_outer(-6, "CONFIG ROTATE", LIGHTHIGH, 1);
		value.u8 = cfg.orientation;
		break;
	case EVSTATE_CONFIG_NTP:
		ticks = CONFIG_TICKS;
		display_text_middle(-2, "NTP", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_NTP_ONOFF:
		ticks = CONFIG2_TICKS;
		display_text_middle(-5, "CONFIG NTP", LIGHTHIGH, 1);
		value.u8 = cfg.ntp_on;
		break;
	case EVSTATE_CONFIG_NTP_OCTET1:
		display_text_middle(-7, "CONFIG NTP PEER", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_IP:
		ticks = CONFIG_TICKS;
		display_text_middle(-1, "IP", LIGHTHIGH, 1);
		if(cfg.dhcp_on)
			display_text_inner(-3, "-DHCP-", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_IP_DHCP:
		ticks = CONFIG2_TICKS;
		display_text_outer(-5, "CONFIG DHCP", LIGHTHIGH, 1);
		value.u8 = cfg.dhcp_on;
		break;
	case EVSTATE_CONFIG_NM_PREFIX:
		ticks = CONFIG2_TICKS;
		value.u8 = maskbits(cfg.nmaddr[0]) + maskbits(cfg.nmaddr[1]) + maskbits(cfg.nmaddr[2]) + maskbits(cfg.nmaddr[3]);
		break;
	case EVSTATE_CONFIG_SAVE:
		ticks = CONFIG_TICKS;
		display_text_middle(-2, "SAVE", LIGHTHIGH, 1);
		break;
	case EVSTATE_CONFIG_SAVE_YESNO:
		ticks = CONFIG2_TICKS;
		display_text_outer(-5, "CONFIG SAVE", LIGHTHIGH, 1);
		value.u8 = 0;
		break;
	case EVSTATE_CONFIG_EXIT:
		ticks = CONFIG_TICKS;
		display_text_middle(-2, "EXIT", LIGHTHIGH, 1);
		break;
	}
	clock_timer_set(TIMER_CONFIG, ticks);
}

static char buf[12];
static void print_u8(uint8_t val, uint8_t lead)
{
	buf[0] = (lead || val >= 100) ? ((val) / 100 + '0') : ' ';
	buf[1] = (lead || val >= 10) ? ((val % 100) / 10 + '0') : ' ';
	buf[2] = (val % 10) + '0';
	buf[3] = 0;
}

void event_display_hook(void)
{
	static uint8_t b;
	static uint8_t c;

	if(evstate == EVSTATE_NONE)
		return;

	if(++c >= 4)
		b = !b;
	switch(evstate) {
	case EVSTATE_CONFIG_TIME_SET_SEC:
		clock_localtime(0);
		buf[0] = (clock_timeparts.hour / 10) + '0';
		buf[1] = (clock_timeparts.hour % 10) + '0';
		buf[2] = '-';
		buf[3] = (clock_timeparts.min / 10) + '0';
		buf[4] = (clock_timeparts.min % 10) + '0';
		buf[5] = '-';
		buf[6] = 0;
		display_text_middle(-7, buf, LIGHTLOW, 1);
		buf[0] = (clock_timeparts.sec / 10) + '0';
		buf[1] = (clock_timeparts.sec % 10) + '0';
		buf[2] = 0;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_TIME_SET_MIN:
		clock_localtime(0);
		buf[0] = (clock_timeparts.hour / 10) + '0';
		buf[1] = (clock_timeparts.hour % 10) + '0';
		buf[2] = '-';
		buf[3] = 0;
		display_text_middle(-4, buf, LIGHTLOW, 1);
		buf[0] = (clock_timeparts.min / 10) + '0';
		buf[1] = (clock_timeparts.min % 10) + '0';
		buf[2] = 0;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		buf[0] = '-';
		buf[1] = (clock_timeparts.sec / 10) + '0';
		buf[2] = (clock_timeparts.sec % 10) + '0';
		buf[3] = 0;
		display_text_middle(1, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_TIME_SET_HOUR:
		clock_localtime(0);
		buf[0] = (clock_timeparts.hour / 10) + '0';
		buf[1] = (clock_timeparts.hour % 10) + '0';
		buf[2] = 0;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 1);
		buf[0] = '-';
		buf[1] = (clock_timeparts.min / 10) + '0';
		buf[2] = (clock_timeparts.min % 10) + '0';
		buf[3] = '-';
		buf[4] = (clock_timeparts.sec / 10) + '0';
		buf[5] = (clock_timeparts.sec % 10) + '0';
		buf[6] = 0;
		display_text_middle(1, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_DATE_SET_DAY:
		clock_localtime(0);
		buf[0] = (clock_timeparts.year / 1000) + '0';
		buf[1] = (clock_timeparts.year % 1000) / 100 + '0';
		buf[2] = (clock_timeparts.year % 100) / 10 + '0';
		buf[3] = (clock_timeparts.year % 10) + '0';
		buf[4] = '-';
		buf[5] = ((clock_timeparts.mon+1) / 10) + '0';
		buf[6] = ((clock_timeparts.mon+1) % 10) + '0';
		buf[7] = '-';
		buf[8] = 0;
		display_text_middle(-9, buf, LIGHTLOW, 1);
		buf[0] = (clock_timeparts.mday / 10) + '0';
		buf[1] = (clock_timeparts.mday % 10) + '0';
		buf[2] = 0;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_DATE_SET_MON:
		clock_localtime(0);
		buf[0] = (clock_timeparts.year / 1000) + '0';
		buf[1] = (clock_timeparts.year % 1000) / 100 + '0';
		buf[2] = (clock_timeparts.year % 100) / 10 + '0';
		buf[3] = (clock_timeparts.year % 10) + '0';
		buf[4] = '-';
		buf[5] = 0;
		display_text_middle(-6, buf, LIGHTLOW, 1);
		buf[0] = ((clock_timeparts.mon+1) / 10) + '0';
		buf[1] = ((clock_timeparts.mon+1) % 10) + '0';
		buf[2] = 0;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		buf[0] = '-';
		buf[1] = (clock_timeparts.mday / 10) + '0';
		buf[2] = (clock_timeparts.mday % 10) + '0';
		buf[3] = 0;
		display_text_middle(1, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_DATE_SET_YEAR:
		clock_localtime(0);
		buf[0] = (clock_timeparts.year / 1000) + '0';
		buf[1] = (clock_timeparts.year % 1000) / 100 + '0';
		buf[2] = (clock_timeparts.year % 100) / 10 + '0';
		buf[3] = (clock_timeparts.year % 10) + '0';
		buf[4] = 0;
		display_text_middle(-2, buf, b ? LIGHTHIGH : LIGHTLOW, 1);
		buf[0] = '-';
		buf[1] = ((clock_timeparts.mon+1) / 10) + '0';
		buf[2] = ((clock_timeparts.mon+1) % 10) + '0';
		buf[3] = '-';
		buf[4] = (clock_timeparts.mday / 10) + '0';
		buf[5] = (clock_timeparts.mday % 10) + '0';
		buf[6] = 0;
		display_text_middle(2, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_TZ_VALUE:
	case EVSTATE_CONFIG_FEATURE_BLANKING:
		if(value.i16 < 0) {
			buf[0] = '-';
			buf[1] = (-value.i16 / 1000) + '0';
			buf[2] = (-value.i16 % 1000) / 100 + '0';
			buf[3] = (-value.i16 % 100) / 10 + '0';
			buf[4] = (-value.i16 % 10) + '0';
			buf[5] = 0;
		} else {
			buf[0] = (value.i16 / 1000) + '0';
			buf[1] = (value.i16 % 1000) / 100 + '0';
			buf[2] = (value.i16 % 100) / 10 + '0';
			buf[3] = (value.i16 % 10) + '0';
			buf[4] = 0;
		}
		display_text_middle(-2, buf, b ? LIGHTHIGH : LIGHTLOW, 1);
		break;
	case EVSTATE_CONFIG_FEATURE_ROTATE:
	case EVSTATE_CONFIG_NM_PREFIX:
		print_u8(value.u8, 0);
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 1);
		break;
	case EVSTATE_CONFIG_NTP_OCTET1:
	case EVSTATE_CONFIG_IP_OCTET1:
	case EVSTATE_CONFIG_GW_OCTET1:
		print_u8(value.ip[0], 1);
		buf[2] |= 0x80;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 1);
		print_u8(value.ip[1], 1);
		buf[2] |= 0x80;
		display_text_middle(2, buf, LIGHTLOW, 0);
		print_u8(value.ip[2], 1);
		buf[2] |= 0x80;
		display_text_middle(5, buf, LIGHTLOW, 0);
		print_u8(value.ip[3], 1);
		display_text_middle(8, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_NTP_OCTET2:
	case EVSTATE_CONFIG_IP_OCTET2:
	case EVSTATE_CONFIG_GW_OCTET2:
		print_u8(value.ip[0], 1);
		buf[2] |= 0x80;
		display_text_middle(-4, buf, LIGHTLOW, 1);
		print_u8(value.ip[1], 1);
		buf[2] |= 0x80;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		print_u8(value.ip[2], 1);
		buf[2] |= 0x80;
		display_text_middle(2, buf, LIGHTLOW, 0);
		print_u8(value.ip[3], 1);
		display_text_middle(5, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_NTP_OCTET3:
	case EVSTATE_CONFIG_IP_OCTET3:
	case EVSTATE_CONFIG_GW_OCTET3:
		print_u8(value.ip[0], 1);
		buf[2] |= 0x80;
		display_text_middle(-7, buf, LIGHTLOW, 1);
		print_u8(value.ip[1], 1);
		buf[2] |= 0x80;
		display_text_middle(-4, buf, LIGHTLOW, 0);
		print_u8(value.ip[2], 1);
		buf[2] |= 0x80;
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		print_u8(value.ip[3], 1);
		display_text_middle(2, buf, LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_NTP_OCTET4:
	case EVSTATE_CONFIG_IP_OCTET4:
	case EVSTATE_CONFIG_GW_OCTET4:
		print_u8(value.ip[0], 1);
		buf[3] |= 0x80;
		display_text_middle(-10, buf, LIGHTLOW, 1);
		print_u8(value.ip[1], 1);
		buf[3] |= 0x80;
		display_text_middle(-7, buf, LIGHTLOW, 0);
		print_u8(value.ip[2], 1);
		buf[3] |= 0x80;
		display_text_middle(-4, buf, LIGHTLOW, 0);
		print_u8(value.ip[3], 1);
		display_text_middle(-1, buf, b ? LIGHTHIGH : LIGHTLOW, 0);
		break;
	case EVSTATE_CONFIG_SAVE_YESNO:
		if(value.u8) {
			display_text_middle(-2, "YES", b ? LIGHTHIGH : LIGHTLOW, 1);
			display_text_middle(1, "-NO", LIGHTLOW, 0);
		} else {
			display_text_middle(-5, "YES-", LIGHTLOW, 1);
			display_text_middle(-1, "NO", b ? LIGHTHIGH : LIGHTLOW, 0);
		}
	case EVSTATE_CONFIG_IP_DHCP:
	case EVSTATE_CONFIG_NTP_ONOFF:
		if(value.u8) {
			display_text_middle(-1, "ON", b ? LIGHTHIGH : LIGHTLOW, 1);
			display_text_middle(1, "-OFF", LIGHTLOW, 0);
		} else {
			display_text_middle(-4, "ON-", LIGHTLOW, 1);
			display_text_middle(-1, "OFF", b ? LIGHTHIGH : LIGHTLOW, 0);
		}
		break;
	}
}

void event_handle_timer(void)
{
	/* Reset the state at timeout */
	setstate(EVSTATE_NONE);
}

static void update_ntp(void)
{
	if(IP_IS_VALID(value.ip) || IP_IS_MULTICAST(value.ip) || IP_IS_BROADCAST(value.ip)) {
		IP_CP(cfg.ntpaddr, value.ip);
		ntp_toggle();
		ntp_toggle();
	}
	setstate(EVSTATE_CONFIG_NTP);
}

static void update_ip(void)
{
	if(IP_IS_VALID(value.ip)) {
		IP_CP(cfg.ipaddr, value.ip);
		ntp_toggle();
		ntp_toggle();
	}
	setstate(EVSTATE_CONFIG_IP);
}

static void update_gw(void)
{
	if(IP_IS_VALID(value.ip)) {
		IP_CP(cfg.gwaddr, value.ip);
		ntp_toggle();
		ntp_toggle();
	}
	setstate(EVSTATE_CONFIG_GW);
}

void event_handle(uint8_t ev) __wparam
{
	switch(evstate) {
	case EVSTATE_NONE:
		if(ev == EV_PRESS_SELECT) {
			display_set_mode(DISPMODE_TEXT);
			setstate(EVSTATE_CONFIG_TIME);
		}
		break;

/* Time config */
	case EVSTATE_CONFIG_TIME:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_DATE);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_EXIT);
		else if(ev == EV_PRESS_SET && !cfg.ntp_on)
			setstate(EVSTATE_CONFIG_TIME_SET_SEC);
		break;

	case EVSTATE_CONFIG_TIME_SET_SEC:
		if(ev == EV_PRESS_UP) {
			cli(); clock_secs++; sti();
		} else if(ev == EV_PRESS_DOWN) {
			cli(); clock_secs--; sti();
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_TIME_SET_MIN);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_TIME);
		break;

	case EVSTATE_CONFIG_TIME_SET_MIN:
		if(ev == EV_PRESS_UP) {
			cli(); clock_secs += 60; sti();
		} else if(ev == EV_PRESS_DOWN) {
			cli(); clock_secs -= 60; sti();
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_TIME_SET_HOUR);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_TIME);
		break;

	case EVSTATE_CONFIG_TIME_SET_HOUR:
		if(ev == EV_PRESS_UP) {
			cli(); clock_secs += 3600; sti();
		} else if(ev == EV_PRESS_DOWN) {
			cli(); clock_secs -= 3600; sti();
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_TIME_SET_SEC);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_TIME);
		break;

/* Date config */
	case EVSTATE_CONFIG_DATE:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_TZ);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_TIME);
		else if(ev == EV_PRESS_SET && !cfg.ntp_on)
			setstate(EVSTATE_CONFIG_DATE_SET_DAY);
		break;

	case EVSTATE_CONFIG_DATE_SET_DAY:
		if(ev == EV_PRESS_UP) {
			cli(); clock_secs += 86400; sti();
		} else if(ev == EV_PRESS_DOWN) {
			cli(); clock_secs -= 86400; sti();
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_DATE_SET_MON);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_DATE);
		break;

	case EVSTATE_CONFIG_DATE_SET_MON:
		if(ev == EV_PRESS_UP) {
			clock_localtime(0);
			if(++clock_timeparts.mon >= 12)
				clock_timeparts.mon = 0;
			clock_mktime();
		} else if(ev == EV_PRESS_DOWN) {
			clock_localtime(0);
			if(--clock_timeparts.mon >= 12)
				clock_timeparts.mon = 0;
			clock_mktime();
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_DATE_SET_YEAR);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_DATE);
		break;

	case EVSTATE_CONFIG_DATE_SET_YEAR:
		if(ev == EV_PRESS_UP) {
			clock_localtime(0);
			clock_timeparts.year++;
			clock_mktime();
		} else if(ev == EV_PRESS_DOWN) {
			clock_localtime(0);
			clock_timeparts.year--;
			clock_mktime();
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_DATE_SET_DAY);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_DATE);
		break;

/* Timezone config */
	case EVSTATE_CONFIG_TZ:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_FEATURE);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_DATE);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_TZ_VALUE);
		break;

	case EVSTATE_CONFIG_TZ_VALUE:
		if(ev == EV_PRESS_UP) {
			if(cfg.tzoffset < 15*95)
				value.i16 = cfg.tzoffset += 15;
		} else if(ev == EV_PRESS_DOWN) {
			if(cfg.tzoffset > -15*95)
				value.i16 = cfg.tzoffset -= 15;
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_TZ);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_TZ);
		break;

/* Feature config */
	case EVSTATE_CONFIG_FEATURE:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_NTP);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_TZ);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_FEATURE_BLANKING);
		break;

	case EVSTATE_CONFIG_FEATURE_BLANKING:
		if(ev == EV_PRESS_UP) {
			if(cfg.movetimeout < 60*60)
				value.i16 = cfg.movetimeout += 15;
		} else if(ev == EV_PRESS_DOWN) {
			if(cfg.movetimeout > 0)
				value.i16 = cfg.movetimeout -= 15;
		} else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_FEATURE_ROTATE);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_FEATURE);
		break;

	case EVSTATE_CONFIG_FEATURE_ROTATE:
		if(ev == EV_PRESS_UP)
			value.u8 = (cfg.orientation + 1) % 12;
		else if(ev == EV_PRESS_DOWN)
			value.u8 = (cfg.orientation - 1 + 12) % 12;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_FEATURE_BLANKING);
		else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_FEATURE);
		break;

/* NTP config */
	case EVSTATE_CONFIG_NTP:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_IP);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_FEATURE);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_NTP_ONOFF);
		break;

	case EVSTATE_CONFIG_NTP_ONOFF:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_DOWN)
			value.u8 = !value.u8;
		else if(ev == EV_PRESS_SET) {
			cfg.ntp_on = value.u8;
			IP_CP(value.ip, cfg.ntpaddr);
			setstate(cfg.ntp_on ? EVSTATE_CONFIG_NTP_OCTET1 : EVSTATE_CONFIG_NTP);
		} else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_NTP);
		break;

	case EVSTATE_CONFIG_NTP_OCTET1:
		if(ev == EV_PRESS_UP)
			value.ip[0]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[0]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_NTP_OCTET2);
		else if(ev == EV_PRESS_SELECT)
			update_ntp();
		break;

	case EVSTATE_CONFIG_NTP_OCTET2:
		if(ev == EV_PRESS_UP)
			value.ip[1]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[1]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_NTP_OCTET3);
		else if(ev == EV_PRESS_SELECT)
			update_ntp();
		break;

	case EVSTATE_CONFIG_NTP_OCTET3:
		if(ev == EV_PRESS_UP)
			value.ip[2]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[2]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_NTP_OCTET4);
		else if(ev == EV_PRESS_SELECT)
			update_ntp();
		break;

	case EVSTATE_CONFIG_NTP_OCTET4:
		if(ev == EV_PRESS_UP)
			value.ip[3]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[3]--;
		else if(ev == EV_PRESS_SELECT || ev == EV_PRESS_SET)
			update_ntp();
		break;

/* IP config */
	case EVSTATE_CONFIG_IP:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(cfg.dhcp_on ? EVSTATE_CONFIG_SAVE : EVSTATE_CONFIG_NM);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_NTP);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_IP_DHCP);
		break;

	case EVSTATE_CONFIG_IP_DHCP:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_DOWN)
			value.u8 = !value.u8;
		else if(ev == EV_PRESS_SET) {
			cfg.dhcp_on = value.u8;
			IP_CP(value.ip, cfg.ipaddr);
			setstate(cfg.dhcp_on ? EVSTATE_CONFIG_IP : EVSTATE_CONFIG_IP_OCTET1);
		} else if(ev == EV_PRESS_SELECT)
			setstate(cfg.dhcp_on ? EVSTATE_CONFIG_SAVE : EVSTATE_CONFIG_NM);
		break;

	case EVSTATE_CONFIG_IP_OCTET1:
		if(ev == EV_PRESS_UP)
			value.ip[0]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[0]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_IP_OCTET2);
		else if(ev == EV_PRESS_SELECT)
			update_ip();
		break;

	case EVSTATE_CONFIG_IP_OCTET2:
		if(ev == EV_PRESS_UP)
			value.ip[1]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[1]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_IP_OCTET3);
		else if(ev == EV_PRESS_SELECT)
			update_ip();
		break;

	case EVSTATE_CONFIG_IP_OCTET3:
		if(ev == EV_PRESS_UP)
			value.ip[2]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[2]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_IP_OCTET4);
		else if(ev == EV_PRESS_SELECT)
			update_ip();
		break;

	case EVSTATE_CONFIG_IP_OCTET4:
		if(ev == EV_PRESS_UP)
			value.ip[3]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[3]--;
		else if(ev == EV_PRESS_SELECT || ev == EV_PRESS_SET)
			update_ip();
		break;

/* Netmask config */
	case EVSTATE_CONFIG_NM:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_GW);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_IP);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_NM_PREFIX);
		break;

	case EVSTATE_CONFIG_NM_PREFIX:
		if(ev == EV_PRESS_UP) {
			if(value.u8 < 30)
				value.u8++;
		} else if(ev == EV_PRESS_DOWN) {
			if(value.u8 > 0)
				value.u8--;
		} else if(ev == EV_PRESS_SET) {
			IP_SET(cfg.nmaddr, 0);
			if(value.u8 > 30)
				value.u8 = 30;
			if(value.u8 > 8) {
				cfg.nmaddr[0] = 255;
				value.u8 -= 8;
				if(value.u8 > 8) {
					cfg.nmaddr[1] = 255;
					value.u8 -= 8;
					if(value.u8 > 8) {
						cfg.nmaddr[2] = 255;
						cfg.nmaddr[3] = 255 << (8-(value.u8-8));
					} else
						cfg.nmaddr[2] = 255 << (8-value.u8);
				} else
					cfg.nmaddr[1] = 255 << (8-value.u8);
			} else
				cfg.nmaddr[0] = 255 << (8-value.u8);
			setstate(EVSTATE_CONFIG_NM);
		} else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_NM);
		break;

/* Gateway config */
	case EVSTATE_CONFIG_GW:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_SAVE);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_NM);
		else if(ev == EV_PRESS_SET) {
			IP_CP(value.ip, cfg.gwaddr);
			setstate(EVSTATE_CONFIG_GW_OCTET1);
		}
		break;

	case EVSTATE_CONFIG_GW_OCTET1:
		if(ev == EV_PRESS_UP)
			value.ip[0]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[0]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_GW_OCTET2);
		else if(ev == EV_PRESS_SELECT)
			update_gw();
		break;

	case EVSTATE_CONFIG_GW_OCTET2:
		if(ev == EV_PRESS_UP)
			value.ip[1]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[1]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_GW_OCTET3);
		else if(ev == EV_PRESS_SELECT)
			update_gw();
		break;

	case EVSTATE_CONFIG_GW_OCTET3:
		if(ev == EV_PRESS_UP)
			value.ip[2]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[2]--;
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_GW_OCTET4);
		else if(ev == EV_PRESS_SELECT)
			update_gw();
		break;

	case EVSTATE_CONFIG_GW_OCTET4:
		if(ev == EV_PRESS_UP)
			value.ip[3]++;
		else if(ev == EV_PRESS_DOWN)
			value.ip[3]--;
		else if(ev == EV_PRESS_SELECT || ev == EV_PRESS_SET)
			update_gw();
		break;

/* Save config */
	case EVSTATE_CONFIG_SAVE:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_EXIT);
		else if(ev == EV_PRESS_DOWN)
			setstate(cfg.dhcp_on ? EVSTATE_CONFIG_IP : EVSTATE_CONFIG_GW);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_CONFIG_SAVE_YESNO);
		break;

	case EVSTATE_CONFIG_SAVE_YESNO:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_DOWN)
			value.u8 = !value.u8;
		else if(ev == EV_PRESS_SET) {
			if(value.u8)
				config_save();
			setstate(EVSTATE_NONE);
		} else if(ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_SAVE);
		break;

/* Exit config */
	case EVSTATE_CONFIG_EXIT:
		if(ev == EV_PRESS_UP || ev == EV_PRESS_SELECT)
			setstate(EVSTATE_CONFIG_TIME);
		else if(ev == EV_PRESS_DOWN)
			setstate(EVSTATE_CONFIG_SAVE);
		else if(ev == EV_PRESS_SET)
			setstate(EVSTATE_NONE);
		break;

	default:
		setstate(EVSTATE_NONE);
		break;
	}
}
