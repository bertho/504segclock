/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <network.h>
#include <config.h>
#include <cmdline.h>
#include <serial.h>
#include <clock.h>
#include <io.h>
#include <dhcp.h>
#include <ntp.h>
#include <rtcc.h>
#include <i2c.h>
#include <adc.h>

#define mytoupper(c)	((c) >= 'a' && (c) <= 'z' ? ((c)-('a'-'A')) : (c))

static uint8_t state;
static uint8_t menustate;

static union _ip_mac {
	uint8_t		mac[6];
	uint8_t		ip[4];
	uint16_t	port;
	int16_t		signum;
	uint32_t	bignum;
	char		hostname[MAXHOSTNAME];
} mc;
static uint8_t octet;
static uint8_t setaddr;

enum {
	STATE_INIT,
	STATE_MENU,
	STATE_PROMPT,
	STATE_MENUINPUT,
	STATE_SETMAC,
	STATE_OCTET,
	STATE_MACOCTET,
	STATE_NUMBER,
	STATE_SIGNUM,
	STATE_SIGNUMMINUS,
	STATE_BIGNUMBER,
	STATE_HOSTNAME,
	STATE_CONFIRM_SAVE,
	STATE_CONFIRM_SAVERTCC,
	STATE_CONFIRM_FACTORYRESET,
	STATE_CONFIRM_DEVICERESET,
};

enum {
	MENU_MAIN,
	MENU_NETWORK,
	MENU_TIME,
	MENU_MISC,
};

enum {
	SETADDR_IP,
	SETADDR_NTP,
	SETADDR_NM,
	SETADDR_GW,
	SETADDR_DNS1,
	SETADDR_DNS2,
	SETINT_PORT,
	SETINT_TZ,
	SETINT_RTCCCAL,
	SETINT_MOVETIMEOUT,
};

void cmdline_init(void)
{
	state = STATE_INIT;
	menustate = MENU_MAIN;
}

#ifndef BUILDID
#error "Missing BUILDID define"
#endif

#ifndef VERSIONID
#error "Missing VERSIONID define"
#endif

static const char header[] =
	"\n"
	"504 Segment Clock v" VERSIONID " (c) 2012 Vagrearg\n"
	"BuildId: " BUILDID "\n"
	"Distributed under GPLv3.\n\n"
	"An ethernet enabled clock. A lot of tech for showing (off) time.\n"
	"\n"
	;

static const char menu_main[] =
	"Main menu:\n"
	"  C - show configuration\n"
	"  M - miscellaneous menu\n"
	"  N - network config menu\n"
	"  O - rotate orientation\n"
	"  S - save configuration\n"
	"  T - time config menu\n"
	"  V - version information\n"
	;

static const char menu_network[] =
	"Network menu:\n"
	"  C - show configuration\n"
	"  D - toggle DHCP/static IP\n"
	"  G - set gateway address\n"
	"  H - set hostname\n"
	"  I - set IP address\n"
	"  M - set MAC address\n"
	"  N - set netmask\n"
	"  O - toggle use DHCP hostname\n"
	"  P - clock listen port\n"
	"  R - DNS server 1 IP\n"
	"  S - DNS server 2 IP\n"
	"  . - back to main menu\n"
	;

static const char menu_time[] =
	"Time menu:\n"
	"  A - show RTC calibration\n"
	"  C - show configuration\n"
	"  D - toggle use DHCP's timeserver\n"
	"  I - set NTP server IP address\n"
	"  M - set movement timeout for blanking\n"
	"  N - toggle NTP on/off\n"
	"  P - toggle use NTP MC peer address\n"
	"  Q - toggle NTP sync to RTCC\n"
	"  R - set RTC calibration\n"
	"  S - save system time to RTC\n"
	"  T - show time\n"
	"  Z - set timezone offset\n"
	"  . - back to main menu\n"
	;

static const char menu_misc[] =
	"Miscellaneous menu:\n"
	"  1 - toggle left green LED\n"
	"  2 - toggle left yellow LED\n"
	"  3 - toggle right green LED\n"
	"  4 - toggle right yellow LED\n"
	"  A - dump ARP table\n"
	"  C - show configuration\n"
	"  E - show ethernet status\n"
	"  F - factory reset\n"
	"  R - reset/reboot\n"
	"  T - dump rtcc registers and sram\n"
	"  U - dump rtcc eeprom and uid\n"
	"  . - back to main menu\n"
	;

static void print_addr_class(uint8_t *addr)
{
	if(IP_IS_MULTICAST(addr))
		serial_puts(" (multicast)");
	else if(IP_IS_BROADCAST(addr))
		serial_puts(" (broadcast)");
	else if(IP_IS_VALID(addr))
		serial_puts(" (unicast)");
	else
		serial_puts(" (invalid)");
}

static const char dow[7][4] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

static void show_time(void)
{
	uint8_t tzh, tzm;
	rtccdatetime_t dt;

	clock_localtime(1);	/* Calculate the current date in UTC */
	rtcc_read(&dt);		/* Get current RTCC data (is in UTC) */

	serial_puts("System UTC time and zone offset: ");
	serial_puts(dow[clock_timeparts.wday]);
	serial_putc(' ');
	serial_putdec_u16(clock_timeparts.year);
	serial_putc('-');
	if(clock_timeparts.mon < 9)
		serial_putc('0');
	serial_putdec(clock_timeparts.mon+1);
	serial_putc('-');
	if(clock_timeparts.mday < 10)
		serial_putc('0');
	serial_putdec(clock_timeparts.mday);
	serial_putc(' ');
	if(clock_timeparts.hour < 10)
		serial_putc('0');
	serial_putdec(clock_timeparts.hour);
	serial_putc(':');
	if(clock_timeparts.min < 10)
		serial_putc('0');
	serial_putdec(clock_timeparts.min);
	serial_putc(':');
	if(clock_timeparts.sec < 10)
		serial_putc('0');
	serial_putdec(clock_timeparts.sec);
	serial_putc('.');
	if(clock_timeparts.msec < 10)
		serial_putc('0');
	if(clock_timeparts.msec < 100)
		serial_putc('0');
	serial_putdec_u16(clock_timeparts.msec);
	serial_putc(' ');
	if(cfg.tzoffset < 0) {
		serial_putc('-');
		tzh = cfg.tzoffset / -60;
		tzm = (-cfg.tzoffset) % 60;
	} else {
		serial_putc('+');
		tzh = cfg.tzoffset / 60;
		tzm = cfg.tzoffset % 60;
	}
	if(tzh < 10)
		serial_putc('0');
	serial_putdec(tzh);
	if(tzm < 10)
		serial_putc('0');
	serial_putdec(tzm);
	/*serial_puts(" (day ");
	serial_putdec_u16(clock_timeparts.yday);
	serial_puts(")\nRTCC UTC time                  : ");*/
	serial_puts("\nRTCC UTC time                  : ");
	serial_puts(dow[dt.wday-1]);
	serial_puts(" 20");
	serial_putc(dt.year10 + '0');
	serial_putc(dt.year1 + '0');
	serial_putc('-');
	serial_putc(dt.mon10 + '0');
	serial_putc(dt.mon1 + '0');
	serial_putc('-');
	serial_putc(dt.mday10 + '0');
	serial_putc(dt.mday1 + '0');
	serial_putc(' ');
	serial_putc(dt.hour10 + '0');
	serial_putc(dt.hour1 + '0');
	serial_putc(':');
	serial_putc(dt.min10 + '0');
	serial_putc(dt.min1 + '0');
	serial_putc(':');
	serial_putc(dt.sec10 + '0');
	serial_putc(dt.sec1 + '0');
	serial_putc('\n');
}

static void show_config(void)
{
	serial_puts("\nSerial nr.    : ");
	serial_put(serialnr, sizeof(serialnr));
	serial_puts("\nSource MAC    : ");
	serial_puthex(cfg.macaddr[0]);	serial_putc(':');
	serial_puthex(cfg.macaddr[1]);	serial_putc(':');
	serial_puthex(cfg.macaddr[2]);	serial_putc(':');
	serial_puthex(cfg.macaddr[3]);	serial_putc(':');
	serial_puthex(cfg.macaddr[4]);	serial_putc(':');
	serial_puthex(cfg.macaddr[5]);
	serial_puts("\n\nClock port    : ");
	serial_putdec_u16(cfg.port);
	serial_puts(" (0x");
	serial_puthex_u16(cfg.port);
	serial_puts(")\nSource IP     : ");
	serial_putdec(cfg.ipaddr[0]);	serial_putc('.');
	serial_putdec(cfg.ipaddr[1]);	serial_putc('.');
	serial_putdec(cfg.ipaddr[2]);	serial_putc('.');
	serial_putdec(cfg.ipaddr[3]);
	serial_puts("\nNetmask       : ");
	serial_putdec(cfg.nmaddr[0]);	serial_putc('.');
	serial_putdec(cfg.nmaddr[1]);	serial_putc('.');
	serial_putdec(cfg.nmaddr[2]);	serial_putc('.');
	serial_putdec(cfg.nmaddr[3]);
	serial_puts("\nGateway IP    : ");
	serial_putdec(cfg.gwaddr[0]);	serial_putc('.');
	serial_putdec(cfg.gwaddr[1]);	serial_putc('.');
	serial_putdec(cfg.gwaddr[2]);	serial_putc('.');
	serial_putdec(cfg.gwaddr[3]);
	serial_puts("\nDNS Server 1  : ");
	serial_putdec(cfg.dnsaddr1[0]);	serial_putc('.');
	serial_putdec(cfg.dnsaddr1[1]);	serial_putc('.');
	serial_putdec(cfg.dnsaddr1[2]);	serial_putc('.');
	serial_putdec(cfg.dnsaddr1[3]);
	serial_puts("\nDNS Server 2  : ");
	serial_putdec(cfg.dnsaddr2[0]);	serial_putc('.');
	serial_putdec(cfg.dnsaddr2[1]);	serial_putc('.');
	serial_putdec(cfg.dnsaddr2[2]);	serial_putc('.');
	serial_putdec(cfg.dnsaddr2[3]);
	serial_puts("\nHostname      : ");
	serial_puts(cfg.hostname);
	serial_puts("\nDHCP          : ");
	serial_puts(cfg.dhcp_on ? "On" : "Off");
	if(cfg.dhcp_on) {
		serial_puts("\nDHCP hostname : ");
		serial_puts(cfg.dhcp_hostname ? "On" : "Off");
		serial_puts("\nLease time    : ");
		serial_putdec_u32(leasetime);
		serial_puts("\nRebind time   : ");
		serial_putdec_u32(rebindingtime);
		serial_puts("\nRenew time    : ");
		serial_putdec_u32(renewaltime);
	}
	serial_puts("\nNTP           : ");
	serial_puts(cfg.ntp_on ? "On" : "Off");
	serial_puts("\nNTP server    : ");
	serial_putdec(cfg.ntpaddr[0]);	serial_putc('.');
	serial_putdec(cfg.ntpaddr[1]);	serial_putc('.');
	serial_putdec(cfg.ntpaddr[2]);	serial_putc('.');
	serial_putdec(cfg.ntpaddr[3]);
	print_addr_class(cfg.ntpaddr);
	serial_puts("\nNTP use DHCP  : ");
	serial_puts(cfg.ntp_use_dhcp ? "On" : "Off");
	serial_puts("\nNTP MC peer   : ");
	serial_puts(cfg.ntp_mcpeer ? "On" : "Off");
	serial_puts("\nNTP syncs RTCC: ");
	serial_puts(cfg.ntp_to_rtcc ? "On" : "Off");

	serial_puts("\n\nOrientation   : ");
	serial_putdec(cfg.orientation);
	serial_puts("\nMove timeout  : ");
	serial_putdec_u16(cfg.movetimeout);
	serial_puts("\nUptime        : ");
	serial_putdec_u32(clock_uptime);
	serial_puts("\nLight-sensor  : ");
	serial_putdec(light);

	serial_puts("\n\nButtonState        : 0x");
	serial_puthex(PORTB & BUTTONMASK);
	serial_puts("\nLeft Green (IP)    : ");
	serial_puts(io_led_ip_state() ? "On" : "Off");
	serial_puts("\nLeft Yellow (CPU)  : ");
	serial_puts(io_led_cpu_state() ? "On" : "Off");
	serial_puts("\nRight Green (Move) : ");
	serial_puts(io_led_move_state() ? "On" : "Off");
	serial_puts("\nRight Yellow (Err) : ");
	serial_puts(io_led_err_state() ? "On" : "Off");
	serial_putc('\n');
	if(cfg.ntp_on) {
		serial_putc('\n');
		ntp_dump_status();
	}
	serial_putc('\n');
	show_time();
}

static void show_ethstatus(void)
{
	serial_puts("\nPHCON1 : 0x");
	serial_puthex_u16(ethernet_getphy(PHCON1));
	serial_puts("\nPHSTAT1: 0x");
	serial_puthex_u16(ethernet_getphy(PHSTAT1));
	serial_puts("\nPHCON2 : 0x");
	serial_puthex_u16(ethernet_getphy(PHCON2));
	serial_puts("\nPHSTAT2: 0x");
	serial_puthex_u16(ethernet_getphy(PHSTAT2));
	serial_puts("\nPHIE   : 0x");
	serial_puthex_u16(ethernet_getphy(PHIE));
	serial_puts("\nPHIR   : 0x");
	serial_puthex_u16(ethernet_getphy(PHIR));
	serial_puts("\nPHLCON : 0x");
	serial_puthex_u16(ethernet_getphy(PHLCON));
	serial_puts("\nEIR    : 0x");
	serial_puthex(EIR);
	serial_puts("\nEIE    : 0x");
	serial_puthex(EIE);
	serial_puts("\nEPKTCNT: 0x");
	serial_puthex(EPKTCNT);
	serial_puts("\nECON1  : 0x");
	serial_puthex(ECON1);
	serial_puts("\nECON2  : 0x");
	serial_puthex(ECON2);
	serial_puts("\nESTAT  : 0x");
	serial_puthex(ESTAT);
	serial_putc('\n');
}

static void unknown_input(char ch)
{
	serial_puts("0x");
	serial_puthex(ch);
	serial_puts(": Unknown command\n");
	state = STATE_MENU;
}

static void handle_menu_main(char ch)
{
	switch(ch) {
	case 'C':
		show_config();
		break;

	case 'M':
		state = STATE_MENU;
		menustate = MENU_MISC;
		break;

	case 'N':
		state = STATE_MENU;
		menustate = MENU_NETWORK;
		break;

	case 'O':
		cfg.orientation++;
		cfg.orientation %= 12;
		serial_puts("Orientation now: ");
		serial_putdec(cfg.orientation);
		serial_putc('\n');
		break;

	case 'S':
		serial_puts("Save config (device will reset after save). Press (uppercase) 'Y' to confirm: ");
		state = STATE_CONFIRM_SAVE;
		break;

	case 'T':
		state = STATE_MENU;
		menustate = MENU_TIME;
		break;

	case 'V':
		serial_puts(header);
		break;

	default:
		unknown_input(ch);
		break;
	}
}

static void handle_menu_network(char ch)
{
	switch(ch) {
	case 'C':
		show_config();
		break;

	case 'D':
		dhcp_toggle();
		serial_puts("DHCP now '");
		serial_puts(cfg.dhcp_on ? "On'\n" : "Off'\n");
		break;

	case 'G':
		if(cfg.dhcp_on) {
			serial_puts("Error: Cannot set gateway while DHCP enabled\n");
			break;
		}
		serial_puts("Enter gateway address: ");
		state = STATE_OCTET;
		setaddr = SETADDR_GW;
		octet = 0;
		IP_SET(mc.ip, 0);
		break;

	case 'H':
		serial_puts("Enter hostname: ");
		memset(mc.hostname, 0xff, sizeof(mc.hostname));
		mc.hostname[0] = 0;
		octet = 0;
		state = STATE_HOSTNAME;
		break;

	case 'I':
		if(cfg.dhcp_on) {
			serial_puts("Error: Cannot set IP address while DHCP enabled\n");
			break;
		}
		serial_puts("Enter IP address: ");
		state = STATE_OCTET;
		setaddr = SETADDR_IP;
		octet = 0;
		IP_SET(mc.ip, 0);
		break;

	case 'M':
		if(cfg.dhcp_on) {
			serial_puts("Error: Cannot set MAC address while DHCP enabled\n");
			break;
		}
		serial_puts("Enter MAC address: ");
		state = STATE_MACOCTET;
		octet = 0;
		MAC_SET(mc.mac, 0);
		break;

	case 'N':
		if(cfg.dhcp_on) {
			serial_puts("Error: Cannot set netmask while DHCP enabled\n");
			break;
		}
		serial_puts("Enter netmask: ");
		state = STATE_OCTET;
		setaddr = SETADDR_NM;
		octet = 0;
		IP_SET(mc.ip, 0);
		break;

	case 'O':
		cfg.dhcp_hostname = cfg.dhcp_hostname ? 0 : 1;
		serial_puts("DHCP hostname option now '");
		serial_puts(cfg.dhcp_hostname ? "On'\n" : "Off'\n");
		break;

	case 'P':
		serial_puts("Enter clock listen port: ");
		state = STATE_NUMBER;
		setaddr = SETINT_PORT;
		mc.port = 0;
		break;

	case 'R':
	case 'S':
		if(cfg.dhcp_on) {
			serial_puts("Error: Cannot set DNS address while DHCP enabled\n");
			break;
		}
		serial_puts("Enter DNS IP address: ");
		state = STATE_OCTET;
		setaddr = ch == 'R' ? SETADDR_DNS1 : SETADDR_DNS2;
		octet = 0;
		IP_SET(mc.ip, 0);
		break;

	case '\x1b':
	case '-':
	case '<':
	case '.':
		state = STATE_MENU;
		menustate = MENU_MAIN;
		break;

	default:
		unknown_input(ch);
		break;
	}
}

static void handle_menu_time(char ch)
{
	switch(ch) {
	case 'A':
		serial_puts("Current RTCC calibration value: ");
		serial_putdec_s8(rtcc_cal_read());
		serial_putc('\n');
		break;

	case 'C':
		show_config();
		break;

	case 'D':
		cfg.ntp_use_dhcp = !cfg.ntp_use_dhcp;
		serial_puts("NTP uses DHCP's timeserver now '");
		serial_puts(cfg.ntp_use_dhcp ? "On'\n" : "Off'\n");
		break;

	case 'I':
		serial_puts("Enter NTP server IP (broad-, multi- or uni-cast) address: ");
		state = STATE_OCTET;
		setaddr = SETADDR_NTP;
		octet = 0;
		IP_SET(mc.ip, 0);
		break;

	case 'M':
		serial_puts("Enter movement timeout for blanking in seconds (0 disables): ");
		state = STATE_NUMBER;
		setaddr = SETINT_MOVETIMEOUT;
		mc.port = 0;
		break;

	case 'N':
		ntp_toggle();
		serial_puts("NTP now '");
		serial_puts(cfg.ntp_on ? "On'\n" : "Off'\n");
		break;

	case 'P':
		ntp_toggle_mcpeer();
		serial_puts("NTP MC peer now '");
		serial_puts(cfg.ntp_mcpeer ? "On'\n" : "Off'\n");
		break;

	case 'Q':
		cfg.ntp_to_rtcc = !cfg.ntp_to_rtcc;
		serial_puts("NTP sync to RTCC now '");
		serial_puts(cfg.ntp_to_rtcc ? "On'\n" : "Off'\n");
		break;

	case 'R':
		serial_puts("Enter RTCC calibration value: ");
		state = STATE_SIGNUM;
		setaddr = SETINT_RTCCCAL;
		mc.signum = 0;
		break;

	case 'S':
		serial_puts("Save system time to RTC.\n. Press (uppercase) 'Y' to confirm: ");
		state = STATE_CONFIRM_SAVERTCC;
		break;

	case 'T':
		show_time();
		break;

	case 'Z':
		serial_puts("Enter timezone offset in minutes: ");
		state = STATE_SIGNUM;
		setaddr = SETINT_TZ;
		mc.signum = 0;
		break;

	case '\x1b':
	case '-':
	case '<':
	case '.':
		state = STATE_MENU;
		menustate = MENU_MAIN;
		break;

	default:
		unknown_input(ch);
		break;
	}
}

static void handle_menu_misc(char ch)
{
	switch(ch) {
	case '1':
		io_led_ip_toggle();
		serial_puts("Toggle left green --> '");
		serial_puts(io_led_ip_state() ? "On'\n" : "Off'\n");
		break;

	case '2':
		io_led_cpu_toggle();
		serial_puts("Toggle left yellow --> '");
		serial_puts(io_led_cpu_state() ? "On'\n" : "Off'\n");
		break;

	case '3':
		io_led_move_toggle();
		serial_puts("Toggle right green --> '");
		serial_puts(io_led_move_state() ? "On'\n" : "Off'\n");
		break;

	case '4':
		io_led_err_toggle();
		serial_puts("Toggle right yellow --> '");
		serial_puts(io_led_err_state() ? "On'\n" : "Off'\n");
		break;

	case 'A':
		arp_dump_arptable();
		break;

	case 'C':
		show_config();
		break;

	case 'E':
		show_ethstatus();
		break;

	case 'F':
		serial_puts("Factory reset will remove all saved parameters.\nThe device will reset afterwards. Press (uppercase) 'Y' to confirm: ");
		state = STATE_CONFIRM_FACTORYRESET;
		break;

	case 'R':
		serial_puts("Device restart (loads last saved parameters).\nPress (uppercase) 'Y' to confirm: ");
		state = STATE_CONFIRM_DEVICERESET;
		break;

	case 'T':
		serial_puts("RTCC registers:");
		for(octet = 0; octet < 0x20; octet++) {
			if(!(octet & 7)) {
				serial_puts("\n0x");
				serial_puthex(octet);
				serial_puts(": ");
			}
			serial_puthex(i2c_reg_read(octet));
			serial_putc(' ');
		}
		serial_puts("\nRTCC SRAM:");
		for(; octet < 0x60; octet++) {
			if(!(octet & 7)) {
				serial_puts("\n0x");
				serial_puthex(octet);
				serial_puts(": ");
			}
			serial_puthex(i2c_reg_read(octet));
			serial_putc(' ');
		}
		serial_putc('\n');
		break;

	case 'U':
		serial_puts("EEPROM content:");
		for(octet = 0; octet < 0x80; octet++) {
			if(!(octet & 7)) {
				serial_puts("\n0x");
				serial_puthex(octet);
				serial_puts(": ");
			}
			i2c_eeprom_read(octet, 1, &setaddr);
			serial_puthex(setaddr);
			serial_putc(' ');
		}
		serial_puts("\nRTCC UID:");
		for(octet = 0xf0; octet < 0xf8; octet++) {
			if(!(octet & 7)) {
				serial_puts("\n0x");
				serial_puthex(octet);
				serial_puts(": ");
			}
			i2c_eeprom_read(octet, 1, &setaddr);
			serial_puthex(setaddr);
			serial_putc(' ');
		}
		serial_putc('\n');
		break;

	case 'Z':
		break;

	case '\x1b':
	case '-':
	case '<':
	case '.':
		state = STATE_MENU;
		menustate = MENU_MAIN;
		break;

	default:
		unknown_input(ch);
		break;
	}
}

void cmdline_loop(void)
{
	static char ch;
	switch(state) {
	case STATE_INIT:
		serial_puts(header);
		state = STATE_MENU;
		break;

	case STATE_MENU:
		serial_putc('\n');
		switch(menustate) {
		default:
			menustate = MENU_MAIN;
			/* Fallthrough */
		case MENU_MAIN:		serial_puts(menu_main);		break;
		case MENU_NETWORK:	serial_puts(menu_network);	break;
		case MENU_TIME:		serial_puts(menu_time);		break;
		case MENU_MISC:		serial_puts(menu_misc);		break;
		}
		state = STATE_PROMPT;
		break;

	case STATE_PROMPT:
		serial_putc('\n');
		switch(menustate) {
		default:
			/* Go back to a defined state and retry input */
			state = STATE_MENU;
			menustate = MENU_MAIN;
			break;
		case MENU_MAIN:		serial_puts("Main");	break;
		case MENU_NETWORK:	serial_puts("Network");	break;
		case MENU_TIME:		serial_puts("Time");	break;
		case MENU_MISC:		serial_puts("Misc");	break;
		}
		serial_puts(" menu: Enter option: ");
		state = STATE_MENUINPUT;
		break;

	case STATE_MENUINPUT:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch >= 0x20 && ch <= 0x7e)
			serial_putc(ch);
		else
			serial_putc(' ');
		ch = mytoupper(ch);
		serial_putc('\r');
		serial_putc('\n');

		state = STATE_PROMPT;	/* Default next state; save a lot of assignments */

		switch(menustate) {
		default:
			/* Go back to a defined state and retry input */
			state = STATE_MENU;
			menustate = MENU_MAIN;
			break;
		case MENU_MAIN:
			handle_menu_main(ch);
			break;

		case MENU_NETWORK:
			handle_menu_network(ch);
			break;

		case MENU_TIME:
			handle_menu_time(ch);
			break;

		case MENU_MISC:
			handle_menu_misc(ch);
			break;
		}
		break;

	case STATE_OCTET:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch >= '0' && ch <= '9') {
			if(octet < 4) {
				serial_putc(ch);
				mc.ip[octet] *= 10;
				mc.ip[octet] += ch - '0';
			}
		} else if(ch == '.' && octet < 3) {
			serial_putc(ch);
			octet++;
		} else if(ch == '\r' || ch == '\n') {
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_PROMPT;
			switch(setaddr) {
			case SETADDR_NTP:
				if(!(IP_IS_VALID(mc.ip) || IP_IS_MULTICAST(mc.ip) || IP_IS_BROADCAST(mc.ip)))
					serial_puts("Error: Invalid NTP IP address.\n");
				else {
					IP_CP(cfg.ntpaddr, mc.ip);
					if(cfg.ntp_on) {
						/* We need to re-initialize NTP when the server changes */
						ntp_toggle();
						ntp_toggle();
					}
				}
				break;
			case SETADDR_IP:
				if(!IP_IS_VALID(mc.ip) || IP_IS_MULTICAST(mc.ip))
					serial_puts("Error: Invalid source IP address.\n");
				else
					IP_CP(cfg.ipaddr, mc.ip);
				break;
			case SETADDR_GW:
				if(!IP_IS_VALID(mc.ip) || IP_IS_MULTICAST(mc.ip))
					serial_puts("Error: Invalid gateway IP address.\n");
				else
					IP_CP(cfg.gwaddr, mc.ip);
				break;
			case SETADDR_NM:
				if(!IP_IS_VALID_NETMASK(mc.ip))
					serial_puts("Error: Invalid IP netmask.\n");
				else
					IP_CP(cfg.nmaddr, mc.ip);
				break;
			case SETADDR_DNS1:
			case SETADDR_DNS2:
				if(!IP_IS_VALID(mc.ip))
					serial_puts("Error: Invalid DNS IP address.\n");
				else {
					if(setaddr == SETADDR_DNS1)
						IP_CP(cfg.dnsaddr1, mc.ip);
					else
						IP_CP(cfg.dnsaddr2, mc.ip);
				}
				break;
			}
		} else if (ch == '\x1b') {
			/* Escape; cancel */
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_MENU;
		}
		/* Ignore anything else */
		break;

	case STATE_MACOCTET:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch == '\x1b') {
			/* Escape; cancel */
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_MENU;
		}
		ch = mytoupper(ch);
		if(octet < 12) {
			if((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'F')) {
				serial_putc(ch);
				if(ch > '9')
					ch -= '0' + 7;
				else
					ch -= '0';
				if(octet & 1) {
					mc.mac[octet >> 1] |= ch;
					if(octet < 11)
						serial_putc(':');
				} else
					mc.mac[octet >> 1] |= ch << 4;
				octet++;
			}
		}
		if(octet == 12) {
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			if(mc.mac[0] & 1)
				serial_puts("Error: Multicast and broadcast MAC addresses are not allowed.\n");
			else
				MAC_CP(cfg.macaddr, mc.mac);
			state = STATE_PROMPT;
		}
		/* Ignore anything else */
		break;

	case STATE_NUMBER:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch == '\x1b') {
			/* Escape; cancel */
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_MENU;
		} else if(ch >= '0' && ch <= '9') {
			serial_putc(ch);
			mc.port *= 10;
			mc.port += ch - '0';
		} else if(ch == '\r' || ch == '\n') {
			serial_putc(' ');
			switch(setaddr) {
			case SETINT_PORT:
				if(mc.port >= 1 && mc.port <= 65534)
					cfg.port = mc.port;
				else
					serial_puts("Error: Port out of range.\n");
				break;
			case SETINT_MOVETIMEOUT:
				if(mc.port <= 43200)
					cfg.movetimeout = mc.port;
				else
					serial_puts("Error: Move timeout value out of range.\n");
				break;
			}
			state = STATE_PROMPT;
		}
		break;

	case STATE_SIGNUM:
	case STATE_SIGNUMMINUS:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch == '\x1b') {
			/* Escape; cancel */
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_MENU;
		} else if(ch == '-') {
			serial_putc(ch);
			/* We actually don't care where the '-' is put in... */
			state = STATE_SIGNUMMINUS;
		} else if(ch >= '0' && ch <= '9') {
			serial_putc(ch);
			mc.signum *= 10;
			mc.signum += ch - '0';
		} else if(ch == '\r' || ch == '\n') {
			serial_putc(' ');
			if(state == STATE_SIGNUMMINUS)
				mc.signum = -mc.signum;
			switch(setaddr) {
			case SETINT_RTCCCAL:
				if(mc.signum >= -128 && mc.signum <= 127)
					rtcc_cal_write((int8_t)mc.signum);
				else
					serial_puts("Error: RTCC calibration value out of range.\n");
				break;
			case SETINT_TZ:
				if(mc.signum >= -1440 && mc.signum <= 1440)
					cfg.tzoffset = mc.signum;
				else
					serial_puts("Error: Timezone offset value out of range.\n");
				break;
			}
			state = STATE_PROMPT;
		}
		break;

	case STATE_BIGNUMBER:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch == '\x1b') {
			/* Escape; cancel */
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_MENU;
		} else if(ch >= '0' && ch <= '9') {
			serial_putc(ch);
			mc.bignum *= 10;
			mc.bignum += ch - '0';
		} else if(ch == '\r' || ch == '\n') {
			serial_putc(' ');
			/*switch(setaddr) {
			case SETI32_PMDSUBCAT:
				break;
			}*/
			state = STATE_PROMPT;
		}
		break;

	case STATE_HOSTNAME:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch == '\x1b') {
			/* Escape; cancel */
			serial_putc(' ');
			serial_putc('\r');
			serial_putc('\n');
			state = STATE_MENU;
		} else if(octet < MAXHOSTNAME-2 && ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z') || ch == '.' || ch == '-')) {
			serial_putc(ch);
			mc.hostname[octet++] = ch;
			mc.hostname[octet] = 0;
		} else if(ch == '\r' || ch == '\n') {
			serial_putc(' ');
			memcpy(cfg.hostname, mc.hostname, sizeof(cfg.hostname));
			state = STATE_PROMPT;
		}
		break;

	case STATE_CONFIRM_SAVE:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch != 'Y')
			serial_puts("***save canceled***\n");
		else
			config_save();
		/* We don't reach this. The box is reset */
		state = STATE_MENU;
		break;

	case STATE_CONFIRM_SAVERTCC:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch != 'Y')
			serial_puts("***rtcc save canceled***\n");
		else
			rtcc_write_now();
		state = STATE_MENU;
		break;

	case STATE_CONFIRM_FACTORYRESET:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch != 'Y')
			serial_puts("***factory reset canceled***\n");
		else
			config_factoryreset();
		/* We don't reach this. The box is reset */
		state = STATE_MENU;
		break;

	case STATE_CONFIRM_DEVICERESET:
		if(!serial_have_input())
			return;
		ch = serial_getc();
		if(ch != 'Y')
			serial_puts("***device restart canceled***\n");
		else {
			/* Hard reset... */
			__asm
			reset
			__endasm;
		}
		/* We don't reach this. The box is reset */
		state = STATE_MENU;
		break;

	default:
		state = STATE_INIT;
		break;
	}
}
