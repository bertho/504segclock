/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_CONFIG_H
#define __504SEGCLOCK_CONFIG_H

#include <stdint.h>

#ifdef __GNUC__
#define PACKED_FIELDS	__attribute__((packed))
#else
#define PACKED_FIELDS

#if defined(pic18f66j65)
#define FLASH_SIZE	0x18000
#elif defined(pic18f67j60)
#define FLASH_SIZE	0x20000
#else
#define FLASH_SIZE	1
#error "Unsupported processor selected"
#endif
#endif

#ifndef __CONFIG1L
#define __CONFIG1L	(FLASH_SIZE-8)
#define __CONFIG1H	(FLASH_SIZE-7)
#define __CONFIG2L	(FLASH_SIZE-6)
#define __CONFIG2H	(FLASH_SIZE-5)
#define __CONFIG3L	(FLASH_SIZE-4)
#define __CONFIG3H	(FLASH_SIZE-3)
#endif
#ifndef _DEVIDL
#define _DEVIDL	0x3FFFFE
#define _DEVIDH	0x3FFFFF
#endif

#ifdef HAVE_BOOTLOADER
#define MAIN_ADDR_OFFSET		0x00800
#else
#define MAIN_ADDR_OFFSET		0x00000
#endif

#define MAIN_ADDR_START			MAIN_ADDR_OFFSET
#define MAIN_ADDR_ISRH			(MAIN_ADDR_OFFSET + 0x00008)
#define MAIN_ADDR_ISRL			(MAIN_ADDR_OFFSET + 0x00018)
#define FIRMWAREID_ADDR_START		(MAIN_ADDR_START + 0x00020)	/* First 32 bytes are shifted vectors */

#define MAXHOSTNAME			64	/* This includes the terminating '\0' */
#define SYSCONFIG_SIZE			128
#define FLASH_WR_SIZE			64

typedef struct __firmwareid_t {
	uint8_t		versionid[16];
	uint8_t		buildid[16];
	uint8_t		text[96];
} firmwareid_t;

#ifndef __GNUC__
extern __code firmwareid_t __at FIRMWAREID_ADDR_START firmwareid;
#endif

#define SYSCONFIG_MAGIC_LO	0x55
#define SYSCONFIG_MAGIC_HI	0xaa

/* Persistent config parameters */
typedef struct __sysconfig_t {
	uint8_t		magic[2];		/*   0 Magic number {0x55, 0xaa} */
	uint8_t		macaddr[6];		/*   2 Source MAC address */
	uint8_t		ipaddr[4];		/*   8 Source IP address */
	uint8_t		nmaddr[4];		/*  12 IP netmask */
	uint8_t		gwaddr[4];		/*  16 Gateway IP address */
	uint8_t		ntpaddr[4];		/*  20 NTP server address (BC, MC or UC) */
	uint16_t	port;			/*  24 Listen UDP port */
	int16_t		tzoffset;		/*  26 Offset from UTC in minutes */
	char		hostname[MAXHOSTNAME];	/*  28 Manually set hostname */
	union {
		struct {			/* 92 */
			uint8_t		dhcp_on:1;		/* Use DHCP for IP config */
			uint8_t		dhcp_hostname:1;	/* Get the hostname from DHCP */
			uint8_t		ntp_on:1;		/* Use NTP for time */
			uint8_t		ntp_mcpeer:1;		/* Keep using MC NTP for time */
			uint8_t		ntp_to_rtcc:1;		/* (re-)Sync rtcc to NTP */
			uint8_t		ntp_use_dhcp:1;		/* Get the NTP server from DHCP and use it if available */
			uint8_t		res_flag:2;		/* Reserved */
		} PACKED_FIELDS;
		uint8_t		flags;
	};
	uint8_t		orientation;				/*  93 Orientation of display */
	uint16_t	movetimeout;				/*  94 Timeout for display blanking */
	uint8_t		dnsaddr1[4];				/*  96 Static DNS server */
	uint8_t		dnsaddr2[4];				/* 100 Static DNS server */
	int32_t		drift;					/* 104 Last drift from NTP calculation */
	uint8_t		reserved[SYSCONFIG_SIZE-108];		/* 108 Reserved fill to SYSCONFIG_SIZE */
} sysconfig_t;

extern sysconfig_t cfg;		/* Running configuration */
extern uint8_t serialnr[8];

uint8_t config_load(void);
void config_save(void);
void config_factoryreset(void);
void config_dump_all(void);

#endif
