/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <pic18fregs.h>
#include <config.h>
#include <io.h>
#include <clock.h>
#include <event.h>

static uint8_t buttons;
static uint8_t oldbuttons;
uint8_t buttonstate;

void io_init(void)
{
	PORTD |= 0x07;			/* LEDs 1, 0, 2 active low */
	TRISDbits.TRISD0 = 0;		/* LED1 output */
	TRISDbits.TRISD1 = 0;		/* LED0 output */
	TRISDbits.TRISD2 = 0;		/* LED2 output */

	PORTB &= 0xf0;			/* Button inputs */
	PORTB |= 0x10;			/* LED 3 active low */
	TRISBbits.TRISB0 = 1;		/* all button inputs */
	TRISBbits.TRISB1 = 1;
	TRISBbits.TRISB2 = 1;
	TRISBbits.TRISB3 = 1;
	TRISBbits.TRISB4 = 0;		/* LED3 output */

	PORTAbits.RA4 = 0;		/* Set boards into reset-mode */
	TRISAbits.TRISA4 = 0;		/* CLR to boards is an output */

	/* Define unused pins (port E is set in spi.c, port F is set in move.c) */
	PORTAbits.RA3 = 0;
	PORTAbits.RA5 = 0;
	TRISAbits.TRISA3 = 0;
	TRISAbits.TRISA5 = 0;

	PORTGbits.RG4 = 0;
	TRISGbits.TRISG4 = 0;

	INTCON2bits.RBPU = 0;		/* Enable Port B pull-ups */
	clock_timer_set(TIMER_BUTTON, BUTTON_TICKS);

	buttons = oldbuttons = BUTTONMASK;
	buttonstate = 0;
}

/*
 * State	n	n+1	n+2	n+3
 * oldbuttons	0	1	1	0
 * PORTB	1	1	0	0
 * change	1	0	1	0
 * event	-	rel	-	press
 */
void io_handle_timer_button(void)
{
	clock_timer_set(TIMER_BUTTON, BUTTON_TICKS);
	oldbuttons = buttons;
	buttons = PORTB & BUTTONMASK;
	if((oldbuttons & buttons & BUTTON_SET) && !(buttonstate & BUTTON_SET)) {
		buttonstate |= BUTTON_SET;
		event_handle(EV_RELEASE_SET);
	} else if(!((oldbuttons | buttons) & BUTTON_SET) && (buttonstate & BUTTON_SET)) {
		buttonstate &= ~BUTTON_SET;
		event_handle(EV_PRESS_SET);
	}

	if((oldbuttons & buttons & BUTTON_UP) && !(buttonstate & BUTTON_UP)) {
		buttonstate |= BUTTON_UP;
		event_handle(EV_RELEASE_UP);
	} else if(!((oldbuttons | buttons) & BUTTON_UP) && (buttonstate & BUTTON_UP)) {
		buttonstate &= ~BUTTON_UP;
		event_handle(EV_PRESS_UP);
	}

	if((oldbuttons & buttons & BUTTON_DOWN) && !(buttonstate & BUTTON_DOWN)) {
		buttonstate |= BUTTON_DOWN;
		event_handle(EV_RELEASE_DOWN);
	} else if(!((oldbuttons | buttons) & BUTTON_DOWN) && (buttonstate & BUTTON_DOWN)) {
		buttonstate &= ~BUTTON_DOWN;
		event_handle(EV_PRESS_DOWN);
	}

	if((oldbuttons & buttons & BUTTON_SELECT) && !(buttonstate & BUTTON_SELECT)) {
		buttonstate |= BUTTON_SELECT;
		event_handle(EV_RELEASE_SELECT);
	} else if(!((oldbuttons | buttons) & BUTTON_SELECT) && (buttonstate & BUTTON_SELECT)) {
		buttonstate &= ~BUTTON_SELECT;
		event_handle(EV_PRESS_SELECT);
	}
}
