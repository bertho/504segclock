/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_CLOCK_H
#define __504SEGCLOCK_CLOCK_H

#include <irq.h>

enum {
	TIMER_BUTTON = 0,
	TIMER_ADC,
	TIMER_ADC_BC,
	TIMER_SPI,
	TIMER_DISPLAY,
	TIMER_GREEN,
	TIMER_ARP,
	TIMER_WHEEL,
	TIMER_ETHTX,
	TIMER_DHCP,
	TIMER_I2C,
	TIMER_NTP,
	TIMER_MOVE,
	TIMER_REMOTE,
	TIMER_CONFIG,
	MAXTIMERS
};

enum {
	TIMER_IDLE = 0,
	TIMER_RUNNING,
	TIMER_EXPIRED
};

typedef struct __clock_timer_t {
	uint8_t		state;
	uint16_t	cnt;
} clock_timer_t;

typedef union __entropy_t {
	uint8_t		bytes[4];
	uint16_t	words[2];
	uint32_t	dword;
} entropy_t;

typedef struct __timeparts_t {
	uint8_t		sec;
	uint8_t		min;
	uint8_t		hour;
	uint8_t		day;
	uint8_t		mon;
	uint8_t		mday;
	uint16_t	year;
	/*uint16_t	yday;*/
	uint8_t		wday;
	uint16_t	msec;
} timeparts_t;

extern volatile clock_timer_t clock_timers[MAXTIMERS];
extern volatile uint32_t clock_uptime;
extern volatile uint32_t clock_secs;
extern volatile uint16_t clock_msecs;
extern volatile entropy_t clock_entropy;
extern timeparts_t clock_timeparts;

void clock_init(void);
void clock_isr(void);
void clock_localtime(uint8_t useutc);
void clock_mktime(void);

#define clock_timer_set(id,ms)		do { cli(); clock_timers[id].cnt = ms; clock_timers[id].state = TIMER_RUNNING; sti(); } while(0)
#define clock_timer_set_isr(id,ms)	do { clock_timers[id].cnt = ms; clock_timers[id].state = TIMER_RUNNING; } while(0)
#define clock_timer_expired(id)		(clock_timers[id].state == TIMER_EXPIRED)
#define clock_timer_idle(id)		(clock_timers[id].state == TIMER_IDLE)
#define clock_timer_clear(id)		do { cli(); clock_timers[id].state = TIMER_IDLE; sti(); } while(0)
#define clock_timer_clear_isr(id)	do { clock_timers[id].state = TIMER_IDLE; } while(0)

#endif
