/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_ICMP_H
#define __504SEGCLOCK_ICMP_H

#define ICMP_ECHO_REPLY		0
#define ICMP_UNREACHABLE	3
#define ICMP_REDIRECT		5
#define ICMP_ECHO_REQUEST	8

typedef struct __icmphdr_t {
	uint8_t		icmptype;
	uint8_t		icmpcode;
	uint16_t	checksum;
	union {
		struct {
			uint16_t	id;
			uint16_t	seq;
		};
		uint32_t	rest;
	};
} icmphdr_t;

uint8_t icmp_handle_rxpacket(uint16_t pktlen);

#endif

