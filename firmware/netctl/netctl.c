#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <getopt.h>
#include <errno.h>

#include <display.h>
#include <config.h>
#include <netctrl.h>

#define NELEM(x)	(sizeof(x) / sizeof(*(x)))

/*
 * Segment arrangement each quadrant:
 *
 *  -     -     -     -     -     -     -     -     -
 * | |   | |   | |   | |   | |   | |   | |   | |   | |
 *  -     -     -     -     -     -     -     -     -   Outer
 * | |   | |   | |   | |   | |   | |   | |   | |   | |
 *  -     -     -     -     -     -     -     -     -
 *
 *     -        -        -        -        -        -
 *    | |      | |      | |      | |      | |      | |
 *     -        -        -        -        -        -   Middle
 *    | |      | |      | |      | |      | |      | |
 *     -        -        -        -        -        -
 *
 *              -                 -                 -
 *             | |               | |               | |
 *              -                 -                 -   Inner
 *             | |               | |               | |
 *              -                 -                 -
 *
 *
 */


#define MOVE_WAIT	5

sysconfig_t cfg;

static volatile int quit = 0;

lineardisplay_t displays;

static uint8_t light = 255;
static int light_enable = 0;
static uint8_t move;

static const uint8_t digits[] = {
	0x3f,		/* 0 00111111 */
	0x06,		/* 1 00000110 */
	0x5b,		/* 2 01011011 */
	0x4f,		/* 3 01001111 */
	0x66,		/* 4 01100110 */
	0x6d,		/* 5 01101101 */
	0x7d,		/* 6 01111101 */
	0x07,		/* 7 00000111 */
	0x7f,		/* 8 01111111 */
	0x6f,		/* 9 01101111 */
	0x77,		/* A 01110111 */
	0x7c,		/* B 01111100 */
	0x39,		/* C 00111001 */
	0x5e,		/* D 01011110 */
	0x79,		/* E 01111001 */
	0x71,		/* F 01110001 */
};

static const uint8_t section_remap[4] = {
	1, 0, 3, 2,
//	2, 3, 0, 1,
};

static netctrl_t pkt;
static int sendsock;
static int recvsock;
static struct sockaddr_in sendsain;
static struct sockaddr_in recvsain;
static int pfd[2];


/*
 * Fill the linear rings into he packet, taking account
 * for any rotation of the rings and remapping of the
 * sections.
 */
static void fill_packet(void)
{
	memcpy(pkt.sections, displays.ringouter, NALL*sizeof(displays.ringouter[0]));
/*
	int i;
	for(i = 0; i < NOUTER; i++) {
		int x = (i + orientation * 3) % NOUTER;
		pkt.sections[section_remap[x / NDISPOUTER]].outer[x % NDISPOUTER] = displays.ringouter[NOUTER - i - 1];
	}
	for(i = 0; i < NMIDDLE; i++) {
		int x = (i + orientation * 2) % NMIDDLE;
		pkt.sections[section_remap[x / NDISPMIDDLE]].middle[x % NDISPMIDDLE] = displays.ringmiddle[NMIDDLE - i - 1];
	}
	for(i = 0; i < NINNER; i++) {
		int x = (i + orientation * 1) % NINNER;
		pkt.sections[section_remap[x / NDISPINNER]].inner[x % NDISPINNER] = displays.ringinner[NINNER - i - 1];
	}
*/
}


static void set_segment_digit(display_t *d, uint8_t n, uint8_t val)
{
	n = digits[n & 0x0f];
	if(n & 0x01)	d->a = val;
	if(n & 0x02)	d->b = val;
	if(n & 0x04)	d->c = val;
	if(n & 0x08)	d->d = val;
	if(n & 0x10)	d->e = val;
	if(n & 0x20)	d->f = val;
	if(n & 0x40)	d->g = val;
	/*if(n & 0x80)	d->p = val;*/
}

static void show_clock(struct timeval *tv, uint8_t val)
{
	struct tm *tmp;
	tmp = localtime(&tv->tv_sec);

	if(tmp->tm_hour >= 10) {
		set_segment_digit(&displays.ringinner[inner_idx(NINNER*(tmp->tm_hour%12) / 12 - 1)], tmp->tm_hour / 10, val);
		set_segment_digit(&displays.ringinner[inner_idx(NINNER*(tmp->tm_hour%12) / 12 - 0)], tmp->tm_hour % 10, val);
	} else
		set_segment_digit(&displays.ringinner[inner_idx(NINNER*tmp->tm_hour / 12 - 1)], tmp->tm_hour % 10, val);

	set_segment_digit(&displays.ringmiddle[middle_idx(NMIDDLE*tmp->tm_min / 60 - 1)], tmp->tm_min / 10, val);
	set_segment_digit(&displays.ringmiddle[middle_idx(NMIDDLE*tmp->tm_min / 60 - 0)], tmp->tm_min % 10, val);

	set_segment_digit(&displays.ringouter[outer_idx(NOUTER*tmp->tm_sec / 60 - 1)], tmp->tm_sec / 10, val);
	set_segment_digit(&displays.ringouter[outer_idx(NOUTER*tmp->tm_sec / 60 - 0)], tmp->tm_sec % 10, val);

	displays.ringinner[inner_idx(NINNER*tv->tv_usec / 1000000)].p = val;
}

static const int mdays[] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

static void show_date(struct timeval *tv, uint8_t val)
{
	struct tm *tmp;
	tmp = localtime(&tv->tv_sec);

	tmp->tm_year += 1900;
	set_segment_digit(&displays.ringinner[inner_idx(-2)], tmp->tm_year / 1000, val);
	set_segment_digit(&displays.ringinner[inner_idx(-1)], (tmp->tm_year % 1000) / 100, val);
	set_segment_digit(&displays.ringinner[inner_idx( 0)], (tmp->tm_year % 100) / 10, val);
	set_segment_digit(&displays.ringinner[inner_idx( 1)], tmp->tm_year % 10, val);

	set_segment_digit(&displays.ringmiddle[middle_idx(NMIDDLE*tmp->tm_mon / 12 - 1)], (tmp->tm_mon+1) / 10, val);
	set_segment_digit(&displays.ringmiddle[middle_idx(NMIDDLE*tmp->tm_mon / 12 - 0)], (tmp->tm_mon+1) % 10, val);

	set_segment_digit(&displays.ringouter[outer_idx(NOUTER*(tmp->tm_mday-1) / mdays[tmp->tm_mon] - 1)], tmp->tm_mday / 10, val);
	set_segment_digit(&displays.ringouter[outer_idx(NOUTER*(tmp->tm_mday-1) / mdays[tmp->tm_mon] - 0)], tmp->tm_mday % 10, val);

	displays.ringinner[inner_idx(NINNER*tv->tv_usec / 1000000)].p = val;
}

static void show_clock2(struct timeval *tv, uint8_t val)
{
	struct tm *tmp;
	int idx;
	int rest;
	int i;
	display_t *d;
//static time_t tt;
//tmp = localtime(&tt);
//tt+=10;
	tmp = localtime(&tv->tv_sec);

	if(val < 32)
		val = 32;
	idx = 256 * NOUTER*2 * (tmp->tm_sec * 1000 + tv->tv_usec / 1000) / (60*1000);
	rest = idx % 256;
	idx /= 256;
	d = &displays.ringouter[outer_idx(idx/2-1)];
	if(idx & 1)
		d->b = d->c = val * (255 - rest) / 255;
	else
		d->e = d->f = val * (255 - rest) / 255;
	d = &displays.ringouter[outer_idx((idx+1)/2-1)];
	if(idx & 1)
		d->e = d->f = val * rest / 255;
	else
		d->b = d->c = val * rest / 255;

	idx = 256 * NMIDDLE*2 * (tmp->tm_min * 60 + tmp->tm_sec) / (60*60);
	rest = idx % 256;
	idx /= 256;
	d = &displays.ringmiddle[middle_idx(idx/2-1)];
	if(idx & 1)
		d->b = d->c = val * (255 - rest) / 255;
	else
		d->e = d->f = val * (255 - rest) / 255;
	d = &displays.ringmiddle[middle_idx((idx+1)/2-1)];
	if(idx & 1)
		d->e = d->f = val * rest / 255;
	else
		d->b = d->c = val * rest / 255;

	idx = 256 * NINNER*2 * ((tmp->tm_hour % 12) * 60 + tmp->tm_min) / (12*60);
	rest = idx % 256;
	idx /= 256;
	d = &displays.ringinner[inner_idx(idx/2-1)];
	if(idx & 1)
		d->b = d->c = val * (255 - rest) / 255;
	else
		d->e = d->f = val * (255 - rest) / 255;
	d = &displays.ringinner[inner_idx((idx+1)/2-1)];
	if(idx & 1)
		d->e = d->f = val * rest / 255;
	else
		d->b = d->c = val * rest / 255;

	idx = NINNER*tv->tv_usec / 1000000;
	displays.ringinner[inner_idx(idx)].p = val;
	for(i = 1; i < (NINNER-1)/2; i++) {
		displays.ringinner[inner_idx(idx+i)].p = val*((NINNER-1)/2-1-i)/((NINNER-1)/2);
		displays.ringinner[inner_idx(idx-i)].p = val*((NINNER-1)/2-1-i)/((NINNER-1)/2);
	}
}

static void set_sequential(int i)
{
	int lv = light_enable ? light : 255;
	set_segment_digit(&displays.ringouter[i % NALL], i, lv);
}

#if 0
static uint8_t lightcurve[] = {
	0, 31, 50, 63, 74, 82, 89, 95, 101, 105, 110, 114, 117, 121, 124, 127,
	130, 132, 135, 137, 140, 142, 144, 146, 148, 149, 151, 153, 154, 156, 157, 159,
	160, 162, 163, 164, 166, 167, 168, 169, 170, 171, 172, 174, 175, 176, 177, 178,
	178, 179, 180, 181, 182, 183, 184, 185, 185, 186, 187, 188, 189, 189, 190, 191,
	191, 192, 193, 194, 194, 195, 196, 196, 197, 197, 198, 199, 199, 200, 200, 201,
	202, 202, 203, 203, 204, 204, 205, 205, 206, 206, 207, 207, 208, 208, 209, 209,
	210, 210, 211, 211, 212, 212, 213, 213, 214, 214, 214, 215, 215, 216, 216, 217,
	217, 217, 218, 218, 219, 219, 219, 220, 220, 220, 221, 221, 222, 222, 222, 223,
	223, 223, 224, 224, 224, 225, 225, 225, 226, 226, 226, 227, 227, 227, 228, 228,
	228, 229, 229, 229, 230, 230, 230, 231, 231, 231, 231, 232, 232, 232, 233, 233,
	233, 233, 234, 234, 234, 235, 235, 235, 235, 236, 236, 236, 237, 237, 237, 237,
	238, 238, 238, 238, 239, 239, 239, 239, 240, 240, 240, 240, 241, 241, 241, 241,
	242, 242, 242, 242, 242, 243, 243, 243, 243, 244, 244, 244, 244, 245, 245, 245,
	245, 245, 246, 246, 246, 246, 247, 247, 247, 247, 247, 248, 248, 248, 248, 248,
	249, 249, 249, 249, 249, 250, 250, 250, 250, 250, 251, 251, 251, 251, 251, 252,
	252, 252, 252, 252, 253, 253, 253, 253, 253, 253, 254, 254, 254, 254, 254, 255,
};
static uint8_t lightcurve[] = {
	0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5,
	5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6,
	6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9, 9,
	9, 10, 10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15,
	16, 17, 17, 18, 18, 19, 20, 21, 21, 22, 23, 24, 25, 26, 27, 28,
	29, 30, 31, 32, 34, 35, 36, 38, 39, 40, 42, 44, 45, 47, 49, 50,
	52, 54, 56, 58, 60, 62, 64, 67, 69, 71, 74, 76, 79, 81, 84, 86,
	89, 92, 94, 97, 100, 103, 106, 109, 112, 115, 118, 121, 124, 127, 130, 132,
	135, 138, 141, 144, 147, 150, 153, 156, 159, 162, 165, 167, 170, 173, 175, 178,
	180, 183, 185, 188, 190, 192, 195, 197, 199, 201, 203, 205, 207, 209, 210, 212,
	214, 215, 217, 219, 220, 221, 223, 224, 225, 227, 228, 229, 230, 231, 232, 233,
	234, 235, 236, 237, 238, 238, 239, 240, 241, 241, 242, 242, 243, 244, 244, 245,
	245, 246, 246, 246, 247, 247, 248, 248, 248, 249, 249, 249, 250, 250, 250, 250,
	251, 251, 251, 251, 251, 252, 252, 252, 252, 252, 252, 253, 253, 253, 253, 253,
	253, 253, 253, 253, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254, 254,
	254, 254, 254, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
};
#endif
static uint8_t lightcurve[] = {
	0, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
	4, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 7, 7, 7, 7,
	7, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12,
	12, 13, 13, 14, 14, 14, 15, 15, 16, 16, 16, 17, 17, 18, 18, 19,
	19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25, 25, 26, 26, 27, 28,
	28, 29, 29, 30, 31, 31, 32, 33, 33, 34, 35, 35, 36, 37, 38, 38,
	39, 40, 41, 41, 42, 43, 44, 44, 45, 46, 47, 48, 49, 49, 50, 51,
	52, 53, 54, 55, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66,
	67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82,
	84, 85, 86, 87, 88, 89, 90, 92, 93, 94, 95, 96, 97, 99, 100, 101,
	102, 104, 105, 106, 107, 109, 110, 111, 112, 114, 115, 116, 118, 119, 120, 122,
	123, 124, 126, 127, 129, 130, 131, 133, 134, 136, 137, 139, 140, 141, 143, 144,
	146, 147, 149, 150, 152, 153, 155, 156, 158, 159, 161, 163, 164, 166, 167, 169,
	171, 172, 174, 175, 177, 179, 180, 182, 184, 185, 187, 189, 190, 192, 194, 196,
	197, 199, 201, 202, 204, 206, 208, 210, 211, 213, 215, 217, 219, 220, 222, 224,
	226, 228, 230, 231, 233, 235, 237, 239, 241, 243, 245, 247, 249, 251, 253, 255,
};

#define lcval(x)	(lightcurve[(x) % (sizeof(lightcurve)/sizeof(lightcurve[0]))])
#define LCSTEP(x)	(3*(x))
static void show_bulge(int state)
{
	int i;
	state *= 4;
	for(i = 0; i < NINNER; i++) {
		displays.ringinner[i].d = lcval(state+LCSTEP(0));
		displays.ringinner[i].c = displays.ringinner[i].e = lcval(state+LCSTEP(1));
		displays.ringinner[i].g = lcval(state+LCSTEP(2));
		displays.ringinner[i].b = displays.ringinner[i].f = lcval(state+LCSTEP(3));
		displays.ringinner[i].a = lcval(state+LCSTEP(4));
		displays.ringinner[i].p = 0;
		state += LCSTEP(1);
	}
	for(i = 0; i < NMIDDLE; i++) {
		displays.ringmiddle[i].d = lcval(state+LCSTEP(0));
		displays.ringmiddle[i].c = displays.ringmiddle[i].e = lcval(state+LCSTEP(1));
		displays.ringmiddle[i].g = lcval(state+LCSTEP(2));
		displays.ringmiddle[i].b = displays.ringmiddle[i].f = lcval(state+LCSTEP(3));
		displays.ringmiddle[i].a = lcval(state+LCSTEP(4));
		displays.ringmiddle[i].p = 0;
		state += LCSTEP(1);
	}
	for(i = 0; i < NOUTER; i++) {
		displays.ringouter[i].d = lcval(state+LCSTEP(0));
		displays.ringouter[i].c = displays.ringouter[i].e = lcval(state+LCSTEP(1));
		displays.ringouter[i].g = lcval(state+LCSTEP(2));
		displays.ringouter[i].b = displays.ringouter[i].f = lcval(state+LCSTEP(3));
		displays.ringouter[i].a = lcval(state+LCSTEP(4));
		displays.ringouter[i].p = 0;
		state += LCSTEP(1);
	}
}

static void show_fade(int state)
{
	if(state & 0x100)
		memset(&displays, lightcurve[(0xff - state) & 0xff], sizeof(displays));
	else
		memset(&displays, lightcurve[state & 0xff], sizeof(displays));
}

static void set_ring(display_t *disp, int ring, int v)
{
	switch(ring) {
	case 0:
		disp->a = v;
		break;
	case 1:
		disp->b = v;
		disp->f = v;
		break;
	case 2:
		disp->g = v;
		break;
	case 3:
		disp->c = v;
		disp->e = v;
		break;
	case 4:
		disp->d = v;
		break;
	case 5:
		disp->p = v;
		break;
	}
}

static void show_rings(int i)
{
	int seg;
	int s, d;
	int lv = light_enable ? light : 255;
	for(s = 0; s < NSECTIONS; s++) {
		i %= 2*18;	/* 3 * 6 rings */

		if(i >= 18)
			i = 35 - i;

		seg = i % 6;
		switch(i / 6) {
		case 0:
			for(d = 0; d < NOUTER; d++)
				set_ring(&displays.ringouter[d], seg, lv);
			break;
		case 1:
			for(d = 0; d < NMIDDLE; d++)
				set_ring(&displays.ringmiddle[d], seg, lv);
			break;
		case 2:
			for(d = 0; d < NINNER; d++)
				set_ring(&displays.ringinner[d], seg, lv);
			break;
		}
	}
}

static void show_spiral(int i, int mode)
{
	int j;
	for(j = 0; j < sizeof(displays)/sizeof(display_t); j++) {
		set_ring(&(&displays.ringouter[0])[j], mode ? ((j+i) % 6) : (5-((j+i) % 6)), ((j+i)*3)%256);
	}
}

static void show_chase(int idx)
{
	int i;
	for(i = 0; i < NELEM(displays.ringinner) * 4 - 2; i++) {
		display_t *d = &displays.ringinner[inner_idx((i + idx)/4)];
		int val = i*255/(NELEM(displays.ringinner) * 4);
		switch((i+idx) % 4) {
		case 3: d->p = val;
			break;
		case 2: d->b = d->c = val;
			break;
		case 1: d->a = d->d = d->g = val;
			break;
		case 0: d->e = d->f = val;
			break;
		}
	}
	for(i = 0; i < NELEM(displays.ringmiddle) * 4 - 2; i++) {
		display_t *d = &displays.ringmiddle[middle_idx((i + idx)/4)];
		int val = i*255/(NELEM(displays.ringmiddle) * 4);
		switch((i+idx) % 4) {
		case 3: d->p = val;
			break;
		case 2: d->b = d->c = val;
			break;
		case 1: d->a = d->d = d->g = val;
			break;
		case 0: d->e = d->f = val;
			break;
		}
	}
	for(i = 0; i < NELEM(displays.ringouter) * 4 - 2; i++) {
		display_t *d = &displays.ringouter[outer_idx((i + idx)/4)];
		int val = i*255/(NELEM(displays.ringouter) * 4);
		switch((i+idx) % 4) {
		case 3: d->p = val;
			break;
		case 2: d->b = d->c = val;
			break;
		case 1: d->a = d->d = d->g = val;
			break;
		case 0: d->e = d->f = val;
			break;
		}
	}
}

static void show_wave(int idx)
{
	int i;
	int lv = light_enable ? light : 255;
	for(i = 0; i < NALL; i++) {
		display_t *d = &displays.ringouter[i];
		int tag = (i+idx/2)%4;
		if(idx & 1) {
			switch(tag) {
			case 0: d->a = d->b = lv; break;
			case 1: d->c = d->g = lv; break;
			case 2: d->c = d->d = lv; break;
			case 3: d->b = d->g = lv; break;
			}
		} else {
			switch(tag) {
			case 0: d->a = d->f = lv; break;
			case 1: d->f = d->g = lv; break;
			case 2: d->d = d->e = lv; break;
			case 3: d->e = d->g = lv; break;
			}
		}
	}
}

static int sendpkt(size_t len)
{
	if(-1 == sendto(sendsock, &pkt, len, 0, (struct sockaddr *)&sendsain, sizeof(sendsain))) {
		perror("sendto");
		return -1;
	}
	return 0;
}

static void sigint(int sig)
{
	(void)sig;
	quit = 1;
}

static void sigalrm(int sig)
{
	char ch = 0;
	(void)sig;
retry:
	if(-1 == write(pfd[1], &ch, 1) && errno == EINTR)
		goto retry;
}

static const char usage_str[] =
	"Usage: netctl [option] <host>\n"
	"Option:\n"
	"  -a|--auto delay        Autoforward mode after 'delay' seconds\n"
	"  -c|--config            Read and print config\n"
	"  -d|--delay delay       Set delay between updates to 'delay' microseconds\n"
	"  -h|--help              This help message\n"
	"  -l|--light             Enable light sensor dependecies\n"
	"  -m|--mode mode         Set mode of operation with 'mode' one of\n"
	"                          - clock\n"
	"                          - time (same as clock)\n"
	"                          - clock2\n"
	"                          - time2 (same as clock2)\n"
	"                          - date\n"
	"                          - chase\n"
	"                          - rings\n"
	"                          - spiral\n"
	"                          - spiral2\n"
	"                          - sequential\n"
	"                          - random\n"
	"                          - wave\n"
	"                          - bulge\n"
	"                          - fade\n"
	"  -o|--move              Enable shifting mode on move\n"
	"  -p|--port port         UDP port number to target\n"
	"  -t|--orientation val   Rotate display (0..11)\n"
	"  -w|--wait secs         Wait at least 'secs' between movement induced mode-change\n"
	;

static const struct option optlong[] = {
	{ "auto",	1,	NULL,	'a' },
	{ "config",	0,	NULL,	'c' },
	{ "delay",	1,	NULL,	'd' },
	{ "help",	0,	NULL,	'h' },
	{ "light",	0,	NULL,	'l' },
	{ "mode",	1,	NULL,	'm' },
	{ "move",	0,	NULL,	'o' },
	{ "port",	1,	NULL,	'p' },
	{ "orientation",	1,	NULL,	'r' },
	{ "wait",	1,	NULL,	'w' },
};

enum {
	MODE_CHASE,
	MODE_CLOCK,
	MODE_DATE,
	MODE_CLOCK2,
	MODE_RINGS,
	MODE_SPIRAL,
	MODE_SPIRAL2,
	MODE_SEQUENTIAL,
	MODE_RANDOM,
	MODE_WAVE,
	MODE_BULGE,
	MODE_FADE,
	MODE_LAST,
};

#define reset_automovetime()	do { automoveleft = automove * 1000000; } while(0)

int main(int argc, char *argv[])
{
	int i = 0;
	unsigned short port = 0xc10c;
	int optc;
	int lose = 0;
	char *host;
	int mode = MODE_CHASE;
	int delay = 50000;
	struct itimerval tmr;
	int enable_modemove = 0;
	struct timeval lastmove;
	int move_wait = MOVE_WAIT;
	int automove = 0;
	int automoveleft;
	int readcfg = 0;

	cfg.orientation = 6;
	gettimeofday(&lastmove, NULL);

	signal(SIGINT, sigint);
	signal(SIGTERM, sigint);
	signal(SIGHUP, sigint);
	signal(SIGQUIT, sigint);
	signal(SIGALRM, sigalrm);

	while(EOF != (optc = getopt_long(argc, argv, "a:cd:hlm:p:or:w:", optlong, NULL))) {
		switch(optc) {
		case 'a':
			automove = strtol(optarg, NULL, 0);
			if(automove < 0 || automove > 1800) {
				printf("Auto mode change delay out of range\n");
				lose++;
			}
			break;

		case 'c':
			readcfg = 1;
			break;

		case 'd':
			delay = strtol(optarg, NULL, 0);
			if(delay < 10000 || delay > 1000000000) {
				printf("Delay out of range\n");
				lose++;
			}
			break;

		case 'h':
			printf("%s", usage_str);
			lose++;
			break;

		case 'l':
			light_enable = 1;
			break;

		case 'm':
			if(!strcasecmp(optarg, "chase"))
				mode = MODE_CHASE;
			else if(!strcasecmp(optarg, "clock") || !strcasecmp(optarg, "time"))
				mode = MODE_CLOCK;
			else if(!strcasecmp(optarg, "date"))
				mode = MODE_DATE;
			else if(!strcasecmp(optarg, "clock2") || !strcasecmp(optarg, "time2"))
				mode = MODE_CLOCK2;
			else if(!strcasecmp(optarg, "rings"))
				mode = MODE_RINGS;
			else if(!strcasecmp(optarg, "spiral"))
				mode = MODE_SPIRAL;
			else if(!strcasecmp(optarg, "spiral2"))
				mode = MODE_SPIRAL2;
			else if(!strcasecmp(optarg, "sequential"))
				mode = MODE_SEQUENTIAL;
			else if(!strcasecmp(optarg, "random"))
				mode = MODE_RANDOM;
			else if(!strcasecmp(optarg, "wave"))
				mode = MODE_WAVE;
			else if(!strcasecmp(optarg, "bulge"))
				mode = MODE_BULGE;
			else if(!strcasecmp(optarg, "fade"))
				mode = MODE_FADE;
			else {
				printf("Unknown mode '%s'\n", optarg);
				lose++;
			}
			break;

		case 'o':
			enable_modemove = 1;
			break;

		case 'p':
			port = (unsigned short)strtoul(optarg, NULL, 0);
			if(port < 1 || port > 65534) {
				printf("Port out of range\n");
				lose++;
			}

		case 'r':
			cfg.orientation = strtol(optarg, NULL, 0);
			cfg.orientation %= 12;
			if(cfg.orientation < 0) {
				printf("Rotate out of range\n");
				lose++;
			}
			break;

		case 'w':
			move_wait = strtol(optarg, NULL, 0);
			if(move_wait < 1 || move_wait > 3600) {
				printf("Move wait out of range\n");
				lose++;
			}
			break;

		default:
			lose++;
			break;
		}
	}

	if(lose)
		return 1;

	if(optind >= argc) {
		printf("Missing host\n");
		return 1;
	}
	host = argv[optind];

	if(-1 == pipe(pfd)) {
		perror("pipe");
		return 1;
	}

	tmr.it_interval.tv_sec = tmr.it_value.tv_sec = delay / 1000000;
	tmr.it_interval.tv_usec = tmr.it_value.tv_usec = delay % 1000000;
	if(-1 == setitimer(ITIMER_REAL, &tmr, NULL)) {
		perror("setitimer");
		return 1;
	}

	if(-1 == (sendsock = socket(AF_INET, SOCK_DGRAM, 0))) {
		perror("socket send");
		return -1;
	}
	memset(&sendsain, 0, sizeof(sendsain));
	sendsain.sin_family = AF_INET;
	if(-1 == bind(sendsock, (struct sockaddr *)&sendsain, sizeof(sendsain)))
		perror("bind send");
	sendsain.sin_addr.s_addr = inet_addr(host);
	sendsain.sin_port = htons(port);

	if(-1 == (recvsock = socket(AF_INET, SOCK_DGRAM, 0))) {
		perror("socket recv");
		return -1;
	}
	memset(&recvsain, 0, sizeof(recvsain));
	recvsain.sin_family = AF_INET;
	recvsain.sin_addr.s_addr = INADDR_BROADCAST;
	recvsain.sin_port = htons(port);
	if(-1 == bind(recvsock, (struct sockaddr *)&recvsain, sizeof(recvsain)))
		perror("bind recv");


	if(readcfg) {
		fd_set rfds;
		struct timeval tv;
		int err;
		ssize_t plen;
		char hname[MAXHOSTNAME+1];
		int tries;

		/* Need to retry at least twice because the an ARP entry must become valid */
		for(tries = 0; tries < 3; tries++) {
			pkt.cmd = htons(NETCMD_GETCFG);
			sendpkt(sizeof(pkt.cmd));

			tv.tv_sec = 1;
			tv.tv_usec = 0;
retry_select:
			FD_ZERO(&rfds);
			FD_SET(sendsock, &rfds);
			if(-1 == (err = select(sendsock+1, &rfds, NULL, NULL, &tv))) {
				if(errno == EINTR)	
					goto retry_select;
				perror("select");
			}
			if(!err) {
				continue;
			}
retry_recv:
			if(-1 == (plen = recv(sendsock, &pkt, sizeof(pkt), MSG_DONTWAIT))) {
				if(errno == EINTR)	
					goto retry_recv;
				perror("recv");
				return 1;
			}
			if(plen != sizeof(pkt.cmd) + sizeof(pkt.config)) {
				fprintf(stderr, "Expected packet of len %u, got %u\n", (unsigned)(sizeof(pkt.cmd) + sizeof(pkt.config.cfg)), (unsigned)plen);
				return 1;
			}
			printf("SerialNr     : %c%c%c%c%c%c%c%c\n", pkt.config.serial[0], pkt.config.serial[1], pkt.config.serial[2], pkt.config.serial[3],
								    pkt.config.serial[4], pkt.config.serial[5], pkt.config.serial[6], pkt.config.serial[7]);
			printf("Magic        : 0x%02x 0x%02x\n", pkt.config.cfg.magic[0], pkt.config.cfg.magic[1]);
			printf("MAC          : %02x:%02x:%02x:%02x:%02x:%02x\n", pkt.config.cfg.macaddr[0], pkt.config.cfg.macaddr[1], pkt.config.cfg.macaddr[2],
										 pkt.config.cfg.macaddr[3], pkt.config.cfg.macaddr[4], pkt.config.cfg.macaddr[5]);
			printf("IP           : %u.%u.%u.%u\n", pkt.config.cfg.ipaddr[0], pkt.config.cfg.ipaddr[1], pkt.config.cfg.ipaddr[2], pkt.config.cfg.ipaddr[3]);
			printf("Netmask      : %u.%u.%u.%u\n", pkt.config.cfg.nmaddr[0], pkt.config.cfg.nmaddr[1], pkt.config.cfg.nmaddr[2], pkt.config.cfg.nmaddr[3]);
			printf("Gateway      : %u.%u.%u.%u\n", pkt.config.cfg.gwaddr[0], pkt.config.cfg.gwaddr[1], pkt.config.cfg.gwaddr[2], pkt.config.cfg.gwaddr[3]);
			printf("NTP peer     : %u.%u.%u.%u\n", pkt.config.cfg.ntpaddr[0], pkt.config.cfg.ntpaddr[1], pkt.config.cfg.ntpaddr[2], pkt.config.cfg.ntpaddr[3]);
			printf("Port         : %u (0x%04x)\n", le16toh(pkt.config.cfg.port), le16toh(pkt.config.cfg.port));
			printf("TZ offset    : %+d\n", (int)((int16_t)le16toh(pkt.config.cfg.tzoffset)));
			memcpy(hname, pkt.config.cfg.hostname, sizeof(pkt.config.cfg.hostname));
			hname[sizeof(hname)-1] = 0;
			printf("Hostname     : %s\n", hname);
			printf("DHCP         : %s\n", pkt.config.cfg.dhcp_on ? "On" : "Off");
			printf("DHCP hostname: %s\n", pkt.config.cfg.dhcp_hostname ? "On" : "Off");
			printf("NTP          : %s\n", pkt.config.cfg.ntp_on ? "On" : "Off");
			printf("NTP MC peer  : %s\n", pkt.config.cfg.ntp_mcpeer ? "On" : "Off");
			printf("NTP to RTCC  : %s\n", pkt.config.cfg.ntp_to_rtcc ? "On" : "Off");
			printf("NTP use DHCP : %s\n", pkt.config.cfg.ntp_use_dhcp ? "On" : "Off");
			printf("Orientation  : %u\n", pkt.config.cfg.orientation);
			printf("Move timeout : %u\n", le16toh(pkt.config.cfg.movetimeout));
			printf("DNS 1        : %u.%u.%u.%u\n", pkt.config.cfg.dnsaddr1[0], pkt.config.cfg.dnsaddr1[1], pkt.config.cfg.dnsaddr1[2], pkt.config.cfg.dnsaddr1[3]);
			printf("DNS 2        : %u.%u.%u.%u\n", pkt.config.cfg.dnsaddr2[0], pkt.config.cfg.dnsaddr2[1], pkt.config.cfg.dnsaddr2[2], pkt.config.cfg.dnsaddr2[3]);
			printf("Drift        : %d\n", (int)((int32_t)le32toh(pkt.config.cfg.drift)));
			return 0;
		}
		fprintf(stderr, "No answer from %s\n", host);
		return 1;
	}

	pkt.cmd = htons(NETCMD_SETMODE);
	pkt.mode = DISPMODE_REMOTE;
	sendpkt(sizeof(pkt.cmd) + sizeof(pkt.mode));

	reset_automovetime();

	while(!quit) {
		struct timeval tv;
		fd_set rfds;
		int lv = light_enable ? light : 255;

		FD_ZERO(&rfds);
		FD_SET(pfd[0], &rfds);
		FD_SET(recvsock, &rfds);

		if(-1 == select((recvsock > pfd[0] ? recvsock : pfd[0])+1, &rfds, NULL, NULL, NULL)) {
			if(errno == EINTR)	
				continue;
			perror("select");
		}

		if(FD_ISSET(pfd[0], &rfds)) {
			char ch;
retryread:
			if(-1 == read(pfd[0], &ch, 1) && errno == EINTR)
				goto retryread;

			automoveleft -= delay;
			if(automove && automoveleft <= 0) {
				reset_automovetime();
				mode = (mode+1) % MODE_LAST;
			}
			memset(&displays, 0, sizeof(displays));
			switch(mode) {
			case MODE_CHASE:
				show_chase(i++);
				break;

			case MODE_CLOCK:
				gettimeofday(&tv, NULL);
				show_clock(&tv, lv);
				break;

			case MODE_DATE:
				gettimeofday(&tv, NULL);
				show_date(&tv, lv);
				break;

			case MODE_CLOCK2:
				gettimeofday(&tv, NULL);
				show_clock2(&tv, lv);
				break;

			case MODE_RINGS:
				show_rings(i++);
				break;

			case MODE_SPIRAL:
				show_spiral(i++, 0);
				break;

			case MODE_SPIRAL2:
				show_spiral(i++, 1);
				break;

			case MODE_SEQUENTIAL:
				set_sequential(i++);
				break;

			case MODE_RANDOM:
				for(i = 0; i < sizeof(displays.bytes); i++) {
					displays.bytes[i] = rand();
				}
				break;

			case MODE_WAVE:
				show_wave(i++);
				break;

			case MODE_BULGE:
				show_bulge(i++);
				break;

			case MODE_FADE:
				show_fade(i++);
				break;

			default:
				mode = MODE_CLOCK;
				break;
			}
			fill_packet();
			pkt.cmd = htons(NETCMD_SETDATA);
			if(-1 == sendpkt(sizeof(pkt.cmd) + sizeof(pkt.sections)))
				break;
		}
		if(FD_ISSET(recvsock, &rfds)) {
			netctrl_t buf;
			ssize_t l = recv(recvsock, &buf, sizeof(buf), 0);
			if(l > sizeof(buf.cmd)) {
				switch(ntohs(buf.cmd)) {
				case NETCMD_MOVE:
					gettimeofday(&tv, NULL);
					if(enable_modemove && tv.tv_sec -lastmove.tv_sec >= move_wait && !move && buf.value.move) {
						mode = (mode+1) % MODE_LAST;
						lastmove = tv;
						reset_automovetime();
					}
					move = buf.value.move != 0;
					break;
				case NETCMD_LIGHT:
					light = buf.value.light;
					break;
				}
			}
		}
	}

	pkt.cmd = htons(NETCMD_SETMODE);
	pkt.mode = DISPMODE_CLOCK;
	sendpkt(sizeof(pkt.cmd) + sizeof(pkt.mode));
	return 0;
}
