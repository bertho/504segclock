/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_I2C_H
#define __504SEGCLOCK_I2C_H

void i2c_init(void);
void i2c_uid_read(uint8_t *uid);
void i2c_uid_write(uint8_t *uid);
uint8_t i2c_reg_read(uint8_t reg) __wparam;
void i2c_reg_write(uint8_t reg, uint8_t val);
void i2c_regs_read(uint8_t addr, uint8_t len, uint8_t *ptr);
void i2c_regs_write(uint8_t addr, uint8_t len, uint8_t *ptr);
void i2c_eeprom_read(uint8_t addr, uint8_t len, uint8_t *ptr);
void i2c_eeprom_write(uint8_t addr, uint8_t len, uint8_t *ptr);

#endif
