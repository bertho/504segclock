/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <pic18fregs.h>
#include <spi.h>
#include <display.h>
#include <clock.h>
#include <config.h>

static uint8_t idx;
static uint8_t section;
static uint8_t nextbyte;
#ifdef LINEAR_DISPLAY
static uint16_t offset;
#else
static uint8_t *bptr;
#endif

void spi_init(void)
{
	PORTE = 0xef;		/* All nSSx disable, nLATCH active */
	TRISE = 0xe0;		/* nSSx outputs and nLATCH */
	TRISCbits.TRISC5 = 0;	/* SDO output */
	TRISCbits.TRISC4 = 1;	/* SDI input */
	TRISCbits.TRISC3 = 0;	/* SCK output */
	PR2 = 4;		/* Timer2 period 41.667MHz/4/(4+1) ~ 2.083MHz -> SPI ~ 1.042MHz */
	T2CON = 0x04;		/* Timer2 enable, prescale 1:1, postscale 1:1 */
	SSP1STAT = 0x00;	/* Sample middle, CKE=0 */
	SSP1CON1 = 0x23;	/* Master mode clock on timer2, CKP=0, enable */
	PIR1bits.SSP1IF = 0;
	PIE1bits.SSP1IE = 0;	/* Will be enabled when we flush the data out */
	clock_timer_set(TIMER_SPI, SPI_TICKS);
}

/* PORTE SS select pins for sections */
static const uint8_t sectidx[5] = {
	0xf7, 0xfb, 0xfd, 0xfe, 0xef,
};

#ifdef LINEAR_DISPLAY
/* Offsets of display buffer to section display position */
static const uint16_t dispoffset[NSECTIONS*NSEGMENTS] = {
	/* Section three */
	544, 545, 546, 547, 548, 549, 550, 551, 536, 537, 538, 539, 540, 541, 542, 543, 528, 529, 530, 531, 532, 533, 534, 535,

	424, 425, 426, 427, 428, 429, 430, 431, 416, 417, 418, 419, 420, 421, 422, 423, 408, 409, 410, 411, 412, 413, 414, 415,
	400, 401, 402, 403, 404, 405, 406, 407, 392, 393, 394, 395, 396, 397, 398, 399, 384, 385, 386, 387, 388, 389, 390, 391,

	208, 209, 210, 211, 212, 213, 214, 215, 200, 201, 202, 203, 204, 205, 206, 207, 192, 193, 194, 195, 196, 197, 198, 199,
	184, 185, 186, 187, 188, 189, 190, 191, 176, 177, 178, 179, 180, 181, 182, 183, 168, 169, 170, 171, 172, 173, 174, 175,
	160, 161, 162, 163, 164, 165, 166, 167, 152, 153, 154, 155, 156, 157, 158, 159, 144, 145, 146, 147, 148, 149, 150, 151,

	/* Section four */
	568, 569, 570, 571, 572, 573, 574, 575, 560, 561, 562, 563, 564, 565, 566, 567, 552, 553, 554, 555, 556, 557, 558, 559,

	472, 473, 474, 475, 476, 477, 478, 479, 464, 465, 466, 467, 468, 469, 470, 471, 456, 457, 458, 459, 460, 461, 462, 463,
	448, 449, 450, 451, 452, 453, 454, 455, 440, 441, 442, 443, 444, 445, 446, 447, 432, 433, 434, 435, 436, 437, 438, 439,

	280, 281, 282, 283, 284, 285, 286, 287, 272, 273, 274, 275, 276, 277, 278, 279, 264, 265, 266, 267, 268, 269, 270, 271,
	256, 257, 258, 259, 260, 261, 262, 263, 248, 249, 250, 251, 252, 253, 254, 255, 240, 241, 242, 243, 244, 245, 246, 247,
	232, 233, 234, 235, 236, 237, 238, 239, 224, 225, 226, 227, 228, 229, 230, 231, 216, 217, 218, 219, 220, 221, 222, 223,

	/* Section one */
	496, 497, 498, 499, 500, 501, 502, 503, 488, 489, 490, 491, 492, 493, 494, 495, 480, 481, 482, 483, 484, 485, 486, 487,

	328, 329, 330, 331, 332, 333, 334, 335, 320, 321, 322, 323, 324, 325, 326, 327, 312, 313, 314, 315, 316, 317, 318, 319,
	304, 305, 306, 307, 308, 309, 310, 311, 296, 297, 298, 299, 300, 301, 302, 303, 288, 289, 290, 291, 292, 293, 294, 295,

	 64,  65,  66,  67,  68,  69,  70,  71,  56,  57,  58,  59,  60,  61,  62,  63,  48,  49,  50,  51,  52,  53,  54,  55,
	 40,  41,  42,  43,  44,  45,  46,  47,  32,  33,  34,  35,  36,  37,  38,  39,  24,  25,  26,  27,  28,  29,  30,  31,
	 16,  17,  18,  19,  20,  21,  22,  23,   8,   9,  10,  11,  12,  13,  14,  15,   0,   1,   2,   3,   4,   5,   6,   7,

	/* Section two */
	520, 521, 522, 523, 524, 525, 526, 527, 512, 513, 514, 515, 516, 517, 518, 519, 504, 505, 506, 507, 508, 509, 510, 511,

	376, 377, 378, 379, 380, 381, 382, 383, 368, 369, 370, 371, 372, 373, 374, 375, 360, 361, 362, 363, 364, 365, 366, 367,
	352, 353, 354, 355, 356, 357, 358, 359, 344, 345, 346, 347, 348, 349, 350, 351, 336, 337, 338, 339, 340, 341, 342, 343,

	136, 137, 138, 139, 140, 141, 142, 143, 128, 129, 130, 131, 132, 133, 134, 135, 120, 121, 122, 123, 124, 125, 126, 127,
	112, 113, 114, 115, 116, 117, 118, 119, 104, 105, 106, 107, 108, 109, 110, 111,  96,  97,  98,  99, 100, 101, 102, 103,
	 88,  89,  90,  91,  92,  93,  94,  95,  80,  81,  82,  83,  84,  85,  86,  87,  72,  73,  74,  75,  76,  77,  78,  79,
};
#endif

void spi_update(void)
{
	if(PIE1bits.SSP1IE)	/* Don't start a new transfer if one is running */
		return;

	section = 0;
	idx = 0;
	PORTE = sectidx[0];		/* Enable nSS0, first section */
#ifdef LINEAR_DISPLAY
	offset = 0;
	nextbyte = ((uint8_t *)&segments.bytes[0])[dispoffset[0]];
#else
	bptr = &segments[0].bytes[0];
	nextbyte = *bptr++;		/* Start with the first byte */
#endif
	PIR1bits.SSP1IF = 1;		/* Enable interrupts */
	PIE1bits.SSP1IE = 1;
}

void spi_handle_timer(void)
{
	PORTE = 0xff;			/* Deactivate nLATCH */
	clock_timer_clear(TIMER_SPI);
}

void spi_isr(void)
{
	/* Read the buffer, throw away the result... */
	__asm
	movf	_SSP1BUF, w
	__endasm;

	if(idx++ >= NSEGMENTS) {
		/* Move the nSSx to the next section */
		PORTE = sectidx[++section];
		if(section >= NSECTIONS) {
			PIE1bits.SSP1IE = 0;
			clock_timer_set_isr(TIMER_SPI, SPI_TICKS);
		}
		idx = 0;	/* Reset the index counter */
		/*
		 * Return from isr and the set IF will return us here. We need
		 * to give the slave(s) time for the SS change and settle the
		 * clock line.
		 */
		return;
		//bptr = &segments[section].bytes[0];
	}

	PIR1bits.SSP1IF = 0;	/* Clear the interrupt flag */
	SSP1BUF = nextbyte;	/* Send the next byte */
#ifdef LINEAR_DISPLAY
	offset++;
	nextbyte = ((uint8_t *)&segments.bytes[0])[dispoffset[offset]];
#else
	nextbyte = *bptr++;
#endif
}
