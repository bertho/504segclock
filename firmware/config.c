/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <pic18fregs.h>
#include <network.h>
#include <bytes.h>
#include <config.h>
#include <serial.h>
#include <i2c.h>

#ifndef HAVE_BOOTLOADER
#if 0	/* See main.c */
#if 1
static void processor_config(void) __naked
{

	__asm
		CONFIG	DEBUG=OFF
		CONFIG	XINST=OFF
		CONFIG	STVR=OFF
		CONFIG	WDT=OFF
		CONFIG	CP0=OFF
		CONFIG	FCMEN=ON
		CONFIG	IESO=ON
		CONFIG	FOSC2=ON
		CONFIG	FOSC=HSPLL
		CONFIG	WDTPS=1024
		CONFIG	ETHLED=ON
	__endasm;
}
#else
__code char __at __CONFIG1L config1l = 0x80;	/* no debug; no extended instuctions; no stack under/overflow; no watchdog */
__code char __at __CONFIG1H config1h = 0xf4;	/* no code protection */
__code char __at __CONFIG2L config2l = 0xc5;	/* two-speed start; fail-safe clock; Clock selected by FOSC1:FOSC0; HS oscillator, PLL enabled and under software control */
__code char __at __CONFIG2H config2h = 0xfa;	/* WDT postscaler 1:1024 (~4s) */
__code char __at __CONFIG3L config3l = 0xff;	/* <only 100pin devices> */
__code char __at __CONFIG3H config3h = 0xf7;	/* Ethernet LED Enable; ECCP[13] multiplex RE; ECCP2 multiplex RC */
#endif
#endif
#endif

uint8_t	serialnr[8];	/* Device's serial from rtcc uid */
sysconfig_t cfg;	/* Global configuration (loaded from persistent storage) */

__code firmwareid_t __at FIRMWAREID_ADDR_START firmwareid = {
	VERSIONID,
	BUILDID,
	"504 Segment Clock (c) 2011 Vagrearg.org\n"
	"Distributed under GPLv3\n"
	"See www.vagrearg.org\n"
};

static uint8_t hexdigit(uint8_t ch)
{
	ch -= '0';
	return ch > 9 ? (ch - 7) : ch;
}

uint8_t config_load(void)
{
	i2c_uid_read(serialnr);
	if(serialnr[0] == 0x00 || serialnr[0] == 0xff) {
		serialnr[0] = 'B';
		serialnr[1] = 'C';
		serialnr[2] = '0';
		serialnr[3] = '0';
		serialnr[4] = '0';
		serialnr[5] = '0';
		serialnr[6] = '0';
		serialnr[7] = '1';
	}
	i2c_eeprom_read(0, sizeof(cfg), (uint8_t *)&cfg);
	if(cfg.magic[0] != SYSCONFIG_MAGIC_LO || cfg.magic[1] != SYSCONFIG_MAGIC_HI) {
		cfg.magic[0] = SYSCONFIG_MAGIC_LO;
		cfg.magic[1] = SYSCONFIG_MAGIC_HI;
		cfg.macaddr[0] = 'B';
		cfg.macaddr[1] = 'C';
		cfg.macaddr[2] = 'L';
		cfg.macaddr[3] = (hexdigit(serialnr[2]) << 4) | hexdigit(serialnr[3]);
		cfg.macaddr[4] = (hexdigit(serialnr[4]) << 4) | hexdigit(serialnr[5]);
		cfg.macaddr[5] = (hexdigit(serialnr[6]) << 4) | hexdigit(serialnr[7]);
		IP_SET(cfg.ipaddr, 0);
		IP_SET(cfg.nmaddr, 0);
		IP_SET(cfg.gwaddr, 0);
		cfg.port = 0xc10c;
		cfg.tzoffset = 60;
		memcpy(cfg.hostname, serialnr, sizeof(serialnr));
		cfg.hostname[sizeof(serialnr)] = 0;
		cfg.ntpaddr[0] = 224;
		cfg.ntpaddr[1] = 0;
		cfg.ntpaddr[2] = 1;
		cfg.ntpaddr[3] = 1;
		cfg.ntp_on = 1;
		cfg.ntp_mcpeer = 1;
		cfg.ntp_to_rtcc = 1;
		cfg.ntp_use_dhcp = 1;
		cfg.dhcp_on = 1;
		cfg.dhcp_hostname = 1;
		cfg.orientation = 0;
		cfg.movetimeout = 0;
		IP_SET(cfg.dnsaddr1, 0);
		IP_SET(cfg.dnsaddr2, 0);
		cfg.drift = 0;
		return 0;
	}
	cfg.orientation %= 12;	/* Ensure only 4 quadrants */
	return 1;
}

void config_save(void)
{
	uint8_t i;
	for(i = 0; i < sizeof(cfg); i += 8)
		i2c_eeprom_write(i, 8, ((uint8_t *)&cfg) + i);
}

void config_factoryreset(void)
{
	uint8_t nul = 0;
	/* Overwrite the magic number */
	i2c_eeprom_write(0, 1, &nul);

	/* Reset the beast after a clear all config */
	__asm
	reset
	__endasm;
}
