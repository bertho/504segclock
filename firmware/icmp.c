/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <string.h>
#include <network.h>
#include <config.h>
#include <ethernet.h>
#include <icmp.h>
#include <serial.h>

//#define DEBUG_ICMP	1

#define	_eth	packet.ethpkt.hdr
#define _iph	packet.ethpkt.iph

uint8_t icmp_handle_rxpacket(uint16_t pktlen)
{
	uint16_t sum;
	icmphdr_t *icmp;
	uint16_t icmplen;

	(void)pktlen;

	icmp = (icmphdr_t *)(((uint8_t *)&_iph) + iphdr_size(&_iph));
	icmplen = ntohs(_iph.len) - iphdr_size(&_iph);
	/* If the datagram is less than the icmp header it is junk */
	if(icmplen < sizeof(icmphdr_t))
		return 0;

#ifdef DEBUG_ICMP
	serial_puts("icmp accept icmplen\n");
#endif
	if(icmp->checksum) {
		/* Check the ICMP checksum */
		if(0 != (sum = ip_checksum((iphdr_t *)icmp, icmplen))) {
			/* Wrong checksum */
#ifdef DEBUG_ICMP
			serial_putc('-');
			serial_puthex_u16(sum);
			serial_putc('-');
			serial_puthex_u16(icmp->checksum);
			serial_putc('-');
#endif
			return 0;
		}
	}
#ifdef DEBUG_ICMP
	serial_puts("icmp accept checksum\n");
#endif

	if(!IP_IS_VALID(_iph.saddr) || IP_IS_MULTICAST(_iph.saddr))
		return 0;
#ifdef DEBUG_ICMP
	serial_puts("icmp accept source address\n");
#endif

	if(icmp->icmptype == ICMP_ECHO_REQUEST) {
		/* The code-field must be set to 0 */
		if(icmp->icmpcode)
			return 0;

#ifdef DEBUG_ICMP
		serial_puts("icmp accept icmp request\n");
#endif
		/* Set the new MACs */
		MAC_CP(_eth.da, _eth.sa);
		MAC_CP(_eth.sa, cfg.macaddr);
		/* ethertype is already set */
		sum = _iph.hdrlen;	/* save the old hdrlen */
		_iph.hdrlen = 5;
		_iph.dscp_ecn = 0;
		_iph.len = htons(icmplen + sizeof(_iph));
		_iph.fo_lo = 0;
		_iph.flag_mf = 0;
		_iph.flag_df = 1;
		_iph.flag_res = 0;
		_iph.fo_hi = 0;
		_iph.ttl = IP_TTL_DEFAULT;
		/* _iph.proto = IPPROTO_ICMP;	already set */
		_iph.checksum = 0;
		IP_CP(_iph.daddr, _iph.saddr);
		IP_CP(_iph.saddr, cfg.ipaddr);
		_iph.checksum = ip_checksum(&_iph, iphdr_size(&_iph));
		/* If the incoming packet had any IP options set, we need to move the data */
		if(sum != 5)
			memmove(((uint8_t *)&packet.ethpkt.iph) + iphdr_size(&_iph), icmp, icmplen);
		icmp = (icmphdr_t *)(((uint8_t *)&packet.ethpkt.iph) + iphdr_size(&_iph));
		icmp->icmptype = ICMP_ECHO_REPLY;
		icmp->checksum = 0;
		icmp->checksum = ip_checksum((iphdr_t *)icmp, icmplen);
#ifdef DEBUG_ICMP
		serial_puts("icmp sending reply\n");
		serial_flush_out();
#endif
		return ethernet_txpacket(ntohs(_iph.len) + sizeof(ethhdr_t));
	}
	/* All others are ignored for now. We should take ICMP_REDIRECT serious though... */
	return 0;
}

