/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdint.h>
#include <network.h>
#include <config.h>
#include <ethernet.h>
#include <ip.h>
#include <udp.h>
#include <icmp.h>
#include <serial.h>

//#define	DEBUG_IP	1

static uint8_t iparpmac[6];	/* Copy of the incoming packet's source MAC */
static uint8_t iparpip[4];	/* Copy of the incoming packet's source IP */

void ip_init(void)
{
}

#define _iph	packet.ethpkt.iph

void ip_handle_rxpacket(uint16_t pktlen)
{
	uint16_t sum;
	uint8_t rv = 0;

	/* No point continuing if the packet is too short */
	if(pktlen < sizeof(ethhdr_t) + sizeof(iphdr_t))
		return;
#ifdef DEBUG_IP
	serial_puts("ip accept pktlen\n");
#endif

	/* Don't accept broad-/multi-cast in the source MAC */
	if(packet.ethpkt.hdr.sa[0] & 1)
		return;

#if 0	/* Actually, we want to receive multicast for this app... */
	/* Don't respond to multicast or broadcast, except when we are waiting for DHCP */
	if(MAC_IS_MULTICAST(packet.ethpkt.hdr.da) && !(cfg.dhcp_on && *(uint32_t *)cfg.ipaddr == 0 && _iph.proto == IPPROTO_UDP))
		return;
#endif

#ifdef DEBUG_IP
	serial_puts("ip accept macs\n");
#endif
	/* No need to check rest if the version does not match or the header is too small */
	if(_iph.version != IP_VERSION_IPV4 || _iph.hdrlen < 5)
		return;

#ifdef DEBUG_IP
	serial_puts("ip accept hdrlen/version\n");
#endif
	/* Check the IP header checksum */
	if(0 != (sum = ip_checksum(&_iph, iphdr_size(&_iph)))) {
		/* Wrong checksum */
#ifdef DEBUG_IP
		serial_putc('!');
		serial_puthex_u16(sum);
		serial_putc('!');
		serial_puthex_u16(_iph.checksum);
		serial_putc('!');
#endif
		return;
	}

#ifdef DEBUG_IP
	serial_puts("ip accept checksum\n");
#endif
	/* We only want packets with a TTL of 1 and above */
	if(_iph.ttl < 1)
		return;

#ifdef DEBUG_IP
	serial_puts("ip accept ttl\n");
#endif
	/* We only want to see packets addressed at us directly or MC, no broadcast */
	/* If dhcp is on and our IP is 0.0.0.0 and UDP, then we must allow the packet to continue */
	/* FIXME: should also check source addres */
	if(!((cfg.dhcp_on && *(uint32_t *)cfg.ipaddr == 0 && _iph.proto == IPPROTO_UDP) || IP_IS_SAME(_iph.daddr, cfg.ipaddr) || IP_IS_MULTICAST(_iph.daddr)))
		return;

#ifdef DEBUG_IP
	serial_puts("ip accept ipaddr\n");
#endif
	/* The packet length must be at least what we received (because we don't do well with fragmentation) */
	if(pktlen < ntohs(_iph.len) + sizeof(ethhdr_t))
		return;

#ifdef DEBUG_IP
	serial_puts("ip accept len\n");
#endif
	/* We don't handle fragmentation */
	if(_iph.flag_mf || _iph.fo_lo || _iph.fo_hi)
		return;

#ifdef DEBUG_IP
	serial_puts("ip accept fragmentation\n");
#endif

	/* Copy the source address, we might need it for an arp entry */
	MAC_CP(iparpmac, packet.ethpkt.hdr.sa);
	IP_CP(iparpip, _iph.saddr);

	/* Packet seems legit, see what we can do */
	if(_iph.proto == IPPROTO_ICMP && !IP_IS_MULTICAST(_iph.daddr))
		rv = icmp_handle_rxpacket(pktlen);
	else if(_iph.proto == IPPROTO_UDP)
		rv = udp_handle_rxpacket(pktlen);
	/* We ignore everything else, for now */

	/* The packet got handled and is valid for use, take note by adding/updating an ARP entry */
	if(rv)
		arp_add(iparpmac, iparpip);
}


/* The 1's complement sum in IP checksums */
uint16_t ip_checksum(iphdr_t *ipp, uint16_t len)
{
	uint32_t sum = 0;
	for(; len > 1; len -= 2) {
		sum += *((uint16_t *)ipp);
		ipp = (iphdr_t *)((uint8_t *)ipp + 2);
	}
	if(len)
		sum += *(uint8_t *)ipp;
	/* Fold the sum */
	while(sum & 0xffff0000)
		sum = (sum & 0x0000ffff) + (sum >> 16);
	return ~sum;
}

