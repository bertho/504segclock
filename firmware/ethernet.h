/*
 * 504 Segment Clock - Control unit
 *
 * Copyright (C) 2012  B.Stultiens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __504SEGCLOCK_ETHERNET_H
#define __504SEGCLOCK_ETHERNET_H

#define ABS_PACKET	1

#define PHCON1	0x00
#define PHSTAT1	0x01
#define PHCON2	0x10
#define PHSTAT2	0x11
#define PHIE	0x12
#define PHIR	0x13
#define PHLCON	0x14

#define ETH_BUFSIZE	((uint16_t)(8*1024))	/* The eth module has 8k mem */
#define TXBUF_SIZE	((uint16_t)(0x05fa))	/* 1530 = Reset default; Max packet size + preamble byte and status vector; MUST be even! */
#define RXBUF_SIZE	((uint16_t)(ETH_BUFSIZE - TXBUF_SIZE))
/* Organization: start with TX buffer and end with RX buffer */
#define TXBUF_START	((uint16_t)(0))
#define TXBUF_END	((uint16_t)(TXBUF_START + TXBUF_SIZE - 1))
#define RXBUF_START	((uint16_t)(TXBUF_END + 1))
#define RXBUF_END	((uint16_t)(ETH_BUFSIZE - 1))

/* Maximum data payload in an eth packet subtract:
 * - ethernet header
 * - FCS
 * - preamble byte
 * - status vecor
 * - on an even boundary
 * But, it is easier to just say 1500...
 */
/* #define ETH_PAYLOADMAX	((TXBUF_SIZE - sizeof(ethhdr_t) - sizeof(uint32_t) - 1 - 6) & 0xfffe) */
#define ETH_PAYLOADMAX	1500

#include <arp.h>
#include <ip.h>

#define ETHTYPE_ARP	0x0806
#define ETHTYPE_IP	0x0800

//#define htons(x)	((((uint16_t)(x) >> 8) & 0x00ff) | (((uint16_t)(x) << 8) & 0xff00))
#define htons(x)	(((uint16_t)(x) >> 8) | ((uint16_t)(x) << 8))
#define ntohs(x)	htons(x)
#define htonl(x)	(((uint32_t)(x) >> 24) | (((uint32_t)(x) & 0x00ff0000) >> 8) | (((uint32_t)(x) & 0x0000ff00) << 8) | ((uint32_t)(x) << 24))
#define ntohl(x)	htonl(x)

extern uint8_t macaddr[6];

#define ETH_LINK_DOWN	0
#define ETH_LINK_UP	1
extern volatile uint8_t ethlink;
extern volatile uint8_t ethlinkchange;
extern volatile uint32_t eth_secs;		/* Timestamp of packet receive */
extern volatile uint16_t eth_msecs;

void ethernet_init(void);
void ethernet_isr(void);
void ethernet_send_pmd(void);
uint16_t ethernet_getphy(uint8_t r);
void ethernet_handle_rxpacket(void);
#define ethernet_rxpacket_ready()	(EPKTCNT != 0)
uint8_t ethernet_txpacket(uint16_t len);

typedef union __eth_rx_status_t {
	uint8_t	bytes[6];
	struct {
		union {
			struct {
				uint8_t	nextl;	/* Next packet pointer */
				uint8_t	nexth;
			};
			uint16_t	next;
		};
		union {
			struct {
				uint8_t	rxl;	/* Number of bytes received */
				uint8_t	rxh;
			};
			uint16_t	rx;
		};
		struct {
			uint8_t	ev_long_drop	:1;
			uint8_t			:1;
			uint8_t	ev_carrier	:1;
			uint8_t			:1;
			uint8_t	err_crc		:1;
			uint8_t	err_length	:1;
			uint8_t	err_range	:1;
			uint8_t	rx_ok		:1;
		};
		struct {
			uint8_t	pkt_mc		:1;
			uint8_t	pkt_bc		:1;
			uint8_t	err_dribble	:1;
			uint8_t	pkt_ctl_frame	:1;
			uint8_t	pkt_pause_frame	:1;
			uint8_t	err_opcode	:1;
			uint8_t	vlan		:1;
			uint8_t			:1;
		};
	};
} eth_rx_status_t;

typedef union __eth_tx_status_t {
	uint8_t	bytes[7];
	struct {
		uint8_t	txh;	/* Bytes sent */
		uint8_t	txl;
		struct {
			uint8_t	collisions	:4;
			uint8_t	err_crc		:1;
			uint8_t	err_length	:1;
			uint8_t	err_range	:1;
			uint8_t	tx_ok		:1;
		};
		struct {
			uint8_t	pkt_mc		:1;
			uint8_t	pkt_bc		:1;
			uint8_t	pkt_defer	:1;
			uint8_t	pkt_exc_defer	:1;
			uint8_t	pkt_exc_coll	:1;
			uint8_t	pkt_late_coll	:1;
			uint8_t	giant		:1;
			uint8_t	err_underrun	:1;
		};
		uint8_t	total_txh;	/* Total bytes sent, incl. re-tx */
		uint8_t	total_txl;
		struct {
			uint8_t	pkt_ctl_frame	:1;
			uint8_t	pkt_pause_frame	:1;
			uint8_t	backpressure	:1;
			uint8_t	vlan		:1;
			uint8_t			:4;
		};
	};
} eth_tx_status_t;

typedef struct __ethhdr_t {
	uint8_t		da[6];		/* MAC destination address */
	uint8_t		sa[6];		/* MAC source address */
	union {
		uint8_t		len[2];		/* Type/length */
		uint16_t	lenw;
	};
} ethhdr_t;

typedef struct __ethpacket_t {
	ethhdr_t	hdr;	/* Ethernet header */
	union {			/* Payload data */
		uint8_t 	octets[1500];	/* Raw data */
		arphdr_t	ah;		/* ARP packet */
		iphdr_t		iph;		/* IP packet */
	};
} ethpacket_t;

#define MAXPKTLEN	(sizeof(ethpacket_t))

typedef union __packet_t {
	uint8_t		octets[MAXPKTLEN];
	ethpacket_t	ethpkt;
} packet_t;

#ifdef ABS_PACKET
#define __PACKET_ADDRESS	0x0100
extern __data packet_t __at __PACKET_ADDRESS packet;
#else
extern packet_t packet;
#endif

#endif
