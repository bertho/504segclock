thickness = 40.0;
bottom = 2.0;
wall = 6.5;
wallmin = 2.0;
wallcutspace = 1.0;
intwidth = 187.3;
intradius = 110.0;
extwidth = intwidth + 2*wall;
extradius = intradius + wall;
rimwidth = 5.0;
rimdepth = 2.0;
pcbthickness = 1.6;
pcbdepth = 7.0+pcbthickness;
pcbspacer = 20.0;

strutradius = 6.0;
strutholeradius = 1.2;
strutx = 50.0;
struty = 89.8;

pillarradius = 5.0;
screwholeradius = 1.0;
washerradius = 3.5;
washerh = 1.0;
nutdia = 5.55;
nutrounding = 0.25;

$fa = 1;
$fs = 0.25;
$fa = 7.5;
$fs = 1.5;
//$fn = 30;

module screwholes()
{
	hrim = ((intwidth+rimwidth)/2);
	hdepth = thickness-2*bottom;
	for(j = [0, 90, 180, 270]) {
		rotate(a = j, v = [0, 0, 1]) {
			for(i = [60, 120]) {
				translate([hrim, hrim*cos(i), bottom]) {
					cylinder(h = hdepth, r = screwholeradius, center=true);
				}
			}
		}
	}
}

module baseshape(_w, _r, _h)
{
	intersection() {
		cube(size = [_w, _w, _h], center = true);
		cylinder(h = _h, r = _r, center = true);
	}
}

module rounded(_x, _y, _z, _r)
{
	minkowski() {
		cube(size = [_x-2*_r, _y-2*_r, _z/2], center = true);
		cylinder(h = _z/2, r = _r, center = true);
	}
}

module bottomhole()
{
	roundrad = 2;
	toppos = -40.21;
	bigw = 25.0;
	bigh = intwidth/2+toppos;
	bigx = -7.87;
	smallw = 40.0;
	smallh = 10.0;
	smallx = 20.7;
	holew = 5.0;
	translate([0, 0, -thickness/2]) {
		union() {
			translate([bigx, toppos-bigh/2, 0]) {
				rounded(bigw, bigh, 2*bottom, roundrad);
			}
			translate([smallx, toppos-smallh/2, 0]) {
				rounded(smallw, smallh, 2*bottom, roundrad);
			}
			translate([bigx+bigw/2+roundrad/2, toppos-smallh-roundrad/2+0.06, 0]) {
				difference() {
					cube(size = [roundrad, roundrad, 2*bottom], center=true);
					translate([roundrad/2, -roundrad/2, 0]) {
						cylinder(r = roundrad, h = 2*bottom, center=true);
					}
				}
			}
			translate([0, -intwidth/2, holew/2]) {
				translate([0, 0, -holew/2]) {
					cube(size = [holew, 50, holew], center = true);
				}
				rotate(a = 90, v = [1, 0, 0]) {
					cylinder(h = 50, r = holew/2, center = true);
				}
			}
		}
	}
}

hhsmallr = 3.5/2;
hhlarger = 7/2;
hhrimh = 1.0;
hhrimw = 2.5;
hhposfromtop = 20.0;

module hh_hull(_w2)
{
	hull() {
		translate([0, _w2, 0])
			cylinder(r = hhsmallr, h = thickness, center = true);
		translate([0, -_w2, 0])
			cylinder(r = hhsmallr, h = thickness, center = true);
	}
}

module hanghole1()
{
	union() {
		hh_hull(hhsmallr);
		translate([0, -hhlarger, 0])
			cylinder(r = hhlarger, h = thickness, center = true);
	}
}

module hanghole2()
{
	union() {
		hh_hull(hhsmallr+hhlarger);
		rotate(a = 90, v = [0, 0, 1])
			hh_hull(hhsmallr+hhlarger);
		cylinder(r = hhlarger, h = thickness, center = true);
	}
}

module hanghole()
{
	translate([0, -hhsmallr+extwidth/2-hhposfromtop, -thickness/2])
		hanghole1();
	translate([-hhsmallr+extwidth/2-hhposfromtop, 0, -thickness/2])
		hanghole2();
	translate([ hhsmallr-extwidth/2+hhposfromtop, 0, -thickness/2])
		hanghole2();
}

module hh_rimhull(_w2)
{
	hull() {
		translate([0, _w2, 0])
			cylinder(r = hhsmallr+hhrimw, h = bottom+hhrimh, center = true);
		translate([0, -_w2, 0])
			cylinder(r = hhsmallr+hhrimw, h = bottom+hhrimh, center = true);
	}
}

module hangrim1()
{
	union() {
		hh_rimhull(hhsmallr);
		translate([0, -hhlarger, 0])
			cylinder(r = hhlarger+hhrimw, h = bottom+hhrimh, center = true);
	}
}

module hangrim2()
{
	union() {
		hh_rimhull(hhsmallr+hhlarger);
		rotate(a = 90, v = [0, 0, 1])
			hh_rimhull(hhsmallr+hhlarger);
		cylinder(r = hhlarger+hhrimw, h = bottom+hhrimh, center = true);
	}
}

module hangrim()
{
	translate([0, -hhsmallr+extwidth/2-hhposfromtop, (-thickness+bottom+hhrimh)/2])
		hangrim1();
	translate([-hhsmallr+extwidth/2-hhposfromtop, 0, (-thickness+bottom+hhrimh)/2])
		hangrim2();
	translate([ hhsmallr-extwidth/2+hhposfromtop, 0, (-thickness+bottom+hhrimh)/2])
		hangrim2();
}

module wallcut()
{
	chord = thickness - (bottom + rimdepth + 2 * wallcutspace);
	sagitta = wall - wallmin;
	wr = chord * chord / (8*sagitta) + sagitta/2;
	ww = 2*(strutx - strutradius);
	translate([0, -wr, 0]) {
		intersection() {
			rotate(a = 90, v = [0, 1, 0])
				cylinder(h = ww, r = wr, center = true);
			translate([0, wr, 0])
				cube(size = [ww, 2.1*sagitta, chord], center = true);
		}
	}
}

module wallcuts()
{
	for(i = [0, 90, 180, 270]) {
		rotate(a = i, v = [0, 0, 1]) {
			translate([0, extwidth/2 - wallmin, 0])
				wallcut();
		}
	}
}

module basebox()
{
	difference() {
		difference() {
			baseshape(extwidth, extradius, thickness);
			translate([0, 0, bottom]) {
				baseshape(intwidth, intradius, thickness);
			}
		}
		translate([0, 0, thickness/2]) {
			baseshape(intwidth+2*rimwidth, intradius+rimwidth, 2*rimdepth);
		}
	}
}

module hexahole(_w, _r, _h)
{
	hull() {
		for(i = [0, 60, 120]) {
			rotate(a = i, v = [0, 0, 1]) {
				translate([ (_w/2-_r)/cos(30), 0, 0])
					cylinder(h = _h, r = _r, center= true, $fn = 20);
				translate([-(_w/2-_r)/cos(30), 0, 0])
					cylinder(h = _h, r = _r, center= true, $fn = 20);
			}
		}
	}
}

module pillar(_h, _r, _pos)
{
	translate(v = _pos) {
		difference() {
			cylinder(h = _h, r = _r, center = true);
			hexahole(nutdia, nutrounding, _h);
			translate([0, 0, (_h-washerh)/2])
				cylinder(h = washerh, r = washerradius, center = true);
		}
	}
}

module strut(_r, _h)
{
	union() {
		difference() {
			union() {
				cylinder(h = _h, r = _r, center = true);
				translate([0, _r/2, 0]) {
					cube(size = [2*_r, _r, _h], center = true);
				}
			}
			cylinder(h = _h, r = strutholeradius, center = true);
		}
	}
}

module strutpair(_r)
{
	height = thickness-(rimdepth+pcbdepth);
	pz = (-thickness+height)/2;
	translate(v = [0, 0, pz]) {
		translate([strutx, struty, 0]) {
			strut(_r, height);
		}
		translate([struty, strutx, 0]) {
			rotate(a = 270, v = [0, 0, 1]) { strut(_r, height); }
		}
	}
}

module struts()
{
	for(i = [0, 90, 180, 270]) {
		rotate(a = i, v = [0, 0, 1]) {
			strutpair(strutradius);
		}
	}
}

module pillars()
{
	height = thickness - (rimdepth+pcbdepth+pcbthickness+pcbspacer+bottom);
	pillar(height, pillarradius, [0,  18.034, -thickness/2+bottom+height/2]);
	pillar(height, pillarradius, [0, -18.034, -thickness/2+bottom+height/2]);
	pillar(height, pillarradius, [ 18.034, 0, -thickness/2+bottom+height/2]);
	pillar(height, pillarradius, [-18.034, 0, -thickness/2+bottom+height/2]);
}

module segmclock()
{
	difference() {
		union() {
			difference() {
				union() {
					basebox();
					hangrim();
				}
*				wallcuts();
				bottomhole();
				hanghole();
			}
			struts();
			pillars();
		}
		screwholes();
	}
}

slice = 10;
//nslice = 0;

translate([0, 0, -nslice * slice]) {
	intersection() {
		translate(v = [0, 0, thickness/2]) {
			segmclock();
		}
		translate([0, 0, nslice*slice + slice/2])
			cube(size = [extwidth, extwidth, slice], center = true);
	}
}
