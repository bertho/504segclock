rimwidth = 8.5;
rimdepth = 3.0;
bottomrim = rimdepth;
thickness = 31.0+bottomrim;
bottom = 0.0;
wall = 10.0;
intwidth = 187.3 + 2.0;
intradius = 108.03 + 1.0;
extwidth = intwidth + 2*wall;
extradius = intradius + wall;
pcbthickness = 1.6;
pcbdepth = 7.5+pcbthickness;

strutradius = 6.0;
strutholeradius = 1.1; // 1.2 for M3, will drill...
strutx = 50.0;
struty = 89.8;

screwholeradius = 1.1; // 1.2 for M3, will drill...

// Locking mechanism
lcrad = 1.0;
lcw = wall/3;
lch = wall/2;
lcspace = 0.125;	// This is half-width spacing

cablerad = 2.50;
wallstub = 0.75;

$fa = 0.5;
$fs = 0.125;
//$fa = 2.5;
//$fs = 1.5;
//$fn = 30;


module screwhole(_j)
{
	hrim = ((intwidth+rimwidth)/2);
	hdepth = thickness/2-2*bottom;
	rotate(a = _j, v = [0, 0, 1]) {
		for(i = [65, 115]) {
			translate([hrim, hrim*cos(i), bottom+thickness/4]) {
				cylinder(h = hdepth, r = screwholeradius, center=true);
			}
		}
	}
}


module baseshape(_w, _r, _h)
{
	intersection() {
		cube(size = [_w, _w, _h], center = true);
		cylinder(h = _h, r = _r, center = true);
	}
}


module strut(_r, _h)
{
	union() {
		difference() {
			union() {
				cylinder(h = _h, r = _r, center = true);
				translate([0, _r/2, 0]) {
					cube(size = [2*_r, _r, _h], center = true);
				}
			}
			cylinder(h = _h, r = strutholeradius, center = true);
		}
	}
}

module strutpair(_r)
{
	height = thickness-pcbdepth-bottomrim;
	pz = (-thickness+height-rimdepth)/2+bottomrim;
	translate(v = [0, 0, pz]) {
		translate([strutx, struty, 0]) {
			strut(_r, height);
		}
		translate([struty, strutx, 0]) {
			rotate(a = 270, v = [0, 0, 1]) { strut(_r, height); }
		}
	}
}


module locker(_s)
{
	_w = lcw + 2*_s;
	_h = lch + _s;
	_r = lcrad + _s;
	translate([0, -_h/2-_r, 0]) {
		union() {
			cube(size = [_w, _h, thickness+rimdepth], center = true);
			translate([_w/2-_s, _h/2, 0])
				cylinder(r = _r, h = thickness+rimdepth, center = true);
			translate([-_w/2+_s, _h/2, 0])
				cylinder(r = _r, h = thickness+rimdepth, center = true);
			translate([0, _h/2, 0])
				cube([_w, 2*_r, thickness+rimdepth], center=true);
		}
	}
}

module cablecut()
{
	_l = wall-2*wallstub;
	rotate(a = 90, v = [1, 0, 0]) {
		union() {
			cube(size = [2*cablerad, 2*cablerad, _l], center=true);
			translate([0, cablerad, 0])
				cylinder(r = cablerad, h = _l, center=true);
		}
	}
}

module case_section()
{
	difference() {
		union() {
			difference() {
				intersection() {
					translate([extwidth/4, extwidth/4, 0])
						cube(size = [extwidth/2, extwidth/2, thickness+rimdepth], center = true);
					baseshape(extwidth, extradius, thickness+rimdepth);
				}
				baseshape(intwidth, intradius, thickness+rimdepth);
				screwhole(0);
				screwhole(90);
				translate([extwidth/2-wall/2, lcw+lcspace, 0])
					locker(lcspace);
			}
			strutpair(strutradius);
			translate([-lcw+lcspace, extwidth/2-wall/2, 0])
				rotate(a = 90, v = [0, 0, 1])
					locker(-lcspace);
		}
		translate([0, 0, thickness/2])
			baseshape(intwidth+2*rimwidth, intradius+rimwidth, rimdepth);
	}
}

module quadrant()
{
	intersection() {
		difference() {
			translate([0, 0, (thickness+rimdepth)/2]) {
				case_section();
			}
			translate([0, (intwidth+wall)/2, 0])
				cablecut();
			translate([(intwidth+wall)/2, 0, 0])
				rotate(a = 90, v = [0, 0, 1])
					cablecut();
		}
		// Enable to have only a base view of 1mm
		*cube(size = [extwidth, extwidth, 2.0], center = true);
	}
}

quadrant();
translate([-0.05, 0, 0]) {
	rotate(a = 90, v = [0, 0, 1])
*		quadrant();
}
