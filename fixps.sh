#!/bin/bash

PAGES=4
FILES="segcontrol/504segcontrol_sch.ps segdisplay/504segdisplay_sch.ps segpsu/504segpsu_sch.ps segsense/504segsense_sch.ps"
BASE="504segclock_sch"
TARGET="${BASE}.ps"
PDF="${BASE}.pdf"

for i in ${FILES}
do
	if [ ! -r "${i}" ]
	then
		echo "${i} not readable"
		exit 1
	fi
done

psmerge -o${TARGET} ${FILES}
sed -i -e's_^%%Title:.*_%%Title: 504 Segment Clock_' -e's,^%%Author:.*,%%Author: Vagrearg,' ${TARGET}
ps2pdf14 -dOptimize=true -dEmbedAllFonts=true -sPAPERSIZE=a4 ${TARGET}
for ((i=1; i<=${PAGES}; i++))
do
	pdftoppm -gray -png -f ${i} -l ${i} ${PDF} > ${BASE}_$i.png
	convert ${BASE}_$i.png -scale '25%' ${BASE}_$i-small.png
done
